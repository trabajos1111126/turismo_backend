import { Sucursal } from 'src/modules/module-empresa/sucursal/entities/sucursal.entity';
export const sucursalProviders = [
  {
    provide: 'SUCURSAL_REPOSITORY',
    useValue: Sucursal,
  },
];
