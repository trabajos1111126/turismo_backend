import { MacrodistritoInventario } from 'src/modules/module-inventario-turistico/inventario-turistico/enum/estatus.enum';

export class CreateSucursalDto {
  id_empresa: number;
  direccion: string;
  contactos: string;
  macrodistrito: MacrodistritoInventario;
  latitud: number;
  longitud: number;
}
