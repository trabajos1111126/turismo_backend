import { MacrodistritoInventario } from 'src/modules/module-inventario-turistico/inventario-turistico/enum/estatus.enum';

export class UpdateSucursalDto {
  id_sucursal: number;
  direccion: string;
  contactos: string;
  macrodistrito: MacrodistritoInventario;
  latitud: number;
  longitud: number;
  id_empresa: number;
}
