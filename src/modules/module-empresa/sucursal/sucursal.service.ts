import { Injectable, Inject, NotFoundException, Body } from '@nestjs/common';
import { Sucursal } from 'src/modules/module-empresa/sucursal/entities/sucursal.entity';

import { CreateSucursalDto } from './dto/createSucursal.dto';

@Injectable()
export class SucursalService {
  constructor(
    @Inject('SUCURSAL_REPOSITORY')
    private sucursalRepository: typeof Sucursal,
  ) {}

  async insert(@Body() body: CreateSucursalDto) {
    try {
      const sucursal = new Sucursal();
      sucursal.direccion = body.direccion;
      sucursal.contactos = body.contactos;
      sucursal.latitud = body.latitud;
      sucursal.longitud = body.longitud;
      sucursal.id_empresa = body.id_empresa;
      const sucursal_guardado = await sucursal.save();
      return sucursal_guardado;
    } catch (error) {
      throw new NotFoundException(error.message);
    }
  }
}
