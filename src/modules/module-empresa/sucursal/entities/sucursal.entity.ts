import {
  Table,
  Column,
  Model,
  BelongsTo,
  ForeignKey,
  DataType,
  BelongsToMany,
} from 'sequelize-typescript';
import { Empresa } from '../../empresa/entities/empresa.entity';
import { DataTypes } from 'sequelize';
import { MacrodistritoInventario } from 'src/modules/module-inventario-turistico/inventario-turistico/enum/estatus.enum';
import { Ruta } from 'src/modules/module-rutas-turisticas/rutas/entities/ruta.entity';
import { RutaSucursal } from 'src/modules/module-rutas-turisticas/rutas/entities/ruta-sucursal.entity';

@Table({ tableName: 'sucursal' })
export class Sucursal extends Model {
  @Column({ primaryKey: true, autoIncrement: true }) // autoIncrement: true
  id_sucursal: number;

  @BelongsTo(() => Empresa)
  empresa: Empresa;
  @ForeignKey(() => Empresa)
  id_empresa: number;

  @Column
  direccion: string;

  @Column
  contactos: string;

  @Column(DataType.ENUM(...Object.values(MacrodistritoInventario)))
  macrodistrito: string;

  @Column({ type: DataTypes.FLOAT, defaultValue: -16.5044627 })
  latitud: number;

  @Column({ type: DataTypes.FLOAT, defaultValue: -68.1308097 })
  longitud: number;

  @Column({ defaultValue: false })
  deleted: boolean;

  @BelongsToMany(() => Ruta, () => RutaSucursal, 'id_sucursal', 'id_ruta')
  rutas: Ruta[];
}
