import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Request,
  UseGuards,
  SetMetadata,
  UseInterceptors,
  UploadedFile,
} from '@nestjs/common';
//import { GuiaTuristicoService } from './guia-turistico.service';

import { AuthGuard } from 'src/modules/module-usuario/auth/auth.guard';
import { EmpresaService } from './empresa.service';
import { CreateEmpresaDto } from './dto/empresa.dto';
import { UpdateEmpresaDto } from './dto/update.empresa.dto';
import { Sucursal } from '../sucursal/entities/sucursal.entity';
import { CreateSucursalDto } from '../sucursal/dto/createSucursal.dto';
import { RolUsuarioEnum } from 'src/modules/module-usuario/usuario/enum/usuario.enum';
import {
  multerConfigEmpresa,
  multerConfigEmpresaLogo,
} from 'src/config/multer.config';
import { FileInterceptor } from '@nestjs/platform-express';
import { UpdateSucursalDto } from '../sucursal/dto/updateSucursal.dto';

@Controller('api/empresa/')
export class EmpresaController {
  constructor(private readonly empresaService: EmpresaService) {}

  @SetMetadata('roles', [
    RolUsuarioEnum.ADMINISTRADOR,
    RolUsuarioEnum.TURISTA,
    RolUsuarioEnum.ANONIMO,
  ])
  @UseGuards(AuthGuard)
  @Get('')
  getAll(@Request() req) {
    console.log('getAll empresas');
    return this.empresaService.getAll(req);
  }

  @SetMetadata('roles', [
    RolUsuarioEnum.ADMINISTRADOR,
    RolUsuarioEnum.TURISTA,
    RolUsuarioEnum.ANONIMO,
  ])
  @UseGuards(AuthGuard)
  @Get(':id')
  async getUserById(@Param('id') id: number, @Request() req) {
    console.log('getById empresa');
    return this.empresaService.getById(id, req);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Get('/admin/:id')
  async getByIdForAdmin(@Param('id') id: number, @Request() req) {
    console.log('getByIdForAdmin empresa');
    return this.empresaService.getByIdForAdmin(id, req);
  }

  @SetMetadata('roles', [
    RolUsuarioEnum.ADMINISTRADOR,
    RolUsuarioEnum.TURISTA,
    RolUsuarioEnum.ANONIMO,
  ])
  @UseGuards(AuthGuard)
  @Get('tipo/:id')
  getAllByTipo(@Request() req, @Param('id') id: number) {
    console.log('getAllByTipo Empresa');
    return this.empresaService.getAllByTipo(req, id);
  }

  @SetMetadata('roles', [
    RolUsuarioEnum.ADMINISTRADOR,
    RolUsuarioEnum.TURISTA,
    RolUsuarioEnum.ANONIMO,
  ])
  @UseGuards(AuthGuard)
  @Get('especialidad/:id')
  getAllEspeciliadadByTipo(@Request() req, @Param('id') id: number) {
    console.log('Especialidad Empresa');
    return this.empresaService.getAllEspeciliadadByTipo(req, id);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Post()
  insert(@Body() body: CreateEmpresaDto) {
    console.log('insert empresas');
    return this.empresaService.insert(body);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Patch()
  async update(@Body() updateEmpresaDto: UpdateEmpresaDto) {
    console.log('Update empresa');
    return this.empresaService.update(updateEmpresaDto);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Delete(':id')
  delete(@Param('id') id: number) {
    console.log('Delete empresa');
    return this.empresaService.delete(id);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Post('sucursal')
  agregarSucursal(@Body() createSucursalDto: CreateSucursalDto) {
    console.log('Agregar sucursal a Empresa');
    return this.empresaService.agregarSucursal(createSucursalDto);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Patch('sucursal')
  updateSucursal(@Body() updateSucursalDto: UpdateSucursalDto) {
    console.log('Editamos sucursal a Empresa');
    return this.empresaService.updateSucursal(updateSucursalDto);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Get('sucursal/:id_sucursal')
  getByIdSucursal(@Param('id_sucursal') id_sucursal: number) {
    console.log('getById sucursal a Empresa');
    console.log(id_sucursal);
    return this.empresaService.getByIdSucursal(id_sucursal);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Patch(':id/foto')
  @UseInterceptors(FileInterceptor('image', multerConfigEmpresa))
  agregarFoto(
    @Param('id') id: number,
    @UploadedFile() file: Express.Multer.File,
  ) {
    console.log('AgregarFoto Empresa');
    return this.empresaService.agregarFoto(id, file);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Delete(':id_empresa/foto/:id_foto')
  deleteFoto(
    @Param('id_empresa') id_empresa: number,
    @Param('id_foto') id_foto: number,
  ) {
    console.log('Delete empresa');
    return this.empresaService.deleteFoto(id_foto);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Patch(':id/logo')
  @UseInterceptors(FileInterceptor('image', multerConfigEmpresaLogo))
  modificarLogo(
    @Param('id') id: number,
    @UploadedFile() file: Express.Multer.File,
  ) {
    console.log('ModificarLogo Empresa');
    return this.empresaService.modificarLogo(id, file);
  }
}
