export enum CategoriaEmpresaEnum {
  INDEFINIDO = 'INDEFINIDO',
  NULL = '',
  A = 'A',
  B = 'B',
  ESTRELLA_1 = '1',
  ESTRELLA_2 = '2',
  ESTRELLA_3 = '3',
  ESTRELLA_4 = '4',
  ESTRELLA_5 = '5',
}
