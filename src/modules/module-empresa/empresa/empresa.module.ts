import { Module } from '@nestjs/common';
import { EmpresaController } from './empresa.controller';
import { EmpresaService } from './empresa.service';
import { empresaProviders } from './empresa.providers';

@Module({
  imports: [],
  controllers: [EmpresaController],
  providers: [EmpresaService, ...empresaProviders],
  exports: [],
})
export class EmpresaModule {}
