import { Empresa } from './entities/empresa.entity';

export const empresaProviders = [
  {
    provide: 'EMPRESA_REPOSITORY',
    useValue: Empresa,
  },
];
