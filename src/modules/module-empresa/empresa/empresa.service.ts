import {
  Injectable,
  Inject,
  NotFoundException,
  Body,
  UploadedFile,
} from '@nestjs/common';

import { CreateEmpresaDto } from './dto/empresa.dto';
import { Empresa } from './entities/empresa.entity';
import { empresaSeedData } from './seeds/empresa.seeds.data';
import { UsuarioHeaderDto } from 'src/modules/module-usuario/auth/dto/header-usuario';
import { Sucursal } from '../sucursal/entities/sucursal.entity';
import { TipoEmpresa } from '../tipo-empresa/entities/tipo_de_empresa.entity';
import { Idioma } from 'src/modules/module-library/idioma/entity/idioma.entity';
import { ValoracionEmpresa } from '../valoracion/entities/valoracion-empresa.entity';
import { Sequelize } from 'sequelize';
import { Usuario } from 'src/modules/module-usuario/usuario/entities/usuario.entity';
import { UpdateEmpresaDto } from './dto/update.empresa.dto';
import { CreateSucursalDto } from '../sucursal/dto/createSucursal.dto';
import { SubtipoEmpresa } from '../subtipo-empresa/entities/subtipo_de_empresa.entity';
import { EspecialidadEmpresa } from '../especialidad-empresa/entities/especialidad_de_empresa.entity';
import { FotoEmpresa } from 'src/modules/module-library/foto_empresa/entities/foto_empresa.entity';
import { TipoIdioma } from 'src/modules/module-library/idioma/enum/status.enum';
import { UpdateSucursalDto } from '../sucursal/dto/updateSucursal.dto';
import { create } from 'domain';
import { Ruta } from 'src/modules/module-rutas-turisticas/rutas/entities/ruta.entity';

@Injectable()
export class EmpresaService {
  constructor(
    @Inject('EMPRESA_REPOSITORY')
    private empresaRepository: typeof Empresa,
  ) {}

  async getAllByTipo(req: any, id: number) {
    try {
      const resp = await this.getAll(req);
      const data = [];
      for (let i = 0; i < resp.data.length; i++) {
        const element = resp.data[i];

        if (element.id_tipo_empresa == id) {
          data.push(element);
        }
      }
      return data;
    } catch (error) {
      throw new NotFoundException(error.message);
    }
  }

  async getAllEspeciliadadByTipo(req: any, id: number) {
    try {
      return EspecialidadEmpresa.findAll({
        attributes: ['id_especialidad_empresa'],
        include: [
          {
            model: Idioma,
            attributes: [[req.idioma, 'txt']],
          },
        ],
      });
      const resp = await this.getAll(req);
      const data = [];
      for (let i = 0; i < resp.data.length; i++) {
        const element = resp.data[i];

        if (element.id_tipo_empresa == id) {
          data.push(element);
        }
      }
      return data;
    } catch (error) {
      throw new NotFoundException(error.message);
    }
  }

  async seeds() {
    const count = await this.empresaRepository.count();
    if (count === 0) {
      for (const element of empresaSeedData) {
        await this.insertSeed(element);
      }
      console.log('Todas las inserciones  de Empresa se han completado.');
    }
  }

  async agregarDescripciones() {
    try {
      const empresas = await Empresa.findAll({
        where: {
          id_idioma: null,
        },
      });

      for (let i = 0; i < empresas.length; i++) {
        const element = empresas[i];
        const traduccion = new Idioma();
        traduccion.es = '';
        traduccion.en = '';
        traduccion.fra = '';
        traduccion.tipo = TipoIdioma.DESCRIPCION_DE_EMPRESA;
        const traduccion_guardado = await traduccion.save();
        //find e descripcion
        element.id_idioma = traduccion_guardado.id_idioma;
        element.descripcion = traduccion_guardado;
        await element.save();
      }
      console.log('Agregamos descripcion de ' + empresas.length + 'EMPRESAS');
      //Agregamos descripcion

      // Aquí puedes hacer lo que necesites con el array de empresas que cumplen con el criterio
      //console.log('Empresas con descripción nula:', empresas);
    } catch (error) {
      // Manejo de errores, por ejemplo, si hay un error en la consulta a la base de datos
      console.error('Error al listar empresas:', error);
    }
  }
  async getByIdForAdmin(id, req: UsuarioHeaderDto) {
    try {
      // Buscar un usuario por su ID
      const empresa = await this.empresaRepository.findByPk(id, {
        attributes: [
          'id_empresa',
          'nombre',
          'categoria',
          'url_web',
          'url_logo',
          'email',
          'id_tipo_empresa',
          'id_subtipo_empresa',
          'id_especialidad_empresa',
          [
            Sequelize.fn(
              'COALESCE',
              Sequelize.fn('AVG', Sequelize.col('valoraciones.calificacion')),
              0,
            ),
            'valoracion',
          ],
          [
            Sequelize.fn('COUNT', Sequelize.col('valoraciones.id_empresa')),
            'cantidad_valoraciones',
          ],
        ],
        include: [
          {
            model: ValoracionEmpresa,
            as: 'valoraciones',
            attributes: ['id_valoracion_empresa', 'calificacion', 'comentario'],
            include: [
              {
                model: Usuario,
                attributes: ['id_usuario', 'nombre', 'url_imagen_perfil'],
              },
            ],
          },

          {
            model: Sucursal,
            attributes: [
              'id_sucursal',
              'direccion',
              'macrodistrito',
              'contactos',
              'latitud',
              'longitud',
            ],
          },
          {
            model: FotoEmpresa,
            //attributes: ['id_foto_empresa:', 'url_foto'],
            where: {
              deleted: false,
            },
            required: false,
          },
          {
            model: TipoEmpresa,
            include: [
              {
                model: Idioma,
                attributes: [[req.idioma, 'idioma']],
              },
            ],
          },
          {
            model: SubtipoEmpresa,
            include: [
              {
                model: Idioma,
                attributes: [[req.idioma, 'idioma']],
              },
            ],
          },
          {
            model: EspecialidadEmpresa,
            include: [
              {
                model: Idioma,
                attributes: [[req.idioma, 'idioma']],
              },
            ],
          },
          {
            model: Idioma,
            as: 'descripcion',
            attributes: [[req.idioma, 'idioma']],
          },
        ],
        group: [
          'Empresa.id_empresa',
          'fotos.id_foto_empresa',
          'fotos.url_foto',
          'descripcion.id_idioma',
          'valoraciones->usuario.id_usuario',
          'valoraciones.id_valoracion_empresa',
          'sucursales.id_sucursal',
          'tipoEmpresa.id_tipo_empresa',
          'tipoEmpresa->idioma.id_idioma',
          'subtipoEmpresa.id_subtipo_empresa',
          'subtipoEmpresa->idioma.id_idioma',
          'especialidadEmpresa.id_especialidad_empresa',
          'especialidadEmpresa->idioma.id_idioma',
        ],
      });
      //console.log(empresa);
      if (empresa) {
        return {
          id_empresa: empresa.id_empresa,
          nombre: empresa.nombre,
          categoria: empresa.categoria,
          url_web: empresa.url_web,
          url_logo: empresa.url_logo,
          email: empresa.email,
          descripcion: empresa.descripcion?.dataValues.idioma,
          fotos: empresa.fotos,
          id_tipo_empresa: empresa?.id_tipo_empresa,
          tipoEmpresa: empresa.tipoEmpresa?.dataValues.idioma.dataValues.idioma,
          id_subtipo_empresa: empresa?.id_subtipo_empresa,
          subtipoEmpresa:
            empresa.subtipoEmpresa?.dataValues.idioma.dataValues.idioma,
          id_especialidad_empresa: empresa?.id_especialidad_empresa,
          tra_especialidad_empresa:
            empresa.especialidadEmpresa?.dataValues.idioma.dataValues.idioma,
          valoraciones: empresa.valoraciones,
          sucursales: empresa.sucursales,
          valoracion: empresa.dataValues.valoracion,
          cantidad_valoraciones: empresa.dataValues.cantidad_valoraciones,
        };
      } else {
        console.log('Empresa no encontrado');
        throw new NotFoundException('Empresa no encontrado');
      }
    } catch (error) {
      console.error('Error al buscar empresa:', error);
      throw new NotFoundException('Error al buscar empresa:', error.message);
    }
  }
  async getById(id, req: UsuarioHeaderDto) {
    try {
      // Buscar un usuario por su ID
      const empresa = await this.empresaRepository.findByPk(id, {
        attributes: [
          'id_empresa',
          'nombre',
          'categoria',
          'url_web',
          'url_logo',
          'email',
          'id_tipo_empresa',
          'id_subtipo_empresa',
          'id_especialidad_empresa',
          [
            Sequelize.fn(
              'COALESCE',
              Sequelize.fn('AVG', Sequelize.col('valoraciones.calificacion')),
              0,
            ),
            'valoracion',
          ],
          [
            Sequelize.fn('COUNT', Sequelize.col('valoraciones.id_empresa')),
            'cantidad_valoraciones',
          ],
        ],
        include: [
          {
            model: ValoracionEmpresa,
            as: 'valoraciones',
            attributes: ['id_valoracion_empresa', 'calificacion', 'comentario'],
            include: [
              {
                model: Usuario,
                attributes: ['id_usuario', 'nombre', 'url_imagen_perfil'],
              },
            ],
          },

          {
            model: Sucursal,
            attributes: [
              'id_sucursal',
              'direccion',
              'macrodistrito',
              'contactos',
              'latitud',
              'longitud',
            ],
          },
          {
            model: FotoEmpresa,
            attributes: ['url_foto'],
            where: {
              deleted: false,
            },
            required: false,
          },
          {
            model: TipoEmpresa,
            include: [
              {
                model: Idioma,
                attributes: [[req.idioma, 'idioma']],
              },
            ],
          },
          {
            model: SubtipoEmpresa,
            include: [
              {
                model: Idioma,
                attributes: [[req.idioma, 'idioma']],
              },
            ],
          },
          {
            model: EspecialidadEmpresa,
            include: [
              {
                model: Idioma,
                attributes: [[req.idioma, 'idioma']],
              },
            ],
          },
          {
            model: Idioma,
            as: 'descripcion',
            attributes: [[req.idioma, 'idioma']],
          },
        ],
        group: [
          'Empresa.id_empresa',
          'fotos.id_foto_empresa',
          'fotos.url_foto',
          'descripcion.id_idioma',
          'valoraciones->usuario.id_usuario',
          'valoraciones.id_valoracion_empresa',
          'sucursales.id_sucursal',
          'tipoEmpresa.id_tipo_empresa',
          'tipoEmpresa->idioma.id_idioma',
          'subtipoEmpresa.id_subtipo_empresa',
          'subtipoEmpresa->idioma.id_idioma',
          'especialidadEmpresa.id_especialidad_empresa',
          'especialidadEmpresa->idioma.id_idioma',
        ],
      });
      //console.log(empresa);
      if (empresa) {
        return {
          id_empresa: empresa.id_empresa,
          nombre: empresa.nombre,
          categoria: empresa.categoria,
          url_web: empresa.url_web,
          url_logo: empresa.url_logo,
          email: empresa.email,
          descripcion: empresa.descripcion?.dataValues.idioma,
          fotos: empresa.fotos,
          id_tipo_empresa: empresa?.id_tipo_empresa,
          tipoEmpresa: empresa.tipoEmpresa?.dataValues.idioma.dataValues.idioma,
          id_subtipo_empresa: empresa?.id_subtipo_empresa,
          subtipoEmpresa:
            empresa.subtipoEmpresa?.dataValues.idioma.dataValues.idioma,
          id_especialidad_empresa: empresa?.id_especialidad_empresa,
          tra_especialidad_empresa:
            empresa.especialidadEmpresa?.dataValues.idioma.dataValues.idioma,
          valoraciones: empresa.valoraciones,
          sucursales: empresa.sucursales,
          valoracion: empresa.dataValues.valoracion,
          cantidad_valoraciones: empresa.dataValues.cantidad_valoraciones,
        };
      } else {
        console.log('Empresa no encontrado');
        throw new NotFoundException('Empresa no encontrado');
      }
    } catch (error) {
      console.error('Error al buscar empresa:', error);
      throw new NotFoundException('Error al buscar empresa:', error.message);
    }
  }

  async getAll(req: UsuarioHeaderDto) {
    try {
      //const n = await this.empresaRepository.findAll();
      //console.log(n);
      const empresas = await this.empresaRepository.findAll({
        attributes: [
          'id_empresa',
          'nombre',
          'categoria',
          'url_web',
          'email',
          'id_tipo_empresa',
          'id_subtipo_empresa',
          'id_especialidad_empresa',
          'url_logo',
          [
            Sequelize.fn(
              'COALESCE',
              Sequelize.fn('AVG', Sequelize.col('valoraciones.calificacion')),
              0,
            ),
            'valoracion',
          ],
          [
            Sequelize.fn('COUNT', Sequelize.col('valoraciones.id_empresa')),
            'cantidad_valoraciones',
          ],
        ],
        include: [
          {
            model: ValoracionEmpresa,
            as: 'valoraciones',
            attributes: [],
          },
          {
            model: Sucursal,
            attributes: [
              'id_sucursal',
              'macrodistrito',
              'direccion',
              'contactos',
              'latitud',
              'longitud',
            ],
          },
          {
            model: TipoEmpresa,
            include: [
              {
                model: Idioma,
                attributes: [[req.idioma, 'idioma']],
              },
            ],
          },
          {
            model: SubtipoEmpresa,
            include: [
              {
                model: Idioma,
                attributes: [[req.idioma, 'idioma']],
              },
            ],
          },
          {
            model: EspecialidadEmpresa,
            include: [
              {
                model: Idioma,
                attributes: [[req.idioma, 'idioma']],
              },
            ],
          },
        ],
        where: { deleted: false, visible: true },
        group: [
          'Empresa.id_empresa',
          'sucursales.id_sucursal',
          'tipoEmpresa.id_tipo_empresa',
          'tipoEmpresa->idioma.id_idioma',
          'subtipoEmpresa.id_subtipo_empresa',
          'subtipoEmpresa->idioma.id_idioma',
          'especialidadEmpresa.id_especialidad_empresa',
          'especialidadEmpresa->idioma.id_idioma',
        ],
        raw: false,
        order: [
          [Sequelize.col('valoracion'), 'DESC'],
          [
            Sequelize.literal(`
            CASE
              WHEN url_web = '' THEN 1
              ELSE 0
            END
          `),
            'ASC',
          ],
          [
            Sequelize.literal(`
            CASE
              WHEN email = '' THEN 1
              ELSE 0
            END
          `),
            'ASC',
          ],
          ['nombre', 'ASC'],
        ],
      });
      //console.log(empresas.length);
      const data = empresas.map((item) => ({
        id_empresa: item.id_empresa,
        nombre: item.nombre,
        categoria: item.categoria,
        url_web: item.url_web,
        email: item.email,
        sucursales: this.resetLatitudesLongitudes(item.sucursales),
        logo_empresa: item.url_logo,
        id_tipo_empresa: item?.id_tipo_empresa,
        tra_tipo_empresa: item.tipoEmpresa?.dataValues.idioma.dataValues.idioma,
        id_subtipo_empresa: item?.id_subtipo_empresa,
        tra_subtipo_empresa:
          item.subtipoEmpresa?.dataValues.idioma.dataValues.idioma,
        id_especialidad_empresa: item?.id_especialidad_empresa,
        tra_especialidad_empresa:
          item.especialidadEmpresa?.dataValues.idioma.dataValues.idioma,
        valoracion: item.dataValues.valoracion,
        cantidad_valoraciones: item.dataValues.cantidad_valoraciones,
      }));
      //console.log(data);
      return {
        data,
      };
    } catch (error) {
      console.error('Error al buscar empresa:', error);
      throw new NotFoundException('Error al buscar empresa:', error.message);
    }
  }

  async getByIdSucursal(id_sucursal: number) {
    console.log(id_sucursal);
    try {
      const ruta = await Sucursal.findByPk(id_sucursal);
      if (ruta) {
        return ruta;
      } else {
        console.log('Empresa no encontrado');
        throw new NotFoundException('Empresa no encontrado');
      }
    } catch (error) {
      console.log(error.message);
      throw new NotFoundException('Error al buscar ruta:', error.message);
    }
  }
  async agregarSucursal(createSucursalDto: CreateSucursalDto) {
    try {
      const empresaExistente = await this.empresaRepository.findByPk(
        createSucursalDto.id_empresa,
      );

      if (!empresaExistente) {
        throw new NotFoundException('Guía no encontrada');
      }
      //primero creamos sucursal
      const sucursal = new Sucursal();
      sucursal.direccion = createSucursalDto.direccion;
      sucursal.contactos = createSucursalDto.contactos;
      sucursal.macrodistrito = createSucursalDto.macrodistrito;
      sucursal.latitud = createSucursalDto.latitud;
      sucursal.longitud = createSucursalDto.longitud;
      sucursal.id_empresa = createSucursalDto.id_empresa;
      const sucursal_guardado = await sucursal.save();
      return sucursal_guardado;
    } catch (error) {
      console.error('Error al buscar usuario:', error);
      throw new NotFoundException('Error al buscar usuario:', error.message);
    }
  }

  async updateSucursal(updateSucursalDto: UpdateSucursalDto) {
    try {
      const sucursal = await Sucursal.findByPk(updateSucursalDto.id_sucursal);

      if (!sucursal) {
        throw new NotFoundException('Sucursal no encontrada');
      }
      //primero creamos sucursal
      sucursal.direccion = updateSucursalDto.direccion;
      sucursal.contactos = updateSucursalDto.contactos;
      sucursal.macrodistrito = updateSucursalDto.macrodistrito;
      sucursal.latitud = updateSucursalDto.latitud;
      sucursal.longitud = updateSucursalDto.longitud;
      const sucursal_guardado = await sucursal.save();
      return sucursal_guardado;
    } catch (error) {
      console.error('Error al editar sucursal:', error);
      throw new NotFoundException('Error al editar sucursal:', error.message);
    }
  }

  async insertSeed(body: any) {
    try {
      //agregamos el tipo de empresa

      const empresa = new Empresa();
      empresa.nombre = body.nombre;
      empresa.categoria = body.categoria;
      empresa.url_web = body.url_web;
      empresa.email = body.email;
      empresa.id_tipo_empresa = body.id_tipo_empresa;
      empresa.id_subtipo_empresa = body.id_subtipo_empresa;
      empresa.id_especialidad_empresa = body.id_especialidad_empresa;
      //Agregamos relacion de descripcion
      const empresa_guardado = await empresa.save();
      //** guardamos sucursales */
      for (let i = 0; i < body.sucursales.length; i++) {
        const element = body.sucursales[i];
        const sucursal = new Sucursal();
        sucursal.id_empresa = empresa_guardado.id_empresa;
        sucursal.macrodistrito = element.macrodistrito;
        sucursal.direccion = element.direccion;
        sucursal.contactos = element.contactos;
        sucursal.latitud = element.latitud;
        sucursal.longitud = element.longitud;
        await sucursal.save();
      }
      return empresa_guardado;
    } catch (error) {
      console.log(error.message);
      throw new NotFoundException(error.message);
    }
  }

  async insert(@Body() body: CreateEmpresaDto) {
    try {
      const empresa = new Empresa();
      empresa.nombre = body.nombre;
      empresa.categoria = body.categoria;
      empresa.url_web = body.url_web;
      empresa.email = body.email;
      empresa.id_tipo_empresa = body.id_tipo_empresa;
      empresa.id_subtipo_empresa = body.id_subtipo_empresa;
      empresa.id_especialidad_empresa = body.id_especialidad_empresa;
      empresa.visible = body.visible;
      //Agregamos descripcion
      const traduccion = new Idioma();
      traduccion.es = body.descripcion_es;
      traduccion.en = body.descripcion_en;
      traduccion.fra = body.descripcion_fra;
      traduccion.tipo = TipoIdioma.DESCRIPCION_DE_EMPRESA;
      const traduccion_guardado = await traduccion.save();
      //find e descripcion
      empresa.id_idioma = traduccion_guardado.id_idioma;
      empresa.descripcion = traduccion_guardado;
      const empresa_guardado = await empresa.save();
      return empresa_guardado;
    } catch (error) {
      throw new NotFoundException(
        'error al insertar una empresa',
        error.message,
      );
    }
  }

  async update(updateEmpresaDto: UpdateEmpresaDto) {
    try {
      const empresaExistente = await this.empresaRepository.findByPk(
        updateEmpresaDto.id_empresa,
      );

      if (!empresaExistente) {
        throw new NotFoundException('Guía no encontrada');
      }

      // Actualizar los campos relevantes con los datos del DTO
      empresaExistente.nombre = updateEmpresaDto.nombre;
      empresaExistente.categoria = updateEmpresaDto.categoria;
      empresaExistente.url_web = updateEmpresaDto.url_web;
      empresaExistente.email = updateEmpresaDto.email;
      empresaExistente.id_tipo_empresa = updateEmpresaDto.id_tipo_empresa;
      empresaExistente.id_subtipo_empresa = updateEmpresaDto.id_subtipo_empresa;
      empresaExistente.id_especialidad_empresa =
        updateEmpresaDto.id_especialidad_empresa;
      empresaExistente.visible = updateEmpresaDto.visible;
      ///Modificamos descripcion
      let traduccion = await Idioma.findByPk(empresaExistente.id_idioma);
      if (!traduccion) {
        traduccion = new Idioma();
      }

      traduccion.es = updateEmpresaDto.descripcion_es;
      traduccion.en = updateEmpresaDto.descripcion_en;
      traduccion.fra = updateEmpresaDto.descripcion_fra;
      traduccion.tipo = TipoIdioma.DESCRIPCION_DE_EMPRESA;
      const traduccion_guardado = await traduccion.save();

      empresaExistente.id_idioma = traduccion_guardado.id_idioma;
      empresaExistente.descripcion = traduccion_guardado;
      const guia_guardado = await empresaExistente.save();

      return guia_guardado;
    } catch (error) {
      console.error('Error al actualizar usuario:', error.message);
      throw new NotFoundException(
        'Error al actualizar usuario:',
        error.message,
      );
    }
  }

  async delete(id: number) {
    try {
      const empresa = await this.empresaRepository.findByPk(id);

      if (!empresa) {
        throw new NotFoundException('Empresa no encontrada');
      }

      empresa.deleted = true; // Marcamos la entidad como borrada
      await empresa.save();
      return 'La Empresa se ha borrado exitosamente';
    } catch (error) {
      console.error('Error al borrar usuario:', error);
      throw new NotFoundException('Error al borrar usuario:', error.message);
    }
  }

  async agregarFoto(
    id_empresa: number,
    @UploadedFile() file: Express.Multer.File,
  ) {
    const empresa = await this.empresaRepository.findByPk(id_empresa);

    if (!empresa) {
      console.log(`No se encontró la empresa con el id ${id_empresa}.`);
      return;
    }

    const foto_empresa = new FotoEmpresa();
    foto_empresa.url_foto = 'images/empresa/' + file.filename;
    //adjunto.url_min = 'images/empresa/' + file.filename;
    foto_empresa.mimetype = file.mimetype;
    foto_empresa.empresa = empresa;
    foto_empresa.id_empresa = empresa.id_empresa;
    const foto_empresa_guardado = await foto_empresa.save();
    return foto_empresa_guardado;
  }

  async deleteFoto(id_foto: number) {
    try {
      const foto_empresa = await FotoEmpresa.findByPk(id_foto);

      if (!foto_empresa) {
        throw new NotFoundException('Empresa no encontrada');
      }

      foto_empresa.deleted = true; // Marcamos la entidad como borrada
      await foto_empresa.save();
      return 'La Foto se ha borrado exitosamente';
    } catch (error) {
      console.error('Error al borrar la Foto', error);
      throw new NotFoundException('Error al borrar foto:', error.message);
    }
  }

  async modificarLogo(
    id_empresa: number,
    @UploadedFile() file: Express.Multer.File,
  ) {
    const empresa = await this.empresaRepository.findByPk(id_empresa);

    if (!empresa) {
      console.log(`No se encontró la empresa con el id ${id_empresa}.`);
      return;
    }
    empresa.url_logo = 'images/empresa/logo/' + file.filename;
    const empresa_guardado = await empresa.save();
    return empresa_guardado;
  }
  // Define una función para modificar las latitudes y longitudes en 0.
  private resetLatitudesLongitudes(sucursales: any[]) {
    const n = [];
    for (const sucursal of sucursales) {
      n.push({
        id_sucursal: sucursal.id_sucursal,
        macrodistrito: sucursal.macrodistrito,
        direccion: sucursal.direccion,
        contactos: sucursal.contactos,
        latitud: 0,
        longitud: 0,
      });
    }
    return n;
  }
}
