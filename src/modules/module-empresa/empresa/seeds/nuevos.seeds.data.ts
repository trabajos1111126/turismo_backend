export const empresaSeedData = [
  {
    nombre: 'La grosería',
    id_tipo_empresa: 1,
    id_subtipo_empresa: 1,
    categoria: '3',
    id_especialidad_empresa: null,
    url_web: 'https://www.facebook.com/Lagroseriafreakshakes?mibextid=ZbWKwL',
    email: '',
    sucursales: [
      {
        macrodistrito: '',
        direccion: '',
        contactos: '',
        latitud: -16.497522620400538,
        longitud: -68.13857224417525,
      },
    ],
  },
  {
    nombre: 'Sangre y Madera Café Espacio Cultural',
    id_tipo_empresa: 1,
    id_subtipo_empresa: 1,
    categoria: '3',
    id_especialidad_empresa: null,
    url_web: 'https://www.facebook.com/CafeSangreyMadera?mibextid=ZbWKwL',
    email: '',
    sucursales: [
      {
        macrodistrito: '',
        direccion: '',
        contactos: '',
        latitud: -16.508944499357444,
        longitud: -68.12844563987179,
      },
    ],
  },
  {
    nombre: 'Sultana Café – Arte',
    id_tipo_empresa: 1,
    id_subtipo_empresa: 1,
    categoria: '3',
    id_especialidad_empresa: null,
    url_web:
      'https://www.facebook.com/profile.php?id=100063501884675&mibextid=ZbWKwL',
    email: '',
    sucursales: [
      {
        macrodistrito: '',
        direccion: '',
        contactos: '',
        latitud: -16.51221604330899,
        longitud: -68.12943163241533,
      },
    ],
  },
  {
    nombre: 'Manq’a restaurante',
    id_tipo_empresa: 1,
    id_subtipo_empresa: 1,
    categoria: '3',
    id_especialidad_empresa: null,
    url_web: 'https://www.facebook.com/ManqaRestaurante?mibextid=ZbWKwL',
    email: '',
    sucursales: [
      {
        macrodistrito: '',
        direccion: '',
        contactos: '',
        latitud: -16.50991458154115,
        longitud: -68.12636729008803,
      },
    ],
  },
  {
    nombre: 'La Choppería',
    id_tipo_empresa: 1,
    id_subtipo_empresa: 1,
    categoria: '3',
    id_especialidad_empresa: null,
    url_web: 'https://www.facebook.com/Lachopperiasopocachi?mibextid=ZbWKwL',
    email: '',
    sucursales: [
      {
        macrodistrito: '',
        direccion: '',
        contactos: '',
        latitud: -16.51334508965084,
        longitud: -68.12868710357888,
      },
    ],
  },

  {
    nombre: 'Reineke Fuchs',
    id_tipo_empresa: 1,
    id_subtipo_empresa: 1,
    categoria: '3',
    id_especialidad_empresa: null,
    url_web: 'https://www.facebook.com/Lachopperiasopocachi?mibextid=ZbWKwL',
    email: '',
    sucursales: [
      {
        macrodistrito: '',
        direccion: '',
        contactos: '',
        latitud: -16.51334508965084,
        longitud: -68.12799364971062,
      },
    ],
  },

  {
    nombre: 'The Carrot Tree',
    id_tipo_empresa: 1,
    id_subtipo_empresa: 1,
    categoria: '3',
    id_especialidad_empresa: null,
    url_web: 'https://www.facebook.com/Lachopperiasopocachi?mibextid=ZbWKwL',
    email: '',
    sucursales: [
      {
        macrodistrito: '',
        direccion: '',
        contactos: '',
        latitud: -16.507888179160748,
        longitud: -68.12899120369576,
      },
    ],
  },

  {
    nombre: 'La Rufina',
    id_tipo_empresa: 1,
    id_subtipo_empresa: 1,
    categoria: '3',
    id_especialidad_empresa: null,
    url_web: 'https://www.facebook.com/Lachopperiasopocachi?mibextid=ZbWKwL',
    email: '',
    sucursales: [
      {
        macrodistrito: '',
        direccion: '',
        contactos: '',
        latitud: -16.511036343273595,
        longitud: -68.12842816103232,
      },
    ],
  },
];
