import { CategoriaEmpresaEnum } from '../enum/empresa.enum';

export class UpdateEmpresaDto {
  id_empresa: number;
  nombre: string;
  categoria: CategoriaEmpresaEnum;
  url_web: string;
  email: string;
  id_tipo_empresa: number;
  id_subtipo_empresa: number;
  id_especialidad_empresa: number;
  descripcion_es: string;
  descripcion_en: string;
  descripcion_fra: string;
  visible: boolean;
}
