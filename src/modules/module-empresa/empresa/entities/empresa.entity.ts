import {
  Table,
  Column,
  Model,
  ForeignKey,
  BelongsTo,
  DataType,
  HasMany,
} from 'sequelize-typescript';
import { TipoEmpresa } from '../../tipo-empresa/entities/tipo_de_empresa.entity';
import { SubtipoEmpresa } from '../../subtipo-empresa/entities/subtipo_de_empresa.entity';
import { EspecialidadEmpresa } from '../../especialidad-empresa/entities/especialidad_de_empresa.entity';
import { CategoriaEmpresaEnum } from '../enum/empresa.enum';
import { Sucursal } from '../../sucursal/entities/sucursal.entity';
import { ValoracionEmpresa } from '../../valoracion/entities/valoracion-empresa.entity';
import { Adjunto } from 'src/modules/module-library/adjunto/entities/adjunto.entity';
import { FotoEmpresa } from 'src/modules/module-library/foto_empresa/entities/foto_empresa.entity';
import { Idioma } from 'src/modules/module-library/idioma/entity/idioma.entity';
@Table({ tableName: 'empresa' })
export class Empresa extends Model {
  @Column({ primaryKey: true, autoIncrement: true }) // autoIncrement: true
  id_empresa: number;

  @Column
  nombre: string;

  @Column(DataType.ENUM(...Object.values(CategoriaEmpresaEnum)))
  categoria: CategoriaEmpresaEnum;

  @Column({ defaultValue: 'images/empresa_default.jpg' })
  url_logo: string;

  @Column({ defaultValue: '' })
  url_web: string;

  @Column({ defaultValue: '' })
  email: string;

  //descripcion
  @BelongsTo(() => Idioma)
  descripcion: Idioma;

  @ForeignKey(() => Idioma)
  id_idioma: number;

  //id_empresa
  @ForeignKey(() => TipoEmpresa)
  id_tipo_empresa: number;
  @BelongsTo(() => TipoEmpresa)
  tipoEmpresa: TipoEmpresa;

  //id_subtipo_empresa
  @ForeignKey(() => SubtipoEmpresa)
  id_subtipo_empresa: number;
  @BelongsTo(() => SubtipoEmpresa)
  subtipoEmpresa: SubtipoEmpresa;

  //id_especialidad_empresa
  @ForeignKey(() => EspecialidadEmpresa)
  id_especialidad_empresa: number;
  @BelongsTo(() => EspecialidadEmpresa)
  especialidadEmpresa: EspecialidadEmpresa;

  @HasMany(() => Sucursal)
  sucursales: Sucursal[];

  @HasMany(() => ValoracionEmpresa)
  valoraciones: ValoracionEmpresa[];

  @HasMany(() => FotoEmpresa)
  fotos: FotoEmpresa[];

  @Column({ defaultValue: true })
  visible: boolean;

  @Column({ defaultValue: false })
  deleted: boolean;
}
