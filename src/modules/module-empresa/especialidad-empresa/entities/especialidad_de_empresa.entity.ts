import {
  Table,
  Column,
  Model,
  BelongsTo,
  ForeignKey,
  HasMany,
} from 'sequelize-typescript';
import { Idioma } from '../../../module-library/idioma/entity/idioma.entity';
import { TipoEmpresa } from '../../tipo-empresa/entities/tipo_de_empresa.entity';
import { Empresa } from '../../empresa/entities/empresa.entity';

@Table({ tableName: 'especialidad_de_empresa' })
export class EspecialidadEmpresa extends Model {
  @Column({ primaryKey: true, autoIncrement: true }) // autoIncrement: true
  id_especialidad_empresa: number;

  @BelongsTo(() => Idioma)
  idioma: Idioma;

  @ForeignKey(() => Idioma)
  id_idioma: number;

  @ForeignKey(() => TipoEmpresa)
  id_tipo_empresa: number;
  @BelongsTo(() => TipoEmpresa)
  tipoEmpresa: TipoEmpresa;

  @HasMany(() => Empresa)
  empresa: Empresa[];

  @Column({ defaultValue: false })
  deleted: boolean;
}
