import { Injectable, Inject, NotFoundException, Body } from '@nestjs/common';

import { Idioma } from 'src/modules/module-library/idioma/entity/idioma.entity';
import { TipoIdioma } from 'src/modules/module-library/idioma/enum/status.enum';
import { EspecialidadEmpresa } from './entities/especialidad_de_empresa.entity';
import { especialidadEmpresaSeedData } from './seeds/especialidad_empresa.seeds.data';
import { CreateEspecialidadEmpresaDto } from './dto/create-especialidad-empresa.dto';

@Injectable()
export class EspecialidadDeEmpresaService {
  constructor(
    @Inject('SUBTIPO_EMPRESA_REPOSITORY')
    private especialidadEmpresaRepository: typeof EspecialidadEmpresa,
  ) {}

  async seeds() {
    const count = await this.especialidadEmpresaRepository.count();
    if (count === 0) {
      for (const element of especialidadEmpresaSeedData) {
        await this.insert({
          es: element.es,
          en: element.en,
          fra: element.fra,
          id_tipo_empresa: element.id_tipo_empresa,
        });
      }
      console.log(
        'Todas las inserciones  de Especialidad de empresa se han completado.',
      );
    }
  }

  async insert(@Body() body: CreateEspecialidadEmpresaDto) {
    try {
      const traduccion = new Idioma();
      traduccion.es = body.es;
      traduccion.en = body.en;
      traduccion.fra = body.fra;
      traduccion.tipo = TipoIdioma.SUBTIPO__DE_EMPRESA;
      const traduccion_guardado = await traduccion.save();

      const especialidad_empresa = new EspecialidadEmpresa();
      especialidad_empresa.idioma = traduccion_guardado;
      especialidad_empresa.id_idioma = traduccion_guardado.id_idioma;
      especialidad_empresa.id_tipo_empresa = body.id_tipo_empresa;

      const especialidad_empresa_guardado = await especialidad_empresa.save();
      return especialidad_empresa_guardado;
    } catch (error) {
      throw new NotFoundException(error.message);
    }
  }
}
