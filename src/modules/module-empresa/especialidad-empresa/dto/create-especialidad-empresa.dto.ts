import { IsNotEmpty } from 'class-validator';

export class CreateEspecialidadEmpresaDto {
  @IsNotEmpty()
  es: string;
  en: string;
  fra: string;
  id_tipo_empresa: number;
}
