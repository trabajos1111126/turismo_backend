import { DataTypes } from 'sequelize';
import {
  Table,
  Column,
  Model,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';

import { Usuario } from 'src/modules/module-usuario/usuario/entities/usuario.entity';
import { Empresa } from '../../empresa/entities/empresa.entity';

@Table({ tableName: 'valoracion_empresa' })
export class ValoracionEmpresa extends Model {
  @Column({ primaryKey: true, autoIncrement: true })
  id_valoracion_empresa: number;

  @Column({ type: DataTypes.FLOAT })
  calificacion: number;

  @Column
  comentario: string;

  @Column
  idioma: string;

  @ForeignKey(() => Usuario)
  id_usuario: number;
  @BelongsTo(() => Usuario)
  usuario: Usuario;

  @ForeignKey(() => Empresa)
  id_empresa: number;
  @BelongsTo(() => Empresa)
  empresa: Empresa;

  @Column({ defaultValue: false })
  deleted: boolean;
}
