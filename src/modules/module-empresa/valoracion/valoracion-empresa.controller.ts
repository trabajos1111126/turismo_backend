import {
  Controller,
  Post,
  Request,
  UseGuards,
  Body,
  SetMetadata,
} from '@nestjs/common';

import { AuthGuard } from 'src/modules/module-usuario/auth/auth.guard';
import { ValoracionEmpresaService } from './valoracion-empresa.service';
import { CreateValoracionEmpresaDto } from './dto/create-valoracionEmpresa.dto';
import { RolUsuarioEnum } from 'src/modules/module-usuario/usuario/enum/usuario.enum';
@Controller('api/valoracion_empresa')
export class ValoracionEmpresaController {
  constructor(
    private readonly valoracionInventarioService: ValoracionEmpresaService,
  ) {}

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Post('')
  insert(@Request() req, @Body() body: CreateValoracionEmpresaDto) {
    return this.valoracionInventarioService.insert(
      req.id_usuario,
      req.idioma,
      body,
    );
  }
}
