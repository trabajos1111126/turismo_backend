import { ValoracionEmpresa } from './entities/valoracion-empresa.entity';

export const valoracionEmpresaProviders = [
  {
    provide: 'VALORACION_EMPRESA_REPOSITORY',
    useValue: ValoracionEmpresa,
  },
];
