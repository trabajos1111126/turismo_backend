import { IsNotEmpty, IsNumber, Length, Max, Min } from 'class-validator';

export class CreateValoracionEmpresaDto {
  @IsNotEmpty()
  @IsNumber()
  @Min(1)
  @Max(5)
  calificacion: number;

  @IsNotEmpty()
  @Length(0, 500)
  comentario: string;

  @IsNotEmpty()
  @IsNumber()
  @Min(1)
  id_empresa: number;
}
