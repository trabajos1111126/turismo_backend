import { Injectable, Inject } from '@nestjs/common';
import { ValoracionEmpresa } from './entities/valoracion-empresa.entity';
import { CreateValoracionEmpresaDto } from './dto/create-valoracionEmpresa.dto';

@Injectable()
export class ValoracionEmpresaService {
  constructor(
    @Inject('VALORACION_EMPRESA_REPOSITORY')
    private valoracionEmpresaRepository: typeof ValoracionEmpresa,
  ) {}
  async insert(
    id_usuario: number,
    idioma: string,
    valoracion: CreateValoracionEmpresaDto,
  ) {
    try {
      const valoracion_nuevo = new ValoracionEmpresa();
      valoracion_nuevo.calificacion = valoracion.calificacion;
      valoracion_nuevo.comentario = valoracion.comentario;
      valoracion_nuevo.idioma = idioma;
      valoracion_nuevo.id_usuario = id_usuario;
      valoracion_nuevo.id_empresa = valoracion.id_empresa;

      const respuesta = await valoracion_nuevo.save();
      return respuesta;
    } catch (error) {
      return error;
    }
  }
}
