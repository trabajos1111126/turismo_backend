import { Module } from '@nestjs/common';
import { ValoracionEmpresaService } from './valoracion-empresa.service';
import { ValoracionEmpresaController } from './valoracion-empresa.controller';
import { valoracionEmpresaProviders } from './valoracion-empresa.providers';

@Module({
  imports: [],
  controllers: [ValoracionEmpresaController],
  providers: [ValoracionEmpresaService, ...valoracionEmpresaProviders],
  exports: [],
})
export class ValoracionEmpresaModule {}
