export class CreateTipoEmpresaSeedDto {
  es: string;
  en: string;
  fra: string;
  mimetype: string;
  foto: string;
}
