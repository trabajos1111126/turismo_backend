import { Injectable, Inject, NotFoundException, Body } from '@nestjs/common';

import { Idioma } from 'src/modules/module-library/idioma/entity/idioma.entity';
import { CreateIdiomaDto } from 'src/modules/module-library/idioma/dto/create-idioma';
import { TipoIdioma } from 'src/modules/module-library/idioma/enum/status.enum';

import { TipoEmpresa } from './entities/tipo_de_empresa.entity';
import { tipoEmpresaSeedData } from './seeds/tipo_empresa.seeds.data';
import { UsuarioHeaderDto } from 'src/modules/module-usuario/auth/dto/header-usuario';
import { CreateTipoEmpresaSeedDto } from './dto/tipo.empresa.seeds.dto';
import { Adjunto } from 'src/modules/module-library/adjunto/entities/adjunto.entity';
@Injectable()
export class TipoEmpresaService {
  constructor(
    @Inject('TIPO_EMPRESA_REPOSITORY')
    private tipoEmpresaRepository: typeof TipoEmpresa,
  ) {}
  async getAll(req: UsuarioHeaderDto) {
    try {
      const tiposEmpresa = await this.tipoEmpresaRepository.findAll({
        attributes: ['id_tipo_empresa'],
        include: [
          {
            model: Idioma,
            attributes: [[req.idioma, 'idioma']],
          },
          {
            model: Adjunto,
            attributes: ['url_foto'],
          },
        ],
      });
      const data = await tiposEmpresa.map((item) => ({
        id_tipo_inventario: item.id_tipo_empresa,
        nombre: item.idioma.dataValues.idioma,
        url_foto: item.adjunto.dataValues.url_foto,
      }));
      console.log('tipos de emrpes = ' + data.length);
      return {
        data,
      };
    } catch (error) {
      console.log(error);
      throw new NotFoundException(error.message);
    }
  }

  async seeds() {
    const count = await this.tipoEmpresaRepository.count();
    if (count === 0) {
      for (const element of tipoEmpresaSeedData) {
        await this.insertSeed(element);
      }

      console.log(
        'Todas las inserciones  de Tipos de empresa se han completado.',
      );
    }
  }

  async insertSeed(@Body() body: CreateTipoEmpresaSeedDto) {
    try {
      const traduccion = new Idioma();
      traduccion.es = body.es;
      traduccion.en = body.en;
      traduccion.fra = body.fra;
      traduccion.tipo = TipoIdioma.TIPO_DE_EMPRESA;
      const traduccion_guardado = await traduccion.save();

      const adjunto = new Adjunto();
      adjunto.url_foto = 'images/tipo_empresa/' + body.foto;
      adjunto.descripcion = 'imagen de tipo de empresa';
      adjunto.mimetype = body.mimetype;
      const adjunto_guardado = await adjunto.save();

      const tipo_empresa = new TipoEmpresa();
      tipo_empresa.idioma = traduccion_guardado;
      tipo_empresa.id_idioma = traduccion_guardado.id_idioma;
      tipo_empresa.id_adjunto = adjunto_guardado.id_adjunto;

      const tipo_empresa_guardado = await tipo_empresa.save();
      return tipo_empresa_guardado;
    } catch (error) {
      throw new NotFoundException(error.message);
    }
  }
  async insert(@Body() body: CreateIdiomaDto) {
    try {
      const traduccion = new Idioma();
      traduccion.es = body.es;
      traduccion.en = body.en;
      traduccion.fra = body.fra;
      traduccion.tipo = TipoIdioma.TIPO_DE_EMPRESA;
      const traduccion_guardado = await traduccion.save();

      const tipo_empresa = new TipoEmpresa();
      tipo_empresa.idioma = traduccion_guardado;
      tipo_empresa.id_idioma = traduccion_guardado.id_idioma;

      const tipo_empresa_guardado = await tipo_empresa.save();
      return tipo_empresa_guardado;
    } catch (error) {
      throw new NotFoundException(error.message);
    }
  }
}
