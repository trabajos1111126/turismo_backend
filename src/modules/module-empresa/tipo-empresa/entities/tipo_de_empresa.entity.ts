import {
  Table,
  Column,
  Model,
  BelongsTo,
  ForeignKey,
  HasMany,
} from 'sequelize-typescript';
import { Idioma } from '../../../module-library/idioma/entity/idioma.entity';
import { EspecialidadEmpresa } from '../../especialidad-empresa/entities/especialidad_de_empresa.entity';
import { SubtipoEmpresa } from '../../subtipo-empresa/entities/subtipo_de_empresa.entity';
import { Empresa } from '../../empresa/entities/empresa.entity';
import { Adjunto } from 'src/modules/module-library/adjunto/entities/adjunto.entity';

@Table({ tableName: 'tipo_de_empresa' })
export class TipoEmpresa extends Model {
  @Column({ primaryKey: true, autoIncrement: true })
  id_tipo_empresa: number;

  @BelongsTo(() => Idioma)
  idioma: Idioma;

  @ForeignKey(() => Idioma)
  id_idioma: number;

  @HasMany(() => EspecialidadEmpresa)
  especialidades_de_empresa: EspecialidadEmpresa[];

  @HasMany(() => SubtipoEmpresa)
  subtipos_de_empresa: SubtipoEmpresa[];

  @HasMany(() => Empresa)
  empresa: Empresa[];

  @BelongsTo(() => Adjunto)
  adjunto: Adjunto;

  @ForeignKey(() => Adjunto)
  id_adjunto: number;

  @Column({ defaultValue: false })
  deleted: boolean;
}
