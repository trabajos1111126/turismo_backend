import { Module } from '@nestjs/common';
import { TipoEmpresaController } from './tipo_empresa.controller';
import { TipoEmpresaService } from './tipo_empresa.service';
import { tipoEmpresaProviders } from './tipo.empresa.providers';
@Module({
  imports: [],
  controllers: [TipoEmpresaController],
  providers: [TipoEmpresaService, ...tipoEmpresaProviders],
  exports: [],
})
export class TipoEmpresaModule {}
