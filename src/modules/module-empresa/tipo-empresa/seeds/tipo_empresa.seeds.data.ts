export const tipoEmpresaSeedData = [
  {
    es: 'HOSPEDAJE',
    en: 'ACCOMMODATION',
    fra: 'HÉBERGEMENT',
    foto: 'hospedaje.png',
    mimetype: 'image/png',
  },
  {
    es: 'AGENCIA DE VIAJE',
    en: 'TRAVEL AGENCY',
    fra: 'AGENCE DE VOYAGE',
    foto: 'agencia.png',
    mimetype: 'image/png',
  },
  {
    es: 'OPERADORAS DE TURISMO',
    en: 'TOUR OPERATOR',
    fra: 'TOUR OPÉRATEUR',
    foto: 'operadoras.png',
    mimetype: 'image/png',
  },
  {
    es: 'GASTRONOMIA',
    en: 'GASTRONOMY',
    fra: 'GASTRONOMIE',
    foto: 'gastronomia.png',
    mimetype: 'image/png',
  },
];
