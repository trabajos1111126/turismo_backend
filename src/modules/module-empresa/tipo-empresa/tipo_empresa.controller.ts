import {
  Body,
  Controller,
  Get,
  Post,
  Request,
  UseGuards,
  SetMetadata,
} from '@nestjs/common';
import { AuthGuard } from 'src/modules/module-usuario/auth/auth.guard';
import { CreateIdiomaDto } from 'src/modules/module-library/idioma/dto/create-idioma';
import { TipoEmpresaService } from './tipo_empresa.service';
import { RolUsuarioEnum } from 'src/modules/module-usuario/usuario/enum/usuario.enum';

@Controller('api/empresa/tipo')
export class TipoEmpresaController {
  constructor(private readonly tipoEmpresaService: TipoEmpresaService) {}

  @SetMetadata('roles', [
    RolUsuarioEnum.ADMINISTRADOR,
    RolUsuarioEnum.TURISTA,
    RolUsuarioEnum.ANONIMO,
  ])
  @UseGuards(AuthGuard)
  @Get('')
  getAll(@Request() req) {
    console.log('getAll Tipos de empresas');
    return this.tipoEmpresaService.getAll(req);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Post()
  insert(@Body() body: CreateIdiomaDto) {
    console.log('Agregar Tipo de empresa');
    return this.tipoEmpresaService.insert(body);
  }
}
