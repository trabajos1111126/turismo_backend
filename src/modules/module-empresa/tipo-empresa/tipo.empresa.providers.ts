import { TipoEmpresa } from './entities/tipo_de_empresa.entity';

export const tipoEmpresaProviders = [
  {
    provide: 'TIPO_EMPRESA_REPOSITORY',
    useValue: TipoEmpresa,
  },
];
