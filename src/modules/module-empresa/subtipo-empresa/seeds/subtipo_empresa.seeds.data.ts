export const subtipoEmpresaSeedData = [
  {
    id_tipo_empresa: 1,
    en: 'HOTELS',
    fra: 'HÔTELS',
    es: 'HOTELES',
  },
  {
    id_tipo_empresa: 1,
    en: 'APART HOTEL',
    fra: 'RÉSIDENCE HÔTELIÈRE',
    es: 'APART HOTEL',
  },
  {
    id_tipo_empresa: 1,
    en: 'BOUTIQUE HOTEL',
    fra: 'HÔTEL BOUTIQUE',
    es: 'HOTEL BOUTIQUE',
  },
  {
    id_tipo_empresa: 1,
    en: 'HOSTEL/RESIDENTIAL',
    fra: 'AUBERGE/RÉSIDENTIEL',
    es: 'HOSTAL/RESIDENCIAL',
  },
  {
    id_tipo_empresa: 1,
    en: 'LODGING',
    fra: 'HÉBERGEMENT',
    es: 'ALOJAMIENTO',
  },

  {
    id_tipo_empresa: 4,
    en: 'AUTHOR CUISINE',
    fra: "CUISINE D'AUTEUR",
    es: 'COCINA DE AUTOR',
  },
  {
    id_tipo_empresa: 4,
    en: 'FUSION',
    fra: 'FUSION',
    es: 'FUSIÓN',
  },
  {
    id_tipo_empresa: 4,
    en: 'NATIONAL CUISINE',
    fra: 'CUISINE NATIONALE',
    es: 'COMIDA NACIONAL',
  },

  {
    id_tipo_empresa: 4,
    en: 'GOURMET',
    fra: 'GOURMET',
    es: 'GOURMET',
  },
  {
    id_tipo_empresa: 4,
    en: 'THEMATIC',
    fra: 'THÉMATIQUE',
    es: 'TEMÁTICO',
  },
  {
    id_tipo_empresa: 4,
    en: 'GRILL',
    fra: 'GRIL',
    es: 'PARRILLA',
  },
  {
    id_tipo_empresa: 4,
    en: 'PIZZA AND PASTA',
    fra: 'PIZZA ET PÂTES',
    es: 'PIZZAS Y PASTA',
  },
  {
    id_tipo_empresa: 4,
    en: 'FAST FOOD',
    fra: 'RESTAURATION RAPIDE',
    es: 'RÁPIDA',
  },
  {
    id_tipo_empresa: 4,
    en: 'CAFETERIA AND SNACK',
    fra: 'CAFÉTÉRIA ET SNACK',
    es: 'CAFETERÍA Y SNACK',
  },
];
