import {
  Table,
  Column,
  Model,
  BelongsTo,
  ForeignKey,
  HasMany,
} from 'sequelize-typescript';
import { Idioma } from '../../../module-library/idioma/entity/idioma.entity';
import { TipoEmpresa } from '../../tipo-empresa/entities/tipo_de_empresa.entity';
import { Empresa } from '../../empresa/entities/empresa.entity';

@Table({ tableName: 'subtipo_de_empresa' })
export class SubtipoEmpresa extends Model {
  @Column({ primaryKey: true, autoIncrement: true })
  id_subtipo_empresa: number;

  @BelongsTo(() => Idioma)
  idioma: Idioma;

  @ForeignKey(() => Idioma)
  id_idioma: number;

  @ForeignKey(() => TipoEmpresa)
  id_tipo_empresa: number;
  @BelongsTo(() => TipoEmpresa)
  tipoEmpresa: TipoEmpresa;

  @HasMany(() => Empresa)
  empresa: Empresa[];

  @Column({ defaultValue: false })
  deleted: boolean;
}
