import { Injectable, Inject, NotFoundException, Body } from '@nestjs/common';

import { Idioma } from 'src/modules/module-library/idioma/entity/idioma.entity';
import { TipoIdioma } from 'src/modules/module-library/idioma/enum/status.enum';

import { SubtipoEmpresa } from './entities/subtipo_de_empresa.entity';
import { subtipoEmpresaSeedData } from './seeds/subtipo_empresa.seeds.data';
import { CreateSubtipoEmpresaDto } from './dto/create-subtipo-empresa';
@Injectable()
export class SubtipoEmpresaService {
  constructor(
    @Inject('SUBTIPO_EMPRESA_REPOSITORY')
    private subtipoEmpresaRepository: typeof SubtipoEmpresa,
  ) {}

  async seeds() {
    const count = await this.subtipoEmpresaRepository.count();
    if (count === 0) {
      for (const element of subtipoEmpresaSeedData) {
        await this.insert({
          es: element.es,
          en: element.en,
          fra: element.fra,
          id_tipo_empresa: element.id_tipo_empresa,
        });
      }
      console.log(
        'Todas las inserciones  de Subtipos de empresa se han completado.',
      );
    }
  }

  async insert(@Body() body: CreateSubtipoEmpresaDto) {
    try {
      const traduccion = new Idioma();
      traduccion.es = body.es;
      traduccion.en = body.en;
      traduccion.fra = body.fra;
      traduccion.tipo = TipoIdioma.SUBTIPO__DE_EMPRESA;
      const traduccion_guardado = await traduccion.save();
      const subtipo_empresa = new SubtipoEmpresa();
      subtipo_empresa.idioma = traduccion_guardado;
      subtipo_empresa.id_idioma = traduccion_guardado.id_idioma;
      subtipo_empresa.id_tipo_empresa = body.id_tipo_empresa;

      const subtipo_empresa_guardado = await subtipo_empresa.save();
      return subtipo_empresa_guardado;
    } catch (error) {
      throw new NotFoundException(error.message);
    }
  }
}
