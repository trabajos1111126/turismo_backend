import { IsNotEmpty, IsString } from 'class-validator';

export class UpdateRutaDto {
  @IsNotEmpty()
  id_ruta: number;

  @IsString()
  @IsNotEmpty()
  nombre: string;

  @IsString()
  @IsNotEmpty()
  descripcion_es;

  @IsString()
  @IsNotEmpty()
  descripcion_en;

  @IsString()
  @IsNotEmpty()
  descripcion_fra; // Assuming you have an IdiomaDto for Idioma entity

  @IsString()
  @IsNotEmpty()
  url_encuesta;
}
