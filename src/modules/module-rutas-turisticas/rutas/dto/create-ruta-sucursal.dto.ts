import { IsInt, IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class RutaSucursalCreateDto {
  @IsInt()
  @IsNotEmpty()
  id_ruta: number;

  @IsInt()
  @IsNotEmpty()
  id_sucursal: number;

  @IsString()
  polilyne: string;
}
