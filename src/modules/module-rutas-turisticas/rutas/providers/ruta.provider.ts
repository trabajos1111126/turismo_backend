import { Ruta } from '../entities/ruta.entity';

export const rutasProviders = [
  {
    provide: 'RUTA_REPOSITORY',
    useValue: Ruta,
  },
];
