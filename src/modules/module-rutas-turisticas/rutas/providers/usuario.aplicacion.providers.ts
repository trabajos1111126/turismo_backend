import { RutaSucursal } from '../entities/ruta-sucursal.entity';

export const usuarioAplicacionProviders = [
  {
    provide: 'RUTA_SUCURSAL_REPOSITORY',
    useValue: RutaSucursal,
  },
];
