import {
  Table,
  Column,
  Model,
  BelongsToMany,
  BelongsTo,
  ForeignKey,
} from 'sequelize-typescript';
import * as moment from 'moment';
import { Sucursal } from 'src/modules/module-empresa/sucursal/entities/sucursal.entity';
import { RutaSucursal } from './ruta-sucursal.entity';
import { Idioma } from 'src/modules/module-library/idioma/entity/idioma.entity';

@Table({ tableName: 'ruta' })
export class Ruta extends Model {
  @Column({ primaryKey: true, autoIncrement: true })
  id_ruta: number;

  @Column({ defaultValue: '' })
  nombre: string;

  @Column({ defaultValue: 'images/ruta_default.png' })
  logo: string;

  @Column({ defaultValue: '' })
  url_encuesta: string;

  //descripcion
  @BelongsTo(() => Idioma)
  descripcion: Idioma;

  @ForeignKey(() => Idioma)
  id_idioma: number;

  @BelongsToMany(() => Sucursal, () => RutaSucursal, 'id_ruta', 'id_sucursal')
  paradas: Sucursal[];

  @Column({ defaultValue: false })
  deleted: boolean;
}
