import {
  Table,
  Column,
  Model,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';
import { Sucursal } from 'src/modules/module-empresa/sucursal/entities/sucursal.entity';
import { Ruta } from './ruta.entity';
import { DataTypes } from 'sequelize';

@Table({ tableName: 'ruta_sucursal' })
export class RutaSucursal extends Model {
  @ForeignKey(() => Ruta)
  @Column({ field: 'id_ruta' }) // Indica explícitamente el nombre de la columna
  id_ruta: number;

  @ForeignKey(() => Sucursal)
  @Column({ field: 'id_sucursal' }) // Indica explícitamente el nombre de la columna
  id_sucursal: number;

  @BelongsTo(() => Ruta, 'id_ruta') // Indica explícitamente el nombre de la columna de la relación
  ruta: Ruta;

  @BelongsTo(() => Sucursal, 'id_sucursal') // Indica explícitamente el nombre de la columna de la relación
  sucursal: Sucursal;

  @Column
  puesto: number;

  @Column({ type: DataTypes.TEXT })
  polilyne: string;
}

/*
@BelongsTo(() => Usuario)
  usuario: Usuario;
  @ForeignKey(() => Usuario)
  @Column
  id_usuario: string;
*/
