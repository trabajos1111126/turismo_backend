import {
  Controller,
  Get,
  SetMetadata,
  UseGuards,
  Param,
  Body,
  Post,
  Patch,
  Delete,
  UseInterceptors,
  Request,
  UploadedFile,
} from '@nestjs/common';
import { AuthGuard } from 'src/modules/module-usuario/auth/auth.guard';
import { RolUsuarioEnum } from 'src/modules/module-usuario/usuario/enum/usuario.enum';
import { RutaService } from './ruta.service';
import { CreateRutaDto } from './dto/create-ruta.dto';
import { multerConfigRutaLogo } from 'src/config/multer.config';
import { FileInterceptor } from '@nestjs/platform-express';
import { UpdateRutaDto } from './dto/update-ruta.dto';
import { RutaSucursalCreateDto } from './dto/create-ruta-sucursal.dto';

@Controller('api/ruta')
export class RutaController {
  constructor(private readonly rutaService: RutaService) {}

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR, RolUsuarioEnum.TURISTA])
  @UseGuards(AuthGuard)
  @Get('')
  getAll(@Request() req) {
    console.log('getAll rutas');
    return this.rutaService.getAll(req);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR, RolUsuarioEnum.TURISTA])
  @UseGuards(AuthGuard)
  @Get(':id_ruta')
  async getUserById(@Param('id_ruta') id_ruta: number, @Request() req) {
    console.log('getById ruta  ' + id_ruta);
    return this.rutaService.getById(id_ruta, req);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR, RolUsuarioEnum.TURISTA])
  @UseGuards(AuthGuard)
  @Get(':id_ruta/fotos')
  async getFotosOfRuta(@Param('id_ruta') id_ruta: number, @Request() req) {
    console.log('getFotosOfRuta ruta  ' + id_ruta);
    return this.rutaService.getFotosOfRuta(id_ruta, req);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Post()
  insert(@Body() body: CreateRutaDto) {
    console.log('insert ruta');
    return this.rutaService.insert(body);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Patch()
  async update(@Body() updateRutaDto: UpdateRutaDto) {
    console.log('Update ruta');
    return this.rutaService.update(updateRutaDto);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Delete(':id')
  delete(@Param('id') id: number) {
    console.log('Delete ruta');
    return this.rutaService.delete(id);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Patch(':id/logo')
  @UseInterceptors(FileInterceptor('image', multerConfigRutaLogo))
  modificarLogo(
    @Param('id') id: number,
    @UploadedFile() file: Express.Multer.File,
  ) {
    console.log('ModificarLogo Ruta');
    return this.rutaService.modificarLogo(id, file);
  }
  /******************************************
   *      Rutas Sucursal
   *
   ******************************************/

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Post('sucursal')
  async agregarSucursal(@Body() rutaSucursalCreateDto: RutaSucursalCreateDto) {
    console.log('Agregamos una sucursal a la ruta');
    return this.rutaService.agregarRutaSucursal(rutaSucursalCreateDto);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Delete('sucursal/:id_ruta')
  async eliminarSucursal(@Param('id_ruta') id_ruta: number) {
    return this.rutaService.deleteRutaSucursal(id_ruta);
  }
}
