import { Module } from '@nestjs/common';
import { RutaController } from './ruta.controller';
import { RutaService } from './ruta.service';
import { rutasProviders } from './providers/ruta.provider';
import { LogModule } from 'src/modules/module-usuario/log/log.module';

@Module({
  imports: [LogModule],
  controllers: [RutaController],
  providers: [RutaService, ...rutasProviders],
})
export class RutaModule {}
