import {
  Injectable,
  Inject,
  NotFoundException,
  ConflictException,
  Body,
  UploadedFile,
} from '@nestjs/common';
import { Ruta } from './entities/ruta.entity';
import { CreateRutaDto } from './dto/create-ruta.dto';
import { Idioma } from 'src/modules/module-library/idioma/entity/idioma.entity';
import { TipoIdioma } from 'src/modules/module-library/idioma/enum/status.enum';
import { UsuarioHeaderDto } from 'src/modules/module-usuario/auth/dto/header-usuario';
import { UpdateRutaDto } from './dto/update-ruta.dto';
import { RutaSucursalCreateDto } from './dto/create-ruta-sucursal.dto';
import { Sucursal } from 'src/modules/module-empresa/sucursal/entities/sucursal.entity';
import { RutaSucursal } from './entities/ruta-sucursal.entity';
import { Model } from 'sequelize';
import { Empresa } from 'src/modules/module-empresa/empresa/entities/empresa.entity';
import { FotoEmpresa } from 'src/modules/module-library/foto_empresa/entities/foto_empresa.entity';
import { LogService } from 'src/modules/module-usuario/log/log.service';

@Injectable()
export class RutaService {
  constructor(
    @Inject('RUTA_REPOSITORY')
    private rutaRepository: typeof Ruta,
    private readonly logService: LogService,
  ) {}

  async getAll(req: UsuarioHeaderDto) {
    try {
      const rutas = await this.rutaRepository.findAll({
        //include: ['id_ruta','nombre','logo','url_encuesta'],
        include: [
          {
            model: Idioma,
            attributes: [[req.idioma, 'idioma']],
          },
          {
            model: Sucursal,
          },
        ],
        where: {
          deleted: false,
        },
      });

      const data = rutas.map((item) => ({
        id_ruta: item.id_ruta,
        nombre: item.nombre,
        logo: item.logo,
        url_encuesta: item.url_encuesta,
        descripcion: item.descripcion?.dataValues.idioma,
      }));

      // Realiza el substring en la descripción para obtener los primeros 30 caracteres
      data.forEach((item) => {
        if (item.descripcion) {
          if (item.descripcion.length > 100)
            item.descripcion = item.descripcion.substring(0, 100) + '..';
        }
      });

      return {
        data,
      };
    } catch (error) {
      this.logService.insertLog(
        'ERROR',
        'ruta:getFotosOfRuta',
        error.message,
        '',
      );
    }
  }
  async getFotosOfRuta(id: number, req: UsuarioHeaderDto) {
    try {
      const ruta = await this.rutaRepository.findByPk(id, {
        attributes: [],
        include: [
          {
            model: Sucursal,
            attributes: ['id_sucursal'],
            include: [
              {
                model: Empresa,
                attributes: ['id_empresa'],
                include: [
                  {
                    model: FotoEmpresa,
                    attributes: ['url_foto'],
                    where: {
                      deleted: false,
                    },
                    required: false,
                  },
                ],
              },
            ],
            through: { attributes: [] },
          },
        ],
      });
      const fotos = [];
      if (ruta) {
        for (let i = 0; i < ruta.paradas.length; i++) {
          const fotos_empresa = ruta.paradas[i].empresa.fotos;
          for (let j = 0; j < fotos_empresa.length; j++) {
            //console.log(fotos_empresa[j]);
            fotos.push({
              id_empresa: ruta.paradas[i].empresa.id_empresa,
              url_foto: fotos_empresa[j].url_foto,
            });
          }
        }
        return fotos;
      } else {
        return [];
      }
    } catch (error) {
      this.logService.insertLog(
        'ERROR',
        'ruta:getFotosOfRuta',
        error.message,
        '',
      );
      return [];
    }
  }
  async getById(id, req: UsuarioHeaderDto) {
    try {
      const ruta = await this.rutaRepository.findByPk(id, {
        attributes: ['id_ruta', 'nombre', 'logo', 'url_encuesta'],
        include: [
          {
            model: Idioma,
            attributes: [[req.idioma, 'txt']],
          },
          {
            model: Sucursal,
            attributes: [
              'id_sucursal',
              'direccion',
              'contactos',
              'macrodistrito',
              'latitud',
              'longitud',
            ],
            include: [
              {
                model: Empresa,
                attributes: [
                  'id_empresa',
                  'nombre',
                  'url_logo',
                  'url_web',
                  'email',
                ],
                include: [
                  {
                    model: Idioma,
                    attributes: [[req.idioma, 'txt']],
                  },
                ],
              },
            ],
            through: { attributes: ['polilyne', 'puesto'] },
          },
        ],
      });

      if (ruta && ruta.paradas && ruta.paradas.length > 0) {
        ruta.paradas.sort((a, b) => {
          const puestoA = parseInt(a?.dataValues.RutaSucursal.puesto);
          const puestoB = parseInt(b?.dataValues.RutaSucursal.puesto);
          return puestoA - puestoB;
        });
      } else {
        console.log('La ruta no se encontró o no tiene sucursales.');
      }
      return ruta;
    } catch (error) {
      console.error('Error al buscar empresa:', error);
      this.logService.insertLog('ERROR', 'ruta:getById', error.message, '');
      throw new NotFoundException('Error al buscar empresa:', error.message);
    }
  }

  async insert(body: CreateRutaDto) {
    try {
      const traduccion = new Idioma();
      traduccion.es = body.descripcion_es;
      traduccion.en = body.descripcion_en;
      traduccion.fra = body.descripcion_fra;
      traduccion.tipo = TipoIdioma.DESCRIPCION_DE_RUTA;
      const traduccion_guardado = await traduccion.save();

      const ruta = new Ruta();
      ruta.nombre = body.nombre;
      ruta.url_encuesta = body.url_encuesta;
      ruta.id_idioma = traduccion_guardado.id_idioma;
      ruta.descripcion = traduccion_guardado;
      const ruta_guardado = await ruta.save();
      return ruta_guardado;
    } catch (error) {
      this.logService.insertLog('ERROR', 'ruta:insert', error.message, '');
      throw new NotFoundException(
        'error al insertar una empresa',
        error.message,
      );
    }
  }

  async update(updateRutaDto: UpdateRutaDto) {
    try {
      const ruta = await this.rutaRepository.findByPk(updateRutaDto.id_ruta);

      if (!ruta) {
        throw new NotFoundException('Ruta no encontrada');
      }

      ruta.nombre = updateRutaDto.nombre;
      ruta.url_encuesta = updateRutaDto.url_encuesta;
      //Buscamos la traduccion
      let traduccion = await Idioma.findByPk(ruta.id_idioma);
      if (!traduccion) {
        traduccion = new Idioma();
      }

      traduccion.es = updateRutaDto.descripcion_es;
      traduccion.en = updateRutaDto.descripcion_en;
      traduccion.fra = updateRutaDto.descripcion_fra;
      traduccion.tipo = TipoIdioma.DESCRIPCION_DE_EMPRESA;
      const traduccion_guardado = await traduccion.save();

      const ruta_guardado = await ruta.save();

      return ruta_guardado;
    } catch (error) {
      this.logService.insertLog('ERROR', 'ruta:update', error.message, '');
      console.error('Error al actualizar usuario:', error.message);
      throw new NotFoundException(
        'Error al actualizar usuario:',
        error.message,
      );
    }
  }

  async delete(id: number) {
    try {
      const empresa = await this.rutaRepository.findByPk(id);
      if (!empresa) {
        throw new NotFoundException('Ruta no encontrada');
      }
      empresa.deleted = true; // Marcamos la entidad como borrada
      await empresa.save();
      return 'La Ruta se ha borrado exitosamente';
    } catch (error) {
      console.error('Error al borrar ruta:', error);
      this.logService.insertLog('ERROR', 'ruta:delete', error.message, '');
      throw new NotFoundException('Error al borrar ruta:', error.message);
    }
  }

  async modificarLogo(
    id_ruta: number,
    @UploadedFile() file: Express.Multer.File,
  ) {
    const ruta = await this.rutaRepository.findByPk(id_ruta);

    if (!ruta) {
      console.log(`No se encontró la ruta con el id ${id_ruta}.`);
      return;
    }
    ruta.logo = 'images/ruta/' + file.filename;
    const ruta_guardado = await ruta.save();
    return ruta_guardado;
  }

  /******************************************
   *      Rutas Sucursal
   *
   ******************************************/
  async agregarRutaSucursal(rutaSucursalCreateDto: RutaSucursalCreateDto) {
    const { id_ruta, id_sucursal, polilyne } = rutaSucursalCreateDto;

    // Buscar la instancia de la ruta por su ID
    const ruta = await Ruta.findByPk(id_ruta, {
      include: 'paradas', // 'paradas' debe coincidir con el nombre de la relación en el modelo Ruta
    });

    if (!ruta) {
      // Manejar el caso en que la ruta no se encuentre
      throw new NotFoundException(`Ruta con ID ${id_ruta} no encontrada`);
    }

    // Buscar la instancia de la sucursal por su ID
    const sucursal = await Sucursal.findByPk(id_sucursal);

    if (!sucursal) {
      // Manejar el caso en que la sucursal no se encuentre
      throw new NotFoundException(
        `Sucursal con ID ${id_sucursal} no encontrada`,
      );
    }

    if (await this.estaParadaInRuta(id_ruta, id_sucursal)) {
      return { message: 'La sucursal ya está en la lista de paradas.' };
    }

    // Utilizar el método `$add` para agregar la sucursal a la ruta
    const puesto = ruta.paradas.length + 1;
    console.log('puesto = ' + puesto);
    await ruta.$add('paradas', sucursal, { through: { polilyne, puesto } });

    // Devolver una respuesta adecuada, por ejemplo, un mensaje de éxito
    return { message: 'Sucursal agregada a la ruta correctamente' };
  }

  async deleteRutaSucursal(id_ruta: number) {
    try {
      const rutaSucursal = await RutaSucursal.findOne({
        where: { id_ruta: id_ruta },
        order: [['puesto', 'DESC']], // Find the highest puesto value for the given id_ruta
      });

      if (!rutaSucursal) {
        throw new NotFoundException(
          `No RutaSucursal entry found for id_ruta = ${id_ruta}`,
        );
      }

      // Delete the entry with the highest puesto value for the given id_ruta
      await rutaSucursal.destroy();
      return { message: 'La sucursal fue eliminada de la ruta correctamente' };
    } catch (error) {
      console.error('Error al borrar la parada de la ruta:', error);
      throw new NotFoundException(
        'Error al borrar la parada de la ruta:',
        error.message,
      );
    }
  }

  async estaParadaInRuta(id_ruta: number, id_sucursal: number) {
    const ruta_sucursal = await RutaSucursal.findOne({
      where: { id_ruta: id_ruta, id_sucursal: id_sucursal },
    });

    console.log(ruta_sucursal);
    if (ruta_sucursal) {
      console.log('es parada');
      return true;
    } else {
      console.log('No es parada');
      return false;
    }
    //return ruta_sucursal;
  }

  async countRutaOccurrences(idRuta: number) {
    const count = await RutaSucursal.count({
      where: { id_ruta: idRuta },
    });
    return count;
  }
}
