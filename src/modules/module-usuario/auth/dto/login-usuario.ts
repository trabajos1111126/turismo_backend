export class UsuarioDto {
  nombre: string;
  url_imagen_perfil: string;
  correo: string;
  uuid: string;
}
