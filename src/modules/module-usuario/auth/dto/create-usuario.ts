import { IsEmail, IsNotEmpty, Length } from 'class-validator';

export class CreateUsuarioDto {
  //@IsNotEmpty({ message: 'El campo nombre no puede estar vacío' })
  nombre: string;

  //@IsNotEmpty()
  //@Length(5, 256)
  url_imagen_perfil: string;

  //@IsEmail()
  //@IsNotEmpty()
  //@Length(5, 256)
  correo: string;

  @IsNotEmpty({ message: 'El campo UUID no puede estar vacío' })
  uuid: string;
}
