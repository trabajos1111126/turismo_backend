export class UpdateUsuarioDto {
  id_usuario: number;
  nombre: string;
  url_imagen_perfil: string;
  correo: string;
  uuid: string;
}
