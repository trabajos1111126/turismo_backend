import { IsEmail, IsNotEmpty, Length } from 'class-validator';

export class UsuarioHeaderDto {
  @IsNotEmpty()
  @Length(5, 256)
  token: string;

  @IsNotEmpty()
  @Length(5, 256)
  idioma: string;
}
