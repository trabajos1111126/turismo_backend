import { Injectable, NotFoundException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { CreateUsuarioDto } from './dto/create-usuario';
import { UsuarioService } from '../usuario/usuario.service';
import * as childProcess from 'child_process';
import * as util from 'util';
import * as fs from 'fs';
import { LogService } from '../log/log.service';
@Injectable()
export class AuthService {
  constructor(
    private usuarioService: UsuarioService,
    private jwtService: JwtService,
    private readonly logService: LogService,
  ) {}

  async signIn(createUsuarioDto: CreateUsuarioDto): Promise<any> {
    try {
      let usuario = await this.usuarioService.findByUuid(createUsuarioDto.uuid);
      let status = 'El usuario ya estaba registrado';
      if (!usuario) {
        //en este caso tenemos que agregar usuarios
        if (createUsuarioDto.nombre == null) createUsuarioDto.nombre = '';
        if (createUsuarioDto.url_imagen_perfil == null)
          createUsuarioDto.url_imagen_perfil = '';
        if (createUsuarioDto.correo == null) createUsuarioDto.correo = '';
        usuario = await this.usuarioService.crearUsuario(createUsuarioDto);
        status = 'Usuario nuevo';
      } else {
        if (usuario.deleted) {
          throw new NotFoundException('el usuario tiene la cuenta eliminada');
        }
      }
      console.log(status);
      const payload = { id_usuario: usuario.id_usuario };
      return {
        access_token: await this.jwtService.signAsync(payload),
        id_usuario: usuario.id_usuario,
        status,
      };
    } catch (error) {
      console.log(error);
      this.logService.insertLog('ERROR', 'auth.service', error.message, '');
      throw new NotFoundException(error.message);
    }
  }
}
