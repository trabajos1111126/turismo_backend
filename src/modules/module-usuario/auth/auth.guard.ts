import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Request } from 'express';
import * as dotenv from 'dotenv';
import { Reflector } from '@nestjs/core';
import { Usuario } from '../usuario/entities/usuario.entity';
import { Log } from '../log/entities/log.entity';
dotenv.config();

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private jwtService: JwtService, private reflector: Reflector) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    //console.log(context);
    const request = await context.switchToHttp().getRequest();
    try {
      const roles = this.reflector.get<string[]>('roles', context.getHandler());
      const token = this.extractTokenFromHeader(request);
      const idioma = this.estractIdiomaFromHeader(request);
      //console.log('token = ' + token);
      //console.log('idioma = ' + idioma);
      if (!token) {
        await this.saveLog(
          'ERROR',
          request.url,
          'Token invalido o inexistente en el header',
          'canActivate',
        );
        throw new UnauthorizedException('Usuario no Autorizado');
      }
      if (!idioma) {
        await this.saveLog(
          'ERROR',
          request.url,
          'Idioma invalido o inexistente en el header',
          'canActivate',
        );
        throw new UnauthorizedException('Usuario no Autorizado');
      }

      const payload = await this.jwtService.verifyAsync(token, {
        secret: process.env.JWT_SECRET,
      });

      if (!payload) {
        await this.saveLog(
          'ERROR',
          request.url,
          'No se puede decodificar el token',
          'canActivate',
        );
        throw new UnauthorizedException('Usuario no Autorizado');
      }

      /*
      buscamos usuario si existe
       */
      const usuario = await Usuario.findByPk(payload.id_usuario);
      if (!usuario) {
        await this.saveLog(
          'ERROR',
          request.url,
          'No existe el usuario',
          'canActivate',
        );
        throw new UnauthorizedException('Usuario no Autorizado');
      }
      if (this.comparaRoles(usuario.categoria, roles)) {
        request['user'] = payload;
        request['idioma'] = idioma;
        request['rol'] = usuario.categoria;
        request['id_usuario'] = payload.id_usuario;
      } else {
        await this.saveLog(
          'ERROR',
          request.url,
          'Rol no encontardo',
          'canActivate',
        );
        throw new UnauthorizedException('Usuario no Autorizado');
      }
    } catch (error) {
      //console.log(error);
      await this.saveLog(
        'ERROR',
        request.url,
        'cath: ' + error.message,
        'canActivate',
      );
      throw new UnauthorizedException('Usuario no Autorizado');
    }
    return true;
  }

  private extractTokenFromHeader(request: Request): string | undefined {
    const [type, token] = request.headers.authorization?.split(' ') ?? [];
    return type === 'Bearer' ? token : undefined;
  }
  private estractIdiomaFromHeader(request: Request): string | undefined {
    const idioma = request.headers.idioma;
    return idioma === 'es' || idioma === 'en' || idioma === 'fra'
      ? idioma
      : undefined;
  }
  private comparaRoles(rol: string, roles: string[]): boolean {
    let sw = false;
    for (let i = 0; i < roles.length; i++) {
      if (roles[i] == rol) {
        sw = true;
        break;
      }
    }
    return sw;
  }
  async saveLog(tipo: string, ruta: string, mensaje: string, params: string) {
    console.log('Generamos Log');
    const log = new Log();
    log.tipo = tipo;
    log.ruta = 'backend:' + ruta;
    log.mensaje = mensaje;
    log.params = params;
    await log.save();
    //console.log(l);
  }
}
