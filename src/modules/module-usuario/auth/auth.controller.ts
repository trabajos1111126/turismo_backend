import { Body, Controller, Post, HttpCode, HttpStatus } from '@nestjs/common';
import { AuthService } from './auth.service';
import { CreateUsuarioDto } from './dto/create-usuario';
@Controller('api/auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @HttpCode(HttpStatus.OK)
  @Post('login')
  signIn(@Body() insertUsuarioDto: CreateUsuarioDto) {
    console.log('SignIn Usuario');
    return this.authService.signIn(insertUsuarioDto);
  }
}
