import { Table, Column, Model, HasMany, DataType } from 'sequelize-typescript';
import { ValoracionEmpresa } from 'src/modules/module-empresa/valoracion/entities/valoracion-empresa.entity';
import { ValoracionInventario } from 'src/modules/module-inventario-turistico/valoracion/entities/valoracion-inventario.entity';
import { RolUsuarioEnum } from '../enum/usuario.enum';

@Table({ tableName: 'usuario' })
export class Usuario extends Model {
  @Column({ primaryKey: true, autoIncrement: true })
  id_usuario: number;

  @Column
  nombre: string;

  @Column
  url_imagen_perfil: string;

  @Column
  correo: string;

  @Column
  uuid: string;

  @Column({
    type: DataType.ENUM(...Object.values(RolUsuarioEnum)),
    defaultValue: RolUsuarioEnum.TURISTA,
  })
  categoria: RolUsuarioEnum;

  @HasMany(() => ValoracionInventario)
  valoraciones_inventarios: ValoracionInventario[];

  @HasMany(() => ValoracionEmpresa)
  valoraciones_empresas: ValoracionEmpresa[];

  @Column({ defaultValue: false })
  deleted: boolean;
}
