import { Module } from '@nestjs/common';
import { UsuarioService } from './usuario.service';
import { usuarioProviders } from './usuario.providers';

@Module({
  providers: [UsuarioService, ...usuarioProviders],
  exports: [UsuarioService],
})
export class UsuarioModule {}
