import { Body, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { Usuario } from 'src/modules/module-usuario/usuario/entities/usuario.entity';
import { CreateUsuarioDto } from '../auth/dto/create-usuario';
import { RolUsuarioEnum } from './enum/usuario.enum';

// This should be a real class/interface representing a user entity
export type User = any;

@Injectable()
export class UsuarioService {
  constructor(
    @Inject('USUARIO_REPOSITORY')
    private usuarioRepository: typeof Usuario,
  ) {}

  async crearUsuario(createUsuarioDto: CreateUsuarioDto): Promise<Usuario> {
    const usuario = new Usuario();
    usuario.nombre = createUsuarioDto.nombre;
    usuario.correo = createUsuarioDto.correo;
    usuario.url_imagen_perfil = createUsuarioDto.url_imagen_perfil;
    usuario.uuid = createUsuarioDto.uuid;
    return usuario.save();
  }

  async findByUuid(uuid: string): Promise<Usuario> {
    return this.usuarioRepository.findOne({ where: { uuid } });
  }

  async InsertAdmin() {
    const count = await this.usuarioRepository.count();
    if (count === 0) {
      const usuario = new Usuario();
      usuario.nombre = process.env.ADMIN_NOMBRE;
      usuario.url_imagen_perfil = '';
      usuario.correo = process.env.ADMIN_CORREO;
      usuario.uuid = process.env.ADMIN_UUID;
      usuario.categoria = RolUsuarioEnum.ADMINISTRADOR;
      await usuario.save();
      console.log('Adminsitrador Agregado.');
      //Agregamos modo libre
      const usuario2 = new Usuario();
      usuario2.nombre = '';
      usuario2.url_imagen_perfil = '';
      usuario2.correo = '';
      usuario2.uuid = process.env.ADMIN_UUID_ANONIMO;
      usuario2.categoria = RolUsuarioEnum.ANONIMO;
      await usuario.save();
    }
  }
}
