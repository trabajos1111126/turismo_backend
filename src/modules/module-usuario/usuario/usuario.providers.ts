import { Usuario } from 'src/modules/module-usuario/usuario/entities/usuario.entity';

export const usuarioProviders = [
  {
    provide: 'USUARIO_REPOSITORY',
    useValue: Usuario,
  },
];
