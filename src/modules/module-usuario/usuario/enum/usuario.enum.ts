export enum RolUsuarioEnum {
  ADMINISTRADOR = 'ADMIN',
  COLABORADOR = 'colaborador',
  TURISTA = 'turista',
  ANONIMO = 'anonimo',
}
