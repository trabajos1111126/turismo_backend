import { Controller, Get, Body, Post, Request } from '@nestjs/common';
import { LogService } from './log.service';
import { CreateLogDto } from './dto/create-log';

@Controller('api/log')
export class LogController {
  constructor(private readonly logService: LogService) {}

  @Get('/json')
  getAllJson(@Request() req) {
    console.log('getAll logs json');
    return this.logService.getAllJson();
  }
  @Get('/vista')
  getAllVista(@Request() req) {
    console.log('getAll logs vista');
    return this.logService.getAllVista();
  }

  @Get('/version-apk')
  checkLatestVersion(@Request() req) {
    console.log('checkLatestVersion');
    return this.logService.checkLatestVersion();
  }
  @Post()
  insert(@Body() log: CreateLogDto) {
    return this.logService.insert(log);
  }
}
