import { IsEmail, IsNotEmpty, Length } from 'class-validator';

export class CreateLogDto {
  tipo: string;
  columna: string;
  ruta: string;
  mensaje: string;
  params: string;
}
