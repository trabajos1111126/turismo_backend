import { Table, Column, Model, HasMany, DataType } from 'sequelize-typescript';

@Table({ tableName: 'log' })
export class Log extends Model {
  @Column({ primaryKey: true, autoIncrement: true })
  id_log: number;

  @Column
  nombre: string;

  @Column
  tipo: string;

  @Column
  ruta: string;

  @Column
  mensaje: string;

  @Column
  params: string;
}
