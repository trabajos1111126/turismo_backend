import { Module } from '@nestjs/common';
import { LogService } from './log.service';
import { logProviders } from './log.providers';
import { LogController } from './log.controller';

@Module({
  controllers: [LogController],
  providers: [LogService, ...logProviders],
  exports: [LogService],
})
export class LogModule {}
