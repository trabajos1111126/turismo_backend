import { Log } from './entities/log.entity';

export const logProviders = [
  {
    provide: 'LOG_REPOSITORY',
    useValue: Log,
  },
];
