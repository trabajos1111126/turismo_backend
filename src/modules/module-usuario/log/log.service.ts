import { Body, Inject, Injectable, NotFoundException } from '@nestjs/common';

import { Log } from './entities/log.entity';
import { CreateLogDto } from './dto/create-log';
import axios from 'axios';

// This should be a real class/interface representing a user entity
export type User = any;

@Injectable()
export class LogService {
  constructor(
    @Inject('LOG_REPOSITORY')
    private logRepository: typeof Log,
  ) {}

  async insert(createLogDto: CreateLogDto): Promise<Log> {
    const log = new Log();
    log.tipo = createLogDto.tipo;
    log.ruta = createLogDto.ruta;
    log.mensaje = createLogDto.mensaje;
    log.params = createLogDto.params;
    return log.save();
  }
  async insertLog(
    tipo: string,
    ruta: string,
    mensaje: string,
    params: string,
  ): Promise<Log> {
    const log = new Log();
    log.tipo = tipo;
    log.ruta = ruta;
    log.mensaje = mensaje;
    log.params = params;
    return log.save();
  }
  async getAllJson() {
    const c = await this.logRepository.findAll();
    return c;
  }
  async getAllVista() {
    const resultado = await Log.findAll({
      order: [['createdAt', 'DESC']], // Ordenar por fecha de creación en orden descendente
      limit: 100, // Limitar los resultados a 100 elementos
    });
    //return c;
    let texto = `<!DOCTYPE html>
    <html>
    <head>
      <title>Logs</title>
    </head>
    <body>
      <center><h1>Logs</h1></center>
      <table border="1" style="margin: 0 auto; border-collapse: collapse;">
        <thead>
          <tr>
            <th style="padding: 8px; border: 1px solid black;">ID</th>
            <th style="padding: 8px; border: 1px solid black;">Tipo</th>
            <th style="padding: 8px; border: 1px solid black;">Ruta</th>
            <th style="padding: 8px; border: 1px solid black;">Mensaje</th>
            <th style="padding: 8px; border: 1px solid black;">Params</th>
            <th style="padding: 8px; border: 1px solid black;">CreateAt</th>
          </tr>
        </thead>
        <tbody>`;

    for (let i = 0; i < resultado.length; i++) {
      texto =
        texto +
        `<tr>
      <td style="padding: 8px; border: 1px solid black;">${resultado[i].id_log}</td>
      <td style="padding: 8px; border: 1px solid black;">${resultado[i].tipo}</td>
      <td style="padding: 8px; border: 1px solid black;">${resultado[i].ruta}</td>
      <td style="padding: 8px; border: 1px solid black;">${resultado[i].mensaje}</td>
      <td style="padding: 8px; border: 1px solid black;">${resultado[i].params}</td>
      <td style="padding: 8px; border: 1px solid black;">${resultado[i].createdAt}</td>
    </tr>`;
    }

    texto =
      texto +
      `</tbody>
      </table>
    </body>
    </html>`;
    return texto;
  }
  async checkLatestVersion(): Promise<string> {
    try {
      const response = await axios.get(
        `https://play.google.com/store/apps/details?id=pdf.tap.scanner&hl=en`,
      );

      console.log('Response 1');
      console.log(response);
      console.log('Response 2');
      // Aquí deberás procesar la respuesta para extraer la versión disponible
      // Esto puede variar dependiendo de la estructura de la página y los datos que proporciona Google Play Store

      // Ejemplo simplificado: buscar la cadena de texto con la versión
      const versionRegex = /"(\d+\.\d+\.\d+)"/;
      const match = response.data.match(versionRegex);

      if (match && match[1]) {
        console.log(match[1]);
        return match[1]; // Devuelve la versión encontrada
      }

      return 'No se pudo encontrar la versión';
    } catch (error) {
      // Manejo de errores
      console.error('Error al obtener la versión:', error);
      throw new Error('Error al obtener la versión');
    }
  }
}
