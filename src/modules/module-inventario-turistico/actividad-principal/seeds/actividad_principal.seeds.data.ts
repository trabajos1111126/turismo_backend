export const actividadPrincipalInventarioSeedData = [
  {
    es: 'Agroturismo',
    en: 'Agrotourism',
    fra: 'Agrotourisme',
  },
  {
    es: 'City tour',
    en: 'City Tour',
    fra: 'Visite de la ville',
  },
  {
    es: 'Degustación',
    en: 'Tasting',
    fra: 'Dégustation',
  },
  {
    es: 'Escalada en roca',
    en: 'Rock Climbing',
    fra: 'Escalade en rocher',
  },
  {
    es: 'Fotografía urbana',
    en: 'Urban Photography',
    fra: 'Photographie urbaine',
  },
  {
    es: 'Fotografía del paisaje',
    en: 'Landscape Photography',
    fra: 'Photographie de paysage',
  },
  {
    es: 'Hiking',
    en: 'Hiking',
    fra: 'Randonnée',
  },
  {
    es: 'Montañismo',
    en: 'Mountaineering',
    fra: 'Alpinisme',
  },
  {
    es: 'Observación de fauna y flora',
    en: 'Fauna and Flora Observation',
    fra: 'Observation de la faune et de la flore',
  },
  {
    es: 'Presencia de la entrada',
    en: 'Entrance Presence',
    fra: "Présence de l'entrée",
  },
  {
    es: 'Presencia en festividad',
    en: 'Presence in Festivity',
    fra: 'Présence en festivité',
  },
  {
    es: 'Práctica de golf',
    en: 'Golf Practice',
    fra: 'Pratique du golf',
  },
  {
    es: 'Recorrido',
    en: 'Tour',
    fra: 'Visite',
  },
  {
    es: 'Recorrido Urbano',
    en: 'Urban Tour',
    fra: 'Visite urbaine',
  },
  {
    es: 'Rappel',
    en: 'Rappelling',
    fra: 'Rappel',
  },
  {
    es: 'Recorrido artesanal',
    en: 'Artisanal Tour',
    fra: 'Visite artisanale',
  },
  {
    es: 'Recorrido interno',
    en: 'Internal Tour',
    fra: 'Visite interne',
  },
  {
    es: 'Recreación familiar',
    en: 'Family Recreation',
    fra: 'Loisirs en famille',
  },
  {
    es: 'Senderismo',
    en: 'Hiking',
    fra: 'Randonnée pédestre',
  },
  {
    es: 'Trekking',
    en: 'Trekking',
    fra: 'Trekking',
  },
  {
    es: 'Visita guiada',
    en: 'Guided Tour',
    fra: 'Visite guidée',
  },
  {
    es: 'Visita independiente',
    en: 'Independent Visit',
    fra: 'Visite indépendante',
  },
  {
    es: 'Urbano',
    en: 'Urban',
    fra: 'Urbain',
  },
];
