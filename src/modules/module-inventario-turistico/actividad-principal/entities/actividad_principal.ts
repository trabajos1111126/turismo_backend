import {
  Table,
  Column,
  Model,
  BelongsTo,
  ForeignKey,
} from 'sequelize-typescript';
import { Idioma } from '../../../module-library/idioma/entity/idioma.entity';

@Table({ tableName: 'actividad_principal' })
export class ActividadPrincipal extends Model {
  @Column({ primaryKey: true, autoIncrement: true })
  id_actividad_principal: number;

  @BelongsTo(() => Idioma)
  idioma: Idioma;

  @ForeignKey(() => Idioma)
  id_idioma: number;

  @Column({ defaultValue: false })
  deleted: boolean;
}
