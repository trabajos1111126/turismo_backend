import {
  Controller,
  Get,
  Post,
  Request,
  UseGuards,
  Body,
  SetMetadata,
} from '@nestjs/common';

import { CreateIdiomaDto } from 'src/modules/module-library/idioma/dto/create-idioma';
import { ActividadPrincipalService } from './actividad.principal.service';
import { AuthGuard } from 'src/modules/module-usuario/auth/auth.guard';
import { RolUsuarioEnum } from 'src/modules/module-usuario/usuario/enum/usuario.enum';

@Controller('api/actividad_principal')
export class ActividadPrincipalController {
  constructor(
    private readonly actividadPrincipalService: ActividadPrincipalService,
  ) {}

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Get('')
  getAll(@Request() req) {
    console.log('getAll actividad_principal inventario');
    return this.actividadPrincipalService.getAll(req);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Post()
  insert(@Body() body: CreateIdiomaDto) {
    console.log('Insert actividad_principal inventario');
    return this.actividadPrincipalService.insert(body);
  }
}
