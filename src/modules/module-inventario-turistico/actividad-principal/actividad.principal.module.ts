import { Module } from '@nestjs/common';
import { ActividadPrincipalService } from './actividad.principal.service';
import { actividadPrincipalProviders } from './actividad.principal.providers';
import { ActividadPrincipalController } from './actividad.principal.controller';

@Module({
  imports: [],
  controllers: [ActividadPrincipalController],
  providers: [ActividadPrincipalService, ...actividadPrincipalProviders],
  exports: [],
})
export class ActividadPrincipalModule {}
