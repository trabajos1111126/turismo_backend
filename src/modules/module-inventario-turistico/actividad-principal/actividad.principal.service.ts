import { Injectable, Inject, NotFoundException } from '@nestjs/common';
import { ActividadPrincipal } from 'src/modules/module-inventario-turistico/actividad-principal/entities/actividad_principal';
import { UsuarioHeaderDto } from '../../module-usuario/auth/dto/header-usuario';
import { Idioma } from 'src/modules/module-library/idioma/entity/idioma.entity';
import { TipoIdioma } from 'src/modules/module-library/idioma/enum/status.enum';
import { CreateIdiomaDto } from 'src/modules/module-library/idioma/dto/create-idioma';
import { actividadPrincipalInventarioSeedData } from './seeds/actividad_principal.seeds.data';

@Injectable()
export class ActividadPrincipalService {
  constructor(
    @Inject('ACTIVIDAD_PRINCIPAL_REPOSITORY')
    private actividadPrincipalRepository: typeof ActividadPrincipal,
  ) {}

  async seeds() {
    const count = await this.actividadPrincipalRepository.count();
    if (count === 0) {
      for (const element of actividadPrincipalInventarioSeedData) {
        await this.insert(element);
      }
    }
  }

  async getAll(req: UsuarioHeaderDto) {
    try {
      const inventarios = await this.actividadPrincipalRepository.findAll({
        attributes: ['id_actividad_principal'],
        include: [
          {
            model: Idioma,
            attributes: [[req.idioma, 'idioma']],
          },
        ],
      });

      const data = await inventarios.map((item) => ({
        id_actividad_principal: item.id_actividad_principal,
        nombre: item.idioma.dataValues.idioma,
      }));
      return data;
    } catch (error) {
      throw new NotFoundException(error.message);
    }
  }

  async insert(body: CreateIdiomaDto) {
    try {
      const traduccion = new Idioma();
      traduccion.es = body.es;
      traduccion.en = body.en;
      traduccion.fra = body.fra;
      traduccion.tipo = TipoIdioma.ACTIVIDAD_PRINCIPAL_INVENTARIO;
      const traduccion_guardado = await traduccion.save();

      const actividadPrincipal = new ActividadPrincipal();
      actividadPrincipal.idioma = traduccion_guardado;
      actividadPrincipal.id_idioma = traduccion_guardado.id_idioma;

      const actividad_principal_guardado = await actividadPrincipal.save();

      return actividad_principal_guardado;
    } catch (error) {
      return error;
    }
  }
}
