import { ActividadPrincipal } from 'src/modules/module-inventario-turistico/actividad-principal/entities/actividad_principal';

export const actividadPrincipalProviders = [
  {
    provide: 'ACTIVIDAD_PRINCIPAL_REPOSITORY',
    useValue: ActividadPrincipal,
  },
];
