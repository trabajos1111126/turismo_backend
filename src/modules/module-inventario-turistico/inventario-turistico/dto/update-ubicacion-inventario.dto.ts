import { IsNotEmpty } from 'class-validator';

export class UpdateUbicacionInventarioDto {
  @IsNotEmpty()
  id_inventario: number; // id principal
  latitud: number;
  longitud: number;
}
