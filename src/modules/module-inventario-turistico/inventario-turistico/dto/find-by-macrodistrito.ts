import { IsNotEmpty } from 'class-validator';

export class FindByMacrodistritoDto {
  @IsNotEmpty()
  nombre: string;
}
