import { IsNotEmpty } from 'class-validator';

export class CreateInventarioDto {
  @IsNotEmpty()
  id_vocacion: number;

  @IsNotEmpty()
  nombre: string;

  latitud: number;
  longitud: number;

  @IsNotEmpty()
  jerarquia: string;

  @IsNotEmpty()
  id_categoria: number;

  @IsNotEmpty()
  id_tipo_inventario: number;

  @IsNotEmpty()
  id_subtipo: number;

  @IsNotEmpty()
  macrodistrito: string;

  @IsNotEmpty()
  distrito: string;

  @IsNotEmpty()
  zona_comunidad: string;

  @IsNotEmpty()
  localizacion_cartografica: string;

  @IsNotEmpty()
  id_modalidad: number;

  @IsNotEmpty()
  id_interes_principal: number;

  @IsNotEmpty()
  existencia_operacional: string;

  @IsNotEmpty()
  presencia_visitas: string;

  @IsNotEmpty()
  id_actividad_principal: number;

  @IsNotEmpty()
  inclusion: string;

  @IsNotEmpty()
  nivel_aprovechamiento: string;

  @IsNotEmpty()
  nivel_significancia: string;

  @IsNotEmpty()
  estado_actual: string;

  @IsNotEmpty()
  tenencia_normativa: string;
}
