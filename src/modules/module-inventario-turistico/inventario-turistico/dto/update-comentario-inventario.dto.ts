import { IsNotEmpty } from 'class-validator';

export class UpdateComentarioInventarioDto {
  @IsNotEmpty()
  id_inventario: number; // id principal
  es: string;
  en: string;
  fra: string;
}
