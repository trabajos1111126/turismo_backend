import { InventarioTuristico } from 'src/modules/module-inventario-turistico/inventario-turistico/entities/inventario_turistico.entity';

export const inventarioTuristicoProviders = [
  {
    provide: 'INVENTARIO_TURISTICO_REPOSITORY',
    useValue: InventarioTuristico,
  },
];
