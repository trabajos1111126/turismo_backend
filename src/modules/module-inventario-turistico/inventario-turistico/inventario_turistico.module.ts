import { Module } from '@nestjs/common';
import { InventarioTuristicoController } from './inventario_turistico.controller';
import { InventarioTuristicoService } from './inventario_turistico.service';
import { inventarioTuristicoProviders } from './inventario.turistico.providers';

@Module({
  controllers: [InventarioTuristicoController],
  providers: [InventarioTuristicoService, ...inventarioTuristicoProviders],
})
export class InventarioTuristicoModule {}
