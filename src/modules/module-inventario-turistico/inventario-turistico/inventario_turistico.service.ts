import {
  Injectable,
  Inject,
  NotFoundException,
  UploadedFile,
} from '@nestjs/common';
import { Idioma } from 'src/modules/module-library/idioma/entity/idioma.entity';
import { ActividadPrincipal } from 'src/modules/module-inventario-turistico/actividad-principal/entities/actividad_principal';
import { CategoriaInventario } from 'src/modules/module-inventario-turistico/categoria-inventario/entities/categoria_inventario.entity';
import { InteresPrincipal } from 'src/modules/module-inventario-turistico/interes-principal/entities/interes_principal';
import { InventarioTuristico } from 'src/modules/module-inventario-turistico/inventario-turistico/entities/inventario_turistico.entity';
import { Modalidad } from 'src/modules/module-inventario-turistico/modalidad/entities/modalidad.entity';
import { SubTipoInventario } from 'src/modules/module-inventario-turistico/subtipo-inventario/entities/subtipo_inventario.entity';
import { TipoInventario } from 'src/modules/module-inventario-turistico/tipo-inventario/entities/tipo_inventario.entity';
import { Vocacion } from 'src/modules/module-inventario-turistico/vocacion/entities/vocacion.entity';
import { SetPaginaDto } from 'src/modules/module-guia-turistico/guia-turistico/dto/setPaguinationDto';
import { UsuarioHeaderDto } from 'src/modules/module-usuario/auth/dto/header-usuario';
import { CreateInventarioDto } from './dto/create-inventario.dto';
import { inventarioSeedData } from './seeds/inventario.seeds';
import { Adjunto } from 'src/modules/module-library/adjunto/entities/adjunto.entity';
import { Sequelize } from 'sequelize';
import { ValoracionInventario } from '../valoracion/entities/valoracion-inventario.entity';
import { Usuario } from 'src/modules/module-usuario/usuario/entities/usuario.entity';
import { UpdateInventarioDto } from './dto/update-inventario.dto';
import { UpdateUbicacionInventarioDto } from './dto/update-ubicacion-inventario.dto';
import { CreateInventarioSeedDto } from './dto/create-inventario.seed.dto';
import { UpdateComentarioInventarioDto } from './dto/update-comentario-inventario.dto';
import { descripcionSeedData } from './seeds/descripciones.seeds';
import { TipoIdioma } from 'src/modules/module-library/idioma/enum/status.enum';

@Injectable()
export class InventarioTuristicoService {
  constructor(
    @Inject('INVENTARIO_TURISTICO_REPOSITORY')
    private inventarioTuristicoRepository: typeof InventarioTuristico,
  ) {}

  async insert(inventario: CreateInventarioDto) {
    try {
      const i = new InventarioTuristico();
      i.id_vocacion = inventario.id_vocacion;
      i.nombre = inventario.nombre;
      i.latitud = inventario.latitud;
      i.longitud = inventario.longitud;
      i.jerarquia = inventario.jerarquia;
      i.id_categoria = inventario.id_categoria;
      i.id_tipo_inventario = inventario.id_tipo_inventario;
      i.id_subtipo = inventario.id_subtipo;
      i.macrodistrito = inventario.macrodistrito;
      i.distrito = inventario.distrito;
      i.zona_comunidad = inventario.zona_comunidad;
      i.localizacion_cartografica = inventario.localizacion_cartografica;
      i.id_modalidad = inventario.id_modalidad;
      i.id_interes_principal = inventario.id_interes_principal;
      i.existencia_operacional = inventario.existencia_operacional;
      i.presencia_visitas = inventario.presencia_visitas;
      i.id_actividad_principal = inventario.id_actividad_principal;
      i.inclusion = inventario.inclusion;
      i.nivel_aprovechamiento = inventario.nivel_aprovechamiento;
      i.nivel_significancia = inventario.nivel_significancia;
      i.estado_actual = inventario.estado_actual;
      i.tenencia_normativa = inventario.tenencia_normativa;

      const inventario_guardado = await i.save();
      return inventario_guardado;
    } catch (error) {
      throw new NotFoundException(error.message);
    }
  }
  async insertSeeds(inventario: CreateInventarioSeedDto) {
    try {
      const i = new InventarioTuristico();
      i.id_vocacion = inventario.id_vocacion;
      i.nombre = inventario.nombre;
      i.latitud = inventario.latitud;
      i.longitud = inventario.longitud;
      i.jerarquia = inventario.jerarquia;
      i.id_categoria = inventario.id_categoria;
      i.id_tipo_inventario = inventario.id_tipo_inventario;
      i.id_subtipo = inventario.id_subtipo;
      i.macrodistrito = inventario.macrodistrito;
      i.distrito = inventario.distrito;
      i.zona_comunidad = inventario.zona_comunidad;
      i.localizacion_cartografica = inventario.localizacion_cartografica;
      i.id_modalidad = inventario.id_modalidad;
      i.id_interes_principal = inventario.id_interes_principal;
      i.existencia_operacional = inventario.existencia_operacional;
      i.presencia_visitas = inventario.presencia_visitas;
      i.id_actividad_principal = inventario.id_actividad_principal;
      i.inclusion = inventario.inclusion;
      i.nivel_aprovechamiento = inventario.nivel_aprovechamiento;
      i.nivel_significancia = inventario.nivel_significancia;
      i.estado_actual = inventario.estado_actual;
      i.tenencia_normativa = inventario.tenencia_normativa;
      //agreamos descripcion
      const descripcion = new Idioma();
      descripcion.es = '';
      descripcion.en = '';
      descripcion.fra = '';
      descripcion.tipo = TipoIdioma.DESCRIPCION_INVENTARIO_INVENTARIO;
      const descripcion_guardada = await descripcion.save();
      i.descripcion = descripcion_guardada;
      i.id_descripcion = descripcion_guardada.id_idioma;
      // fin de modificaciones
      const inventario_guardado = await i.save();
      //ahora guardamos las adjunto de inventario
      for (const item of inventario.fotos) {
        //console.log(item);
        const adjunto = new Adjunto();
        const c = item.split('.');
        adjunto.url_foto = 'images/inventario/' + item;
        //adjunto.url_min = 'images/inventario/' + file.filename;
        adjunto.descripcion = 'imagen de inventario';
        if (c.length > 0) adjunto.mimetype = 'image/' + c[1];
        else adjunto.mimetype = 'indefinido';
        adjunto.inventario = inventario_guardado;
        adjunto.id_inventario = inventario_guardado.id_inventario;
        await adjunto.save();
        //console.log(adjunto_guardado)
      }
      return inventario_guardado;
    } catch (error) {
      throw new NotFoundException(error.message);
    }
  }
  async getById(id: number, req: UsuarioHeaderDto) {
    try {
      // Buscar un usuario por su ID
      const inventario = await this.inventarioTuristicoRepository.findByPk(id, {
        attributes: [
          'id_inventario',
          'nombre',
          'jerarquia',
          'macrodistrito',
          'distrito',
          'zona_comunidad',
          'localizacion_cartografica',
          'id_vocacion',
          'id_categoria',
          'id_tipo_inventario',
          'id_subtipo',
          'id_modalidad',
          'id_interes_principal',
          'id_actividad_principal',
          'latitud',
          'longitud',
        ],
        include: [
          {
            model: ValoracionInventario,
            as: 'valoraciones',
            attributes: [
              'id_valoracion_inventario',
              'calificacion',
              'comentario',
              'createdAt',
            ],
            include: [
              {
                model: Usuario,
                attributes: ['id_usuario', 'nombre', 'url_imagen_perfil'],
              },
            ],
            where: {
              deleted: false,
            },
            required: false,
          },
          {
            model: Adjunto,
            attributes: ['url_foto'],
          },
          {
            model: Vocacion,
            attributes: ['id_idioma'],
            include: [
              {
                model: Idioma,
                attributes: [[req.idioma, 'idioma']],
              },
            ],
          },
          {
            model: CategoriaInventario,
            attributes: ['id_idioma'],
            include: [
              {
                model: Idioma,
                attributes: [[req.idioma, 'idioma']],
              },
            ],
          },
          {
            model: TipoInventario,
            attributes: ['id_idioma'],
            include: [
              {
                model: Idioma,
                attributes: [[req.idioma, 'idioma']],
              },
            ],
          },
          {
            model: SubTipoInventario,
            attributes: ['id_idioma'],
            include: [
              {
                model: Idioma,
                attributes: [[req.idioma, 'idioma']],
              },
            ],
          },
          {
            model: Modalidad,
            attributes: ['id_idioma'],
            include: [
              {
                model: Idioma,
                attributes: [[req.idioma, 'idioma']],
              },
            ],
          },
          {
            model: InteresPrincipal,
            attributes: ['id_idioma'],
            include: [
              {
                model: Idioma,
                attributes: [[req.idioma, 'idioma']],
              },
            ],
          },
          {
            model: ActividadPrincipal,
            attributes: ['id_idioma'],
            include: [
              {
                model: Idioma,
                attributes: [[req.idioma, 'idioma']],
              },
            ],
          },

          //descripcion

          {
            model: Idioma,
            attributes: ['es', [req.idioma, 'idioma']],
            //attributes: [[req.idioma, 'idioma']],
          },
          /*{
              model: Adjunto,
            }*/
        ],
        group: [
          'InventarioTuristico.id_inventario',
          'adjuntos.id_adjunto',
          'valoraciones.id_valoracion_inventario',
          'valoraciones->usuario.id_usuario',
          'vocacion.id_vocacion',
          'vocacion->idioma.id_idioma',
          'categoriaInventario.id_categoria',
          'categoriaInventario->idioma.id_idioma',
          'tipoInventario.id_tipo_inventario',
          'tipoInventario->idioma.id_idioma',
          'subTipoInventario.id_subtipo',
          'subTipoInventario->idioma.id_idioma',
          'modalidad.id_modalidad',
          'modalidad->idioma.id_idioma',
          'interesPrincipal.id_interes_principal',
          'interesPrincipal->idioma.id_idioma',
          'actividadPrincipal.id_actividad_principal',
          'actividadPrincipal->idioma.id_idioma',
          'descripcion.id_idioma',
        ],
        raw: false,
        order: [[Sequelize.col('id_valoracion_inventario'), 'DESC']],
      });
      let promedio = 0;
      for (let i = 0; i < inventario.valoraciones.length; i++) {
        const element = inventario.valoraciones[i];
        //console.log(element.calificacion);
        promedio = promedio + element.calificacion;
      }

      return {
        id_inventario: inventario.id_inventario,
        nombre: inventario.nombre,
        jerarquia: inventario.jerarquia,
        macrodistrito: inventario.macrodistrito,
        distrito: inventario.distrito,
        fotos: inventario.adjuntos,
        zona_comunidad: inventario.zona_comunidad,
        latitud: inventario.latitud,
        longitud: inventario.longitud,
        localizacion_cartografica: inventario.localizacion_cartografica,

        id_vocacion: inventario.id_vocacion,
        tra_vocacion: inventario.vocacion.idioma.dataValues.idioma,

        id_categoria: inventario.id_categoria,
        tra_categoria: inventario.categoriaInventario.idioma.dataValues.idioma,

        id_tipo_inventario: inventario.id_tipo_inventario,
        tra_tipo_inventario: inventario.tipoInventario.idioma.dataValues.idioma,

        id_subtipo: inventario.id_subtipo,
        tra_subtipo: inventario.subTipoInventario.idioma.dataValues.idioma,

        id_modalidad: inventario.id_modalidad,
        tra_modalidad: inventario.modalidad.idioma.dataValues.idioma,

        id_interes_principlal: inventario.id_interes_principal,
        tra_interes_principal:
          inventario.interesPrincipal.idioma.dataValues.idioma,

        id_actividad_principlal: inventario.id_actividad_principal,
        tra_actividad_principal:
          inventario.actividadPrincipal.idioma.dataValues.idioma,
        valoracion:
          promedio > 0
            ? (promedio / inventario.valoraciones.length).toFixed(1)
            : '0',
        cantidad_valoraciones: inventario.valoraciones.length,
        valoraciones: inventario.valoraciones,
        descripcion:
          inventario.descripcion.dataValues.idioma == ''
            ? inventario.descripcion.dataValues.es
            : inventario.descripcion.dataValues.idioma, //.dataValues.es,
      };
    } catch (error) {
      console.error('Error al buscar inventario:', error);
      throw new NotFoundException('Error al buscar inventario:', error.message);
    }
  }
  async getAllByValoracion(req: UsuarioHeaderDto, id: number): Promise<any> {
    const resp = await this.getAll(req);
    const data = [];
    for (let i = 0; i < resp.data.length; i++) {
      const element = resp.data[i];

      if (element.id_vocacion == id) {
        data.push(element);
      }
    }
    return data;
  }
  async update(updateIventarioDTO: UpdateInventarioDto) {
    const inventario = await this.inventarioTuristicoRepository.findByPk(
      updateIventarioDTO.id_inventario,
    );

    if (!inventario) {
      throw new NotFoundException('Inventario no encontrada');
    }

    inventario.id_vocacion = updateIventarioDTO.id_vocacion;
    inventario.nombre = updateIventarioDTO.nombre;
    inventario.latitud = updateIventarioDTO.latitud;
    inventario.longitud = updateIventarioDTO.longitud;
    inventario.jerarquia = updateIventarioDTO.jerarquia;
    inventario.id_categoria = updateIventarioDTO.id_categoria;
    inventario.id_tipo_inventario = updateIventarioDTO.id_tipo_inventario;
    inventario.id_subtipo = updateIventarioDTO.id_subtipo;
    inventario.macrodistrito = updateIventarioDTO.macrodistrito;
    inventario.distrito = updateIventarioDTO.distrito;
    inventario.zona_comunidad = updateIventarioDTO.zona_comunidad;
    inventario.localizacion_cartografica =
      updateIventarioDTO.localizacion_cartografica;
    inventario.id_modalidad = updateIventarioDTO.id_modalidad;
    inventario.id_interes_principal = updateIventarioDTO.id_interes_principal;
    inventario.existencia_operacional =
      updateIventarioDTO.existencia_operacional;
    inventario.presencia_visitas = updateIventarioDTO.presencia_visitas;
    inventario.id_actividad_principal =
      updateIventarioDTO.id_actividad_principal;
    inventario.inclusion = updateIventarioDTO.inclusion;
    inventario.nivel_aprovechamiento = updateIventarioDTO.nivel_aprovechamiento;
    inventario.nivel_significancia = updateIventarioDTO.nivel_significancia;
    inventario.estado_actual = updateIventarioDTO.estado_actual;
    inventario.tenencia_normativa = updateIventarioDTO.tenencia_normativa;

    const inventario_guardado = await inventario.save();
    return inventario_guardado;
  }

  async updateUbicacion(
    updateUbicacionInventarioDto: UpdateUbicacionInventarioDto,
  ) {
    const inventario = await this.inventarioTuristicoRepository.findByPk(
      updateUbicacionInventarioDto.id_inventario,
    );

    if (!inventario) {
      throw new NotFoundException('Inventario no encontrado');
    }

    inventario.latitud = updateUbicacionInventarioDto.latitud;
    inventario.longitud = updateUbicacionInventarioDto.longitud;

    const inventario_guardado = await inventario.save();
    return inventario_guardado;
  }

  async getAllByMacrodistritos(
    req: UsuarioHeaderDto,
    nombre: string,
  ): Promise<any> {
    console.log('return por macrodistirto');
    const resp = await this.getAll(req);
    const data = [];
    for (let i = 0; i < resp.data.length; i++) {
      const element = resp.data[i];

      if (element.macrodistrito == '' + nombre) {
        data.push(element);
      }
    }
    return data;
  }
  async paginate(
    req: UsuarioHeaderDto,
    paguinaDTO: SetPaginaDto,
  ): Promise<any> {
    try {
      const page = parseInt(paguinaDTO.page, 10);
      const pageSize = parseInt(paguinaDTO.pageSize, 10);
      if (page <= 0) {
        throw new NotFoundException(
          'el numero de paquina tiene que ser mayor o igual a 1',
        );
      }
      if (pageSize <= 5 && pageSize > 100) {
        throw new NotFoundException(
          'el numero de datos por paquina tiene que ser mayor o igual a 5 y menor que 100',
        );
      }
      const { count, rows } =
        await this.inventarioTuristicoRepository.findAndCountAll({
          attributes: [
            'id_inventario',
            'nombre',
            'jerarquia',
            'macrodistrito',
            'distrito',
            'zona_comunidad',
            'localizacion_cartografica',
            'id_vocacion',
            'id_categoria',
            'id_tipo_inventario',
            'id_subtipo',
            'id_modalidad',
            'id_interes_principal',
            'id_actividad_principal',
          ],
          include: [
            {
              model: Vocacion,
              attributes: ['id_idioma'],
              include: [
                {
                  model: Idioma,
                  attributes: [[req.idioma, 'idioma']],
                },
              ],
            },
            {
              model: CategoriaInventario,
              attributes: ['id_idioma'],
              include: [
                {
                  model: Idioma,
                  attributes: [[req.idioma, 'idioma']],
                },
              ],
            },
            {
              model: TipoInventario,
              attributes: ['id_idioma'],
              include: [
                {
                  model: Idioma,
                  attributes: [[req.idioma, 'idioma']],
                },
              ],
            },
            {
              model: SubTipoInventario,
              attributes: ['id_idioma'],
              include: [
                {
                  model: Idioma,
                  attributes: [[req.idioma, 'idioma']],
                },
              ],
            },
            {
              model: Modalidad,
              attributes: ['id_idioma'],
              include: [
                {
                  model: Idioma,
                  attributes: [[req.idioma, 'idioma']],
                },
              ],
            },
            {
              model: InteresPrincipal,
              attributes: ['id_idioma'],
              include: [
                {
                  model: Idioma,
                  attributes: [[req.idioma, 'idioma']],
                },
              ],
            },
            {
              model: ActividadPrincipal,
              attributes: ['id_idioma'],
              include: [
                {
                  model: Idioma,
                  attributes: [[req.idioma, 'idioma']],
                },
              ],
            },
          ],
          limit: pageSize,
          offset: (page - 1) * pageSize,
        });
      const totalPaguinas = Math.ceil(count / pageSize);

      const data = rows.map((item) => ({
        id_inventario: item.id_inventario,
        nombre: item.nombre,
        jerarquia: item.jerarquia,
        macrodistrito: item.macrodistrito,
        distrito: item.distrito,
        zona_comunidad: item.zona_comunidad,
        localizacion_cartografica: item.localizacion_cartografica,
        id_vocacion: item.id_vocacion,
        tra_vocacion: item.vocacion.idioma.dataValues.idioma,

        id_categoria: item.id_categoria,
        tra_categoria: item.categoriaInventario.idioma.dataValues.idioma,

        id_tipo_inventario: item.id_tipo_inventario,
        tra_tipo_inventario: item.tipoInventario.idioma.dataValues.idioma,

        id_subtipo: item.id_subtipo,
        tra_subtipo: item.subTipoInventario.idioma.dataValues.idioma,

        id_modalidad: item.id_modalidad,
        tra_modalidad: item.subTipoInventario.idioma.dataValues.idioma,

        id_interes_principlal: item.id_interes_principal,
        tra_interes_principal: item.interesPrincipal.idioma.dataValues.idioma,

        id_actividad_principlal: item.id_actividad_principal,
        tra_actividad_principal:
          item.actividadPrincipal.idioma.dataValues.idioma,
      }));
      return {
        data,
        paguinaActual: page,
        totalItems: count,
        totalPaguinas,
      };
    } catch (error) {
      throw new NotFoundException(error.message);
    }
  }
  async getAll(req: UsuarioHeaderDto): Promise<any> {
    try {
      const rows = await this.inventarioTuristicoRepository.findAll({
        attributes: [
          'id_inventario',
          'nombre',
          'jerarquia',
          'macrodistrito',
          'distrito',
          'zona_comunidad',
          'localizacion_cartografica',
          'id_vocacion',
          'id_categoria',
          'id_tipo_inventario',
          'id_subtipo',
          'id_modalidad',
          'id_interes_principal',
          'id_actividad_principal',
          'latitud',
          'longitud',
          [
            Sequelize.fn(
              'COALESCE',
              Sequelize.fn('AVG', Sequelize.col('valoraciones.calificacion')),
              0,
            ),
            'valoracion',
          ],
          [
            Sequelize.fn('COUNT', Sequelize.col('valoraciones.id_inventario')),
            'cantidad_valoraciones',
          ],
        ],
        include: [
          {
            model: ValoracionInventario,
            as: 'valoraciones',
            attributes: [],
            where: {
              deleted: false,
            },
            required: false,
          },
          {
            model: Adjunto,
            attributes: ['url_foto'],
          },
          {
            model: Vocacion,
            attributes: ['id_idioma'],
            include: [
              {
                model: Idioma,
                attributes: [[req.idioma, 'idioma']],
              },
            ],
          },
          {
            model: CategoriaInventario,
            attributes: ['id_idioma'],
            include: [
              {
                model: Idioma,
                attributes: [[req.idioma, 'idioma']],
              },
            ],
          },
          {
            model: TipoInventario,
            attributes: ['id_idioma'],
            include: [
              {
                model: Idioma,
                attributes: [[req.idioma, 'idioma']],
              },
            ],
          },
          {
            model: SubTipoInventario,
            attributes: ['id_idioma'],
            include: [
              {
                model: Idioma,
                attributes: [[req.idioma, 'idioma']],
              },
            ],
          },
          {
            model: Modalidad,
            attributes: ['id_idioma'],
            include: [
              {
                model: Idioma,
                attributes: [[req.idioma, 'idioma']],
              },
            ],
          },
          {
            model: InteresPrincipal,
            attributes: ['id_idioma'],
            include: [
              {
                model: Idioma,
                attributes: [[req.idioma, 'idioma']],
              },
            ],
          },
          {
            model: ActividadPrincipal,
            attributes: ['id_idioma'],
            include: [
              {
                model: Idioma,
                attributes: [[req.idioma, 'idioma']],
              },
            ],
          },
          /*{
              model: Adjunto,
            }*/
        ],
        group: [
          'InventarioTuristico.id_inventario',
          'adjuntos.id_adjunto',
          'vocacion.id_vocacion',
          'vocacion->idioma.id_idioma',
          'categoriaInventario.id_categoria',
          'categoriaInventario->idioma.id_idioma',
          'tipoInventario.id_tipo_inventario',
          'tipoInventario->idioma.id_idioma',
          'subTipoInventario.id_subtipo',
          'subTipoInventario->idioma.id_idioma',
          'modalidad.id_modalidad',
          'modalidad->idioma.id_idioma',
          'interesPrincipal.id_interes_principal',
          'interesPrincipal->idioma.id_idioma',
          'actividadPrincipal.id_actividad_principal',
          'actividadPrincipal->idioma.id_idioma',
        ],
        raw: false,
        where: {
          deleted: false, // Agregar esta condición para excluir registros borrados
        },
        order: [
          [Sequelize.col('valoracion'), 'DESC'],
          ['nombre', 'ASC'],
        ],
      });

      const data = rows.map((item) => ({
        id_inventario: item.id_inventario,
        nombre: item.nombre,
        jerarquia: item.jerarquia,
        macrodistrito: item.macrodistrito,
        distrito: item.distrito,
        fotos: item.adjuntos,
        zona_comunidad: item.zona_comunidad,
        latitud: item.latitud,
        longitud: item.longitud,
        localizacion_cartografica: item.localizacion_cartografica,

        id_vocacion: item.id_vocacion,
        tra_vocacion: item.vocacion.idioma.dataValues.idioma,

        id_categoria: item.id_categoria,
        tra_categoria: item.categoriaInventario.idioma.dataValues.idioma,

        id_tipo_inventario: item.id_tipo_inventario,
        tra_tipo_inventario: item.tipoInventario.idioma.dataValues.idioma,

        id_subtipo: item.id_subtipo,
        tra_subtipo: item.subTipoInventario.idioma.dataValues.idioma,

        id_modalidad: item.id_modalidad,
        tra_modalidad: item.modalidad.idioma.dataValues.idioma,

        id_interes_principlal: item.id_interes_principal,
        tra_interes_principal: item.interesPrincipal.idioma.dataValues.idioma,

        id_actividad_principlal: item.id_actividad_principal,
        tra_actividad_principal:
          item.actividadPrincipal.idioma.dataValues.idioma,
        valoracion: item.dataValues.valoracion,
        cantidad_valoraciones: item.dataValues.cantidad_valoraciones,
      }));
      return {
        data,
      };
    } catch (error) {
      console.log(error.message);
      throw new NotFoundException(error.message);
    }
  }

  async agregarFoto(id: number, @UploadedFile() file: Express.Multer.File) {
    const inventario_turistico =
      await this.inventarioTuristicoRepository.findByPk(id);
    if (!inventario_turistico) {
      console.log(`No se encontró el inventario con el id ${id}.`);
      return;
    }
    const adjunto = new Adjunto();
    adjunto.url_foto = 'images/inventario/' + file.filename;
    //adjunto.url_min = 'images/inventario/' + file.filename;
    adjunto.descripcion = 'imagen de inventario';
    adjunto.mimetype = file.mimetype;
    adjunto.inventario = inventario_turistico;
    adjunto.id_inventario = inventario_turistico.id_inventario;
    const adjunto_guardado = await adjunto.save();
    return adjunto_guardado;
  }
  async updateDescripcion(
    updateComentarioInventarioDto: UpdateComentarioInventarioDto,
  ) {
    const inventario = await this.inventarioTuristicoRepository.findByPk(
      updateComentarioInventarioDto.id_inventario,
    );

    if (!inventario) {
      throw new NotFoundException('Inventario no encontrado');
    }

    const comentario = await Idioma.findByPk(inventario.id_descripcion);
    if (!comentario) {
      throw new NotFoundException('Comentario no encontrado');
    }
    try {
      comentario.es = updateComentarioInventarioDto.es;
      comentario.en = updateComentarioInventarioDto.en;
      comentario.fra = updateComentarioInventarioDto.fra;

      const comentario_guardado = await comentario.save();
      return comentario_guardado;
    } catch (error) {
      console.log(error.message);
      return error;
    }
  }

  async seeds() {
    const count = await this.inventarioTuristicoRepository.count();
    if (count === 0) {
      //**  AGREGAMOS INVENTARIO */

      for (const element of inventarioSeedData) {
        await this.insertSeeds(element);
      }

      for (const element2 of descripcionSeedData) {
        await this.updateDescripcion(element2);
      }
      //const promises2 = descripcionSeedData.map(async (element2) => {
      //return await this.updateDescripcion(element2);
      //});
      //await Promise.all(promises2);

      console.log(
        'Todas las inserciones  de inventarios turisticos se han completado.',
      );
    }
  }

  async delete(id: number) {
    const guia = await this.inventarioTuristicoRepository.findByPk(id);

    if (!guia) {
      throw new NotFoundException('Inventario no encontrado');
    }

    guia.deleted = true; // Marcamos la entidad como borrada
    await guia.save();
    return 'El Inventario se ha borrado exitosamente';
  }
}
