import {
  Table,
  Column,
  Model,
  BelongsTo,
  ForeignKey,
  DataType,
  HasMany,
} from 'sequelize-typescript';
import { CategoriaInventario } from '../../categoria-inventario/entities/categoria_inventario.entity';
import { TipoInventario } from '../../tipo-inventario/entities/tipo_inventario.entity';
import { SubTipoInventario } from '../../subtipo-inventario/entities/subtipo_inventario.entity';
import { Vocacion } from '../../vocacion/entities/vocacion.entity';
import { Modalidad } from '../../modalidad/entities/modalidad.entity';
import { InteresPrincipal } from '../../interes-principal/entities/interes_principal';
import { ActividadPrincipal } from '../../actividad-principal/entities/actividad_principal';
import {
  DistritoInventario,
  EstadoActualInventario,
  ExistenciaOperacionalInventario,
  InclusionInventario,
  JerarquiaInventario,
  LocacionCartograficaInventario,
  MacrodistritoInventario,
  NivelAprovechamientoInventario,
  NivelSignificanciaInventario,
  PresenciaVisitasInventario,
  TenenciaNormativaInventario,
  ZonaComunidadInventario,
} from '../enum/estatus.enum';
import { Adjunto } from 'src/modules/module-library/adjunto/entities/adjunto.entity';
import { ValoracionInventario } from '../../valoracion/entities/valoracion-inventario.entity';
import { DataTypes } from 'sequelize';
import { Idioma } from 'src/modules/module-library/idioma/entity/idioma.entity';

@Table({ tableName: 'inventario_turistico' })
export class InventarioTuristico extends Model {
  @Column({ primaryKey: true, autoIncrement: true })
  id_inventario: number;

  @ForeignKey(() => Vocacion)
  id_vocacion: number;
  @BelongsTo(() => Vocacion)
  vocacion: Vocacion;

  @Column
  nombre: string;

  @Column({ type: DataTypes.FLOAT, defaultValue: 0 }) //-16.5044627
  latitud: number;

  @Column({ type: DataTypes.FLOAT, defaultValue: 0 }) // -68.1308097
  longitud: number;

  @Column(DataType.ENUM(...Object.values(JerarquiaInventario)))
  jerarquia: string;

  @ForeignKey(() => CategoriaInventario)
  id_categoria: number;
  @BelongsTo(() => CategoriaInventario)
  categoriaInventario: CategoriaInventario;

  @ForeignKey(() => TipoInventario)
  id_tipo_inventario: number;
  @BelongsTo(() => TipoInventario)
  tipoInventario: TipoInventario;

  @ForeignKey(() => SubTipoInventario)
  id_subtipo: number;
  @BelongsTo(() => SubTipoInventario)
  subTipoInventario: SubTipoInventario;

  @Column(DataType.ENUM(...Object.values(MacrodistritoInventario)))
  macrodistrito: string;

  @Column(DataType.ENUM(...Object.values(DistritoInventario)))
  distrito: string;

  @Column(DataType.ENUM(...Object.values(ZonaComunidadInventario)))
  zona_comunidad: string;

  @Column(DataType.ENUM(...Object.values(LocacionCartograficaInventario)))
  localizacion_cartografica: string;

  @ForeignKey(() => Modalidad)
  id_modalidad: number;
  @BelongsTo(() => Modalidad)
  modalidad: Modalidad;

  @ForeignKey(() => InteresPrincipal)
  id_interes_principal: number;
  @BelongsTo(() => InteresPrincipal)
  interesPrincipal: InteresPrincipal;

  @Column(DataType.ENUM(...Object.values(ExistenciaOperacionalInventario)))
  existencia_operacional: string;

  @Column(DataType.ENUM(...Object.values(PresenciaVisitasInventario)))
  presencia_visitas: string;

  @ForeignKey(() => ActividadPrincipal)
  id_actividad_principal: number;
  @BelongsTo(() => ActividadPrincipal)
  actividadPrincipal: ActividadPrincipal;

  @Column(DataType.ENUM(...Object.values(InclusionInventario)))
  inclusion: string;

  @Column(DataType.ENUM(...Object.values(NivelAprovechamientoInventario)))
  nivel_aprovechamiento: string;

  @Column(DataType.ENUM(...Object.values(NivelSignificanciaInventario)))
  nivel_significancia: string;

  @Column(DataType.ENUM(...Object.values(EstadoActualInventario)))
  estado_actual: string;

  @Column(DataType.ENUM(...Object.values(TenenciaNormativaInventario)))
  tenencia_normativa: string;

  @HasMany(() => Adjunto)
  adjuntos: Adjunto[];

  @HasMany(() => ValoracionInventario)
  valoraciones: ValoracionInventario[];
  /** colocamos descripcion de inventarios */
  @BelongsTo(() => Idioma)
  descripcion: Idioma;

  @ForeignKey(() => Idioma)
  id_descripcion: number;

  @Column({ defaultValue: false })
  deleted: boolean;
}
