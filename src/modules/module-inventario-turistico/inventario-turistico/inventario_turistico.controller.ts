import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Request,
  SetMetadata,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { InventarioTuristicoService } from './inventario_turistico.service';
import { AuthGuard } from 'src/modules/module-usuario/auth/auth.guard';
import { SetPaginaDto } from 'src/modules/module-guia-turistico/guia-turistico/dto/setPaguinationDto';
import { CreateInventarioDto } from './dto/create-inventario.dto';
import { multerConfigInventario } from 'src/config/multer.config';
import { FileInterceptor } from '@nestjs/platform-express';
import { UpdateInventarioDto } from './dto/update-inventario.dto';
import { UpdateUbicacionInventarioDto } from './dto/update-ubicacion-inventario.dto';
import { RolUsuarioEnum } from 'src/modules/module-usuario/usuario/enum/usuario.enum';

@Controller('api/inventario/')
export class InventarioTuristicoController {
  constructor(
    private readonly inventarioTuristicoService: InventarioTuristicoService,
  ) {}

  @SetMetadata('roles', [
    RolUsuarioEnum.ADMINISTRADOR,
    RolUsuarioEnum.TURISTA,
    RolUsuarioEnum.ANONIMO,
  ])
  @UseGuards(AuthGuard)
  @Get('')
  getAll(@Request() req) {
    console.log('getAll Invetario_turistico');
    return this.inventarioTuristicoService.getAll(req);
  }

  @SetMetadata('roles', [
    RolUsuarioEnum.ADMINISTRADOR,
    RolUsuarioEnum.TURISTA,
    RolUsuarioEnum.ANONIMO,
  ])
  @UseGuards(AuthGuard)
  @Get(':id')
  getUserById(@Request() req, @Param('id') id: number) {
    console.log('getById inventario');
    return this.inventarioTuristicoService.getById(id, req);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Post()
  insert(@Body() body: CreateInventarioDto) {
    console.log('insert Invetario_turistico');
    return this.inventarioTuristicoService.insert(body);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Patch()
  update(@Body() updateGuiaDTO: UpdateInventarioDto) {
    console.log('Update Inventario_turistico');
    return this.inventarioTuristicoService.update(updateGuiaDTO);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Patch('ubicacion')
  updateUbicacion(
    @Body() updateUbicacionInventarioDto: UpdateUbicacionInventarioDto,
  ) {
    console.log('Update Inventario_turistico');
    return this.inventarioTuristicoService.updateUbicacion(
      updateUbicacionInventarioDto,
    );
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Get('paguina')
  getProfile(@Request() req, @Query() paginaDto: SetPaginaDto) {
    console.log('getAllByPage Invetario_turistico');
    return this.inventarioTuristicoService.paginate(req, paginaDto);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Get('vocacion/:id')
  getAllByValoracion(@Request() req, @Param('id') id: number) {
    console.log('getAllByValoracion Invetario_turistico');
    return this.inventarioTuristicoService.getAllByValoracion(req, id);
  }

  @SetMetadata('roles', [
    RolUsuarioEnum.ADMINISTRADOR,
    RolUsuarioEnum.TURISTA,
    RolUsuarioEnum.ANONIMO,
  ])
  @UseGuards(AuthGuard)
  @Get('macrodistrito/:nombre')
  getAllByMacrodistritos(@Request() req, @Param('nombre') nombre: string) {
    console.log('getAllByMacrodistrito Invetario_turistico');
    return this.inventarioTuristicoService.getAllByMacrodistritos(req, nombre);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Patch(':id/foto')
  @UseInterceptors(FileInterceptor('image', multerConfigInventario))
  agregarFoto(
    @Param('id') id: number,
    @UploadedFile() file: Express.Multer.File,
  ) {
    console.log('AgregarFoto Invetario_turistico');
    return this.inventarioTuristicoService.agregarFoto(id, file);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Delete(':id')
  delete(@Param('id') id: number) {
    console.log('Delete Invetario_turitico');
    return this.inventarioTuristicoService.delete(id);
  }
}
