import { Injectable, Inject, NotFoundException } from '@nestjs/common';
import { CreateIdiomaDto } from 'src/modules/module-library/idioma/dto/create-idioma';
import { Idioma } from 'src/modules/module-library/idioma/entity/idioma.entity';
import { TipoIdioma } from 'src/modules/module-library/idioma/enum/status.enum';

import { TipoInventario } from 'src/modules/module-inventario-turistico/tipo-inventario/entities/tipo_inventario.entity';
import { UsuarioHeaderDto } from 'src/modules/module-usuario/auth/dto/header-usuario';
import { tipoInventarioSeedData } from './seeds/tipo.seeds.data';

@Injectable()
export class TipoInventarioService {
  constructor(
    @Inject('TIPO_INVENTARIO_REPOSITORY')
    private tipoInventarioRepository: typeof TipoInventario,
  ) {}

  async seeds() {
    const count = await this.tipoInventarioRepository.count();
    if (count === 0) {

      for (const element of tipoInventarioSeedData) {
        await this.insert(element);
      }
      console.log(
        'Todas las inserciones  de tipo de Inventario turisticos se han completado.',
      );
    }
  }

  async getAll(req: UsuarioHeaderDto) {
    try {
      const tipo = await this.tipoInventarioRepository.findAll({
        attributes: ['id_tipo_inventario'],
        include: [
          {
            model: Idioma,
            attributes: [[req.idioma, 'idioma']],
          },
        ],
      });

      const data = await tipo.map((item) => ({
        id_tipo_inventario: item.id_tipo_inventario,
        nombre: item.idioma.dataValues.idioma,
      }));
      return data;
    } catch (error) {
      throw new NotFoundException(error.message);
    }
  }

  async insert(body: CreateIdiomaDto) {
    try {
      const traduccion = new Idioma();
      traduccion.es = body.es;
      traduccion.en = body.en;
      traduccion.fra = body.fra;
      traduccion.tipo = TipoIdioma.TIPO_INVENTARIO;
      const traduccion_guardado = await traduccion.save();

      const tipoInventario = new TipoInventario();
      tipoInventario.idioma = traduccion_guardado;
      tipoInventario.id_idioma = traduccion_guardado.id_idioma;

      const tipoInventario_guardado = await tipoInventario.save();
      return tipoInventario_guardado;
    } catch (error) {
      return error;
    }
  }
}
