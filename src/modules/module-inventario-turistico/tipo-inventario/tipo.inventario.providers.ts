import { TipoInventario } from 'src/modules/module-inventario-turistico/tipo-inventario/entities/tipo_inventario.entity';

export const tipoInventarioProviders = [
  {
    provide: 'TIPO_INVENTARIO_REPOSITORY',
    useValue: TipoInventario,
  },
];
