import {
  Body,
  Controller,
  Get,
  Post,
  Request,
  SetMetadata,
  UseGuards,
} from '@nestjs/common';

import { AuthGuard } from 'src/modules/module-usuario/auth/auth.guard';
import { CreateIdiomaDto } from 'src/modules/module-library/idioma/dto/create-idioma';
import { TipoInventarioService } from './tipo_inventario.service';
import { RolUsuarioEnum } from 'src/modules/module-usuario/usuario/enum/usuario.enum';

@Controller('api/tipo')
export class TipoInventarioController {
  constructor(private readonly tipoInventarioService: TipoInventarioService) {}

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Get('')
  getAll(@Request() req) {
    console.log('getAll tipo_de_Inventario');
    return this.tipoInventarioService.getAll(req);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Post()
  insert(@Body() body: CreateIdiomaDto) {
    console.log('insert tipo_de_inventario');
    return this.tipoInventarioService.insert(body);
  }
}
