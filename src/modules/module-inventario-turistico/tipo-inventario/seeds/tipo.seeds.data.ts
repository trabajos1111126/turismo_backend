export const tipoInventarioSeedData = [
  {
    es: 'Aguas subterráneas',
    en: 'Groundwater',
    fra: 'Eaux souterraines',
  },
  {
    es: 'Artísticos',
    en: 'Artistic',
    fra: 'Artistiques',
  },
  {
    es: 'Asentamientos humanos y arquitectura viva',
    en: 'Human settlements and living architecture',
    fra: 'Établissements humains et architecture vivante',
  },
  {
    es: 'Caminos y senderos pintorescos',
    en: 'Scenic roads and trails',
    fra: 'Routes et sentiers pittoresques',
  },
  {
    es: 'Centros científicos y técnicos',
    en: 'Scientific and technical centers',
    fra: 'Centres scientifiques et techniques',
  },
  {
    es: 'Deportivos',
    en: 'Sports-related',
    fra: 'Sportifs',
  },
  {
    es: 'Espectáculos, congresos y otros',
    en: 'Shows, conferences, and others',
    fra: 'Spectacles, conférences et autres',
  },
  {
    es: 'Explotaciones agropecuarias',
    en: 'Agricultural and livestock activities',
    fra: "Activités agricoles et d'élevage",
  },
  {
    es: 'Explotaciones industriales',
    en: 'Industrial operations',
    fra: 'Opérations industrielles',
  },
  {
    es: 'Explotaciones mineras',
    en: 'Mining operations',
    fra: 'Opérations minières',
  },
  {
    es: 'Fenómenos cársticos',
    en: 'Karstic phenomena',
    fra: 'Phénomènes karstiques',
  },
  {
    es: 'Folclore espiritual mental',
    en: 'Spiritual and mental folklore',
    fra: 'Folklore spirituel et mental',
  },
  {
    es: 'Folclore material artesanía',
    en: 'Material and artisanal folklore',
    fra: 'Folklore matériel et artisanal',
  },
  {
    es: 'Folclore social',
    en: 'Social folklore',
    fra: 'Folklore social',
  },
  {
    es: 'Formaciones geológicas y paleontológicas',
    en: 'Geological and paleontological formations',
    fra: 'Formations géologiques et paléontologiques',
  },
  {
    es: 'Grupos étnicos',
    en: 'Ethnic groups',
    fra: 'Groupes ethniques',
  },
  {
    es: 'Lagos',
    en: 'Lakes',
    fra: 'Lacs',
  },
  {
    es: 'Legado arqueológico',
    en: 'Archaeological legacy',
    fra: 'Patrimoine archéologique',
  },
  {
    es: 'Llanuras',
    en: 'Plains',
    fra: 'Plaines',
  },
  {
    es: 'Lugares de caza fotográfica y pesca deportiva',
    en: 'Photographic hunting and sports fishing spots',
    fra: 'Lieux de chasse photographique et de pêche sportive',
  },
  {
    es: 'Lugares de observación de flora y fauna',
    en: 'Flora and fauna observation sites',
    fra: "Lieux d'observation de la flore et de la faune",
  },
  {
    es: 'Montañas y cordilleras',
    en: 'Mountains and mountain ranges',
    fra: 'Montagnes et chaînes de montagnes',
  },
  {
    es: 'Museos y salas de exposición',
    en: 'Museums and exhibition halls',
    fra: "Musées et salles d'exposition",
  },
  {
    es: 'Obras arquitectónicas actuales',
    en: 'Current architectural works',
    fra: 'Œuvres architecturales actuelles',
  },
  {
    es: 'Obras de arte representativas',
    en: 'Representative works of art',
    fra: "Œuvres d'art représentatives",
  },
  {
    es: 'Obras de ingeniería',
    en: 'Engineering works',
    fra: "Travaux d'ingénierie",
  },
  {
    es: 'Puna, altiplano y valles',
    en: 'Puna, highlands, and valleys',
    fra: 'Puna, hauts plateaux et vallées',
  },
  {
    es: 'Ríos y caídas de agua',
    en: 'Rivers and waterfalls',
    fra: 'Rivières et cascades',
  },
  {
    es: 'Tierras insulares',
    en: 'Island lands',
    fra: 'Terres insulaires',
  },
  {
    es: 'Áreas naturales protegidas',
    en: 'Protected natural areas',
    fra: 'Aires naturelles protégées',
  },
];
