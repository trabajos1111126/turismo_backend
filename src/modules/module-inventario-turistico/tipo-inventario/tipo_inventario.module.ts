import { Module } from '@nestjs/common';
import { TipoInventarioService } from './tipo_inventario.service';
import { tipoInventarioProviders } from './tipo.inventario.providers';
import { TipoInventarioController } from './tipo_inventario.controller';

@Module({
  imports: [],
  controllers: [TipoInventarioController],
  providers: [TipoInventarioService, ...tipoInventarioProviders],
  exports: [TipoInventarioService],
})
export class TipoInventarioModule {}
