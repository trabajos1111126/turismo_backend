import { Module } from '@nestjs/common';
import { vocacionProviders } from './vocacion.providers';
import { VocacionService } from './vocacion.service';
import { VocacionController } from './vocacion.controller';

@Module({
  imports: [],
  controllers: [VocacionController],
  providers: [VocacionService, ...vocacionProviders],
  exports: [],
})
export class VocacionModule {}
