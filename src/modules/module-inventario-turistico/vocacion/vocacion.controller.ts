import {
  Controller,
  Get,
  Post,
  Request,
  UseGuards,
  UploadedFile,
  UseInterceptors,
  Body,
  Param,
  Patch,
  SetMetadata,
} from '@nestjs/common';
import { VocacionService } from './vocacion.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { CreateIdiomaDto } from 'src/modules/module-library/idioma/dto/create-idioma';

import { AuthGuard } from 'src/modules/module-usuario/auth/auth.guard';
import { multerConfigVocacion } from 'src/config/multer.config';
import { RolUsuarioEnum } from 'src/modules/module-usuario/usuario/enum/usuario.enum';

@Controller('api/vocacion')
export class VocacionController {
  constructor(private readonly vocacionService: VocacionService) {}

  @SetMetadata('roles', [
    RolUsuarioEnum.ADMINISTRADOR,
    RolUsuarioEnum.TURISTA,
    RolUsuarioEnum.ANONIMO,
  ])
  @UseGuards(AuthGuard)
  @Get('')
  getAll(@Request() req) {
    console.log('GetAll vocaciones');
    return this.vocacionService.getAll(req);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Post()
  insert(@Body() body: CreateIdiomaDto) {
    console.log('Insert vocaciones');
    return this.vocacionService.insert(body);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Patch('/:id/foto')
  @UseInterceptors(FileInterceptor('image', multerConfigVocacion))
  agregarFoto(
    @Param('id') id: number,
    @UploadedFile() file: Express.Multer.File,
  ) {
    console.log('Agregar Foto en vocaciones');
    return this.vocacionService.agregarFoto(id, file);
  }
}
