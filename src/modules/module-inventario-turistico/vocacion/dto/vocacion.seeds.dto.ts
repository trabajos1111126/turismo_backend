export class CreateVocacionSeedDto {
  nombre_es: string;
  nombre_en: string;
  nombre_fra: string;
  mimetype: string;
  foto: string;
}
