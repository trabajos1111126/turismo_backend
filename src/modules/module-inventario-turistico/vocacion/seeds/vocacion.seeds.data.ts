export const vocacionInventarioSeedData = [
  {
    nombre_es: 'Cultura',
    nombre_en: 'Culture',
    nombre_fra: 'Culture',
    foto: 'cultura.jpg',
    mimetype: 'image/jpg',
  },
  {
    nombre_es: 'Aventura',
    nombre_en: 'Adventure',
    nombre_fra: 'Aventure',
    foto: 'aventura.jpg',
    mimetype: 'image/jpg',
  },
  {
    nombre_es: 'Gastronomía',
    nombre_en: 'Gastronomy',
    nombre_fra: 'Gastronomie',
    foto: 'gastronomia.jpg',
    mimetype: 'image/jpg',
  },
  {
    nombre_es: 'Naturaleza',
    nombre_en: 'Nature',
    nombre_fra: 'Nature',
    foto: 'naturaleza.jpg',
    mimetype: 'image/jpg',
  },
];
