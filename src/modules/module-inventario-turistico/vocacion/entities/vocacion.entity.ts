import {
  Table,
  Column,
  Model,
  BelongsTo,
  ForeignKey,
  HasMany,
} from 'sequelize-typescript';
import { Idioma } from '../../../module-library/idioma/entity/idioma.entity';
import { InventarioTuristico } from '../../inventario-turistico/entities/inventario_turistico.entity';
import { Adjunto } from 'src/modules/module-library/adjunto/entities/adjunto.entity';

@Table({ tableName: 'vocacion' })
export class Vocacion extends Model {
  @Column({ primaryKey: true, autoIncrement: true }) //
  id_vocacion: number;

  @BelongsTo(() => Idioma)
  idioma: Idioma;

  @ForeignKey(() => Idioma)
  id_idioma: number;

  @BelongsTo(() => Adjunto)
  adjunto: Adjunto;

  @ForeignKey(() => Adjunto)
  id_adjunto: number;

  @HasMany(() => InventarioTuristico)
  inventarios_turisticos: InventarioTuristico[];

  @Column({ defaultValue: false })
  deleted: boolean;
}
