import { Vocacion } from 'src/modules/module-inventario-turistico/vocacion/entities/vocacion.entity';

export const vocacionProviders = [
  {
    provide: 'VOCACION_REPOSITORY',
    useValue: Vocacion,
  },
];
