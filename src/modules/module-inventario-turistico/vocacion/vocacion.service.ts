import {
  Injectable,
  Inject,
  NotFoundException,
  Body,
  UploadedFile,
} from '@nestjs/common';

import { Vocacion } from 'src/modules/module-inventario-turistico/vocacion/entities/vocacion.entity';

import { Idioma } from 'src/modules/module-library/idioma/entity/idioma.entity';
import { UsuarioHeaderDto } from 'src/modules/module-usuario/auth/dto/header-usuario';
import { CreateIdiomaDto } from 'src/modules/module-library/idioma/dto/create-idioma';
import { TipoIdioma } from 'src/modules/module-library/idioma/enum/status.enum';
import { Adjunto } from 'src/modules/module-library/adjunto/entities/adjunto.entity';

import * as path from 'path';
import { vocacionInventarioSeedData } from './seeds/vocacion.seeds.data';
import { CreateVocacionSeedDto } from './dto/vocacion.seeds.dto';
@Injectable()
export class VocacionService {
  constructor(
    @Inject('VOCACION_REPOSITORY')
    private vocacionRepository: typeof Vocacion,
  ) {}

  async seeds() {
    const count = await this.vocacionRepository.count();
    if (count === 0) {
      for (const element of vocacionInventarioSeedData) {
        await this.insertSeed(element);
      }
      console.log(
        'Todas las inserciones  de vocaciones de guia se han completado.',
      );
    }
  }
  async getAllPrueva() {
    return this.vocacionRepository.findAll({
      attributes: ['id_vocacion', 'id_adjunto'],
      include: [
        {
          model: Idioma,
        },
        {
          model: Adjunto,
        },
      ],
    });
  }
  async insertSeed(body: CreateVocacionSeedDto) {
    try {
      const traduccion = new Idioma();
      traduccion.es = body.nombre_es;
      traduccion.en = body.nombre_en;
      traduccion.fra = body.nombre_fra;
      traduccion.tipo = TipoIdioma.VOCACION_INVENTARIO;
      const traduccion_guardado = await traduccion.save();

      const adjunto = new Adjunto();
      adjunto.url_foto = 'images/vocacion/' + body.foto;
      adjunto.descripcion = 'imagen de vocacion';
      adjunto.mimetype = body.mimetype;
      const adjunto_guardado = await adjunto.save();

      const vocacion = new Vocacion();
      vocacion.idioma = traduccion_guardado;
      vocacion.id_idioma = traduccion_guardado.id_idioma;
      vocacion.id_adjunto = adjunto_guardado.id_adjunto;
      const vocacion_guardado = await vocacion.save();
      return vocacion_guardado;
    } catch (error) {
      throw new NotFoundException(error.message);
    }
  }
  async insert(@Body() body: CreateIdiomaDto) {
    try {
      const traduccion = new Idioma();
      traduccion.es = body.es;
      traduccion.en = body.en;
      traduccion.fra = body.fra;
      traduccion.tipo = TipoIdioma.VOCACION_INVENTARIO;
      const traduccion_guardado = await traduccion.save();

      const vocacion = new Vocacion();
      vocacion.idioma = traduccion_guardado;
      vocacion.id_idioma = traduccion_guardado.id_idioma;
      vocacion.id_adjunto = 1;
      const vocacion_guardado = await vocacion.save();
      return vocacion_guardado;
    } catch (error) {
      throw new NotFoundException(error.message);
    }
  }
  async agregarFoto(id: number, @UploadedFile() file: Express.Multer.File) {
    const vocacion = await this.vocacionRepository.findByPk(id);
    const adjunto = new Adjunto();
    adjunto.url_foto = 'images/vocacion/' + file.filename;
    adjunto.descripcion = 'imagen de vocacion';
    adjunto.mimetype = file.mimetype;
    const adjunto_guardado = await adjunto.save();

    vocacion.id_adjunto = adjunto_guardado.id_adjunto;
    vocacion.adjunto = adjunto_guardado;
    vocacion.save();
    return vocacion;
  }

  async getAll(req: UsuarioHeaderDto) {
    try {
      const inventarios = await this.vocacionRepository.findAll({
        attributes: ['id_vocacion'],
        include: [
          {
            model: Idioma,
            attributes: [[req.idioma, 'idioma']],
          },
          {
            model: Adjunto,
          },
        ],
      });

      const data = await inventarios.map((item) => ({
        id_vocacion: item.id_vocacion,
        nombre: item.idioma.dataValues.idioma,
        url_file: item.adjunto.url_foto,
      }));
      return data;
    } catch (error) {
      throw new NotFoundException(error.message);
    }
  }

  async agregarImagen(@UploadedFile() file: Express.Multer.File) {
    try {
      const fileName = file.filename;
      const baseDir = process.cwd();
      return path.join(baseDir, '/uploads/vocacion', fileName);
    } catch (error) {
      return error;
    }
  }
}
