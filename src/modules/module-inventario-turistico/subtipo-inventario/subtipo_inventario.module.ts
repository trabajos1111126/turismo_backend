import { Module } from '@nestjs/common';
import { SubtipoInventarioController } from './subtipo_inventario.controller';
import { SubtipoInventarioService } from './subtipo_inventario.service';
import { subtipoInventarioProviders } from './subtipo.inventario.providers';

@Module({
  imports: [],
  controllers: [SubtipoInventarioController],
  providers: [SubtipoInventarioService, ...subtipoInventarioProviders],
  exports: [],
})
export class SubtipoInventarioModule {}
