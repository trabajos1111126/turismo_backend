import { SubTipoInventario } from 'src/modules/module-inventario-turistico/subtipo-inventario/entities/subtipo_inventario.entity';

export const subtipoInventarioProviders = [
  {
    provide: 'SUBTIPO_INVENTARIO_REPOSITORY',
    useValue: SubTipoInventario,
  },
];
