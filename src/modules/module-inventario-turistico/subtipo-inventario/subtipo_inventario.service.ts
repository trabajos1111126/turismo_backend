import { Injectable, Inject, NotFoundException } from '@nestjs/common';
import { CreateIdiomaDto } from 'src/modules/module-library/idioma/dto/create-idioma';
import { Idioma } from 'src/modules/module-library/idioma/entity/idioma.entity';
import { TipoIdioma } from 'src/modules/module-library/idioma/enum/status.enum';
import { SubTipoInventario } from 'src/modules/module-inventario-turistico/subtipo-inventario/entities/subtipo_inventario.entity';
import { UsuarioHeaderDto } from 'src/modules/module-usuario/auth/dto/header-usuario';
import { subtipoInventarioSeedData } from './seeds/subtipo.seeds.data';

@Injectable()
export class SubtipoInventarioService {
  constructor(
    @Inject('SUBTIPO_INVENTARIO_REPOSITORY')
    private subtipoInventarioRepository: typeof SubTipoInventario,
  ) {}

  async seeds() {
    const count = await this.subtipoInventarioRepository.count();
    if (count === 0) {
      for (const element of subtipoInventarioSeedData) {
        await this.insert(element);
      }
    }
  }

  async getAll(req: UsuarioHeaderDto) {
    try {
      const subtipo = await this.subtipoInventarioRepository.findAll({
        attributes: ['id_subtipo'],
        include: [
          {
            model: Idioma,
            attributes: [[req.idioma, 'idioma']],
          },
        ],
      });

      const data = await subtipo.map((item) => ({
        id_subtipo: item.id_subtipo,
        nombre: item.idioma.dataValues.idioma,
      }));
      return data;
    } catch (error) {
      throw new NotFoundException(error.message);
    }
  }

  async insert(body: CreateIdiomaDto) {
    try {
      const traduccion = new Idioma();
      traduccion.es = body.es;
      traduccion.en = body.en;
      traduccion.fra = body.fra;
      traduccion.tipo = TipoIdioma.SUBITPO_INVENTARIO;
      const traduccion_guardado = await traduccion.save();
      const subTipoInventario = new SubTipoInventario();
      subTipoInventario.idioma = traduccion_guardado;
      subTipoInventario.id_idioma = traduccion_guardado.id_idioma;

      const subTipoInventario_guardado = await subTipoInventario.save();
      return subTipoInventario_guardado;
    } catch (error) {
      return error;
    }
  }
}
