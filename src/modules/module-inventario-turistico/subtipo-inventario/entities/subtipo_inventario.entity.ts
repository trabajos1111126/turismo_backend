import {
  Table,
  Column,
  Model,
  BelongsTo,
  ForeignKey,
  HasMany,
} from 'sequelize-typescript';
import { Idioma } from '../../../module-library/idioma/entity/idioma.entity';
import { InventarioTuristico } from '../../inventario-turistico/entities/inventario_turistico.entity';

@Table({ tableName: 'subtipo' })
export class SubTipoInventario extends Model {
  @Column({ primaryKey: true, autoIncrement: true })
  id_subtipo: number;

  @BelongsTo(() => Idioma)
  idioma: Idioma;

  @ForeignKey(() => Idioma)
  id_idioma: number;

  @HasMany(() => InventarioTuristico)
  inventarios_turisticos: InventarioTuristico[];

  @Column({ defaultValue: false })
  deleted: boolean;
}
