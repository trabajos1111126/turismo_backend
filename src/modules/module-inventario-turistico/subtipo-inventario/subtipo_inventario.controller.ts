import {
  Body,
  Controller,
  Get,
  Post,
  Request,
  SetMetadata,
  UseGuards,
} from '@nestjs/common';

import { AuthGuard } from 'src/modules/module-usuario/auth/auth.guard';
import { CreateIdiomaDto } from 'src/modules/module-library/idioma/dto/create-idioma';
import { SubtipoInventarioService } from './subtipo_inventario.service';
import { RolUsuarioEnum } from 'src/modules/module-usuario/usuario/enum/usuario.enum';
@Controller('api/subtipo')
export class SubtipoInventarioController {
  constructor(
    private readonly subtipoInventarioService: SubtipoInventarioService,
  ) {}

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Get('')
  getAll(@Request() req) {
    console.log('getAll subtipo_inventario');
    return this.subtipoInventarioService.getAll(req);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Post()
  insert(@Body() body: CreateIdiomaDto) {
    console.log('insert subtipo_inventario');
    return this.subtipoInventarioService.insert(body);
  }
}
