export const subtipoInventarioSeedData = [
  { es: 'Abras', en: 'Abras', fra: 'Abras' },
  {
    es: 'Acueductos',
    en: 'Aqueducts',
    fra: 'Aqueducs',
  },
  {
    es: 'Aguas minerales frías',
    en: 'Cold mineral waters',
    fra: 'Eaux minérales froides',
  },
  {
    es: 'Aguas termales',
    en: 'Hot springs',
    fra: 'Sources thermales',
  },
  {
    es: 'Altiplano',
    en: 'Plateau',
    fra: 'Plateau',
  },
  {
    es: 'Antropológico',
    en: 'Anthropological',
    fra: 'Anthropologique',
  },
  {
    es: 'Arqueológico',
    en: 'Archaeological',
    fra: 'Archéologique',
  },
  {
    es: 'Arquitectura y arte',
    en: 'Architecture and art',
    fra: 'Architecture et art',
  },
  {
    es: 'Arte rupestre',
    en: 'Rock art',
    fra: 'Art rupestre',
  },
  {
    es: 'Artes',
    en: 'Arts',
    fra: 'Arts',
  },
  {
    es: 'Bahías',
    en: 'Bays',
    fra: 'Baies',
  },
  {
    es: 'Bibliotecas',
    en: 'Libraries',
    fra: 'Bibliothèques',
  },
  {
    es: 'Bocaminas',
    en: 'Mine entrances',
    fra: 'Entrées de mine',
  },
  {
    es: 'Bordados',
    en: 'Embroidery',
    fra: 'Broderie',
  },
  {
    es: 'Bosques',
    en: 'Forests',
    fra: 'Forêts',
  },
  {
    es: 'Caminos prehispánicos',
    en: 'Pre-Hispanic paths',
    fra: 'Chemins préhispaniques',
  },
  {
    es: 'Carnavales',
    en: 'Carnivals',
    fra: 'Carnavals',
  },
  {
    es: 'Carpintería',
    en: 'Carpentry',
    fra: 'Menuiserie',
  },
  {
    es: 'Cascadas, cataratas o saltos',
    en: 'Waterfalls',
    fra: "Chutes d'eau",
  },
  {
    es: 'Cavernas',
    en: 'Caverns',
    fra: 'Grottes',
  },
  {
    es: 'Caza fotográfica',
    en: 'Photographic hunting',
    fra: 'Chasse photographique',
  },
  {
    es: 'Cañones o desfiladeros',
    en: 'Canyons',
    fra: 'Canyons',
  },
  {
    es: 'Centros de importancia',
    en: 'Centers of importance',
    fra: "Centres d'importance",
  },
  {
    es: 'Centros de producción',
    en: 'Production centers',
    fra: 'Centres de production',
  },
  {
    es: 'Centros poblados y ciudades',
    en: 'Settlements and cities',
    fra: 'Établissements et villes',
  },
  {
    es: 'Cerámica',
    en: 'Ceramics',
    fra: 'Céramique',
  },
  {
    es: 'Cestería',
    en: 'Basketry',
    fra: 'Vannerie',
  },
  {
    es: 'Ciénagas',
    en: 'Marshes',
    fra: 'Marais',
  },
  {
    es: 'Comidas y bebidas',
    en: 'Food and drinks',
    fra: 'Nourriture et boissons',
  },
  {
    es: 'Complejos',
    en: 'Complexes',
    fra: 'Complexes',
  },
  {
    es: 'Complejos recreacionales',
    en: 'Recreational complexes',
    fra: 'Complexes récréatifs',
  },
  {
    es: 'Concursos de belleza',
    en: 'Beauty pageants',
    fra: 'Concours de beauté',
  },
  {
    es: 'Confluencias',
    en: 'Confluences',
    fra: 'Confluences',
  },
  {
    es: 'Convenciones y congresos',
    en: 'Conventions and congresses',
    fra: 'Conventions et congrès',
  },
  {
    es: 'Corridas de toros',
    en: 'Bullfights',
    fra: 'Corridas de toros',
  },
  {
    es: 'Cuevas',
    en: 'Caves',
    fra: 'Grottes',
  },
  {
    es: 'Danzas y bailes',
    en: 'Dances and dances',
    fra: 'Danses et danses',
  },
  {
    es: 'Depresiones',
    en: 'Depressions',
    fra: 'Dépressions',
  },
  {
    es: 'Depresiones húmedas',
    en: 'Wet depressions',
    fra: 'Dépressions humides',
  },
  {
    es: 'Desiertos',
    en: 'Deserts',
    fra: 'Déserts',
  },
  {
    es: 'Diques',
    en: 'Dams',
    fra: 'Barrages',
  },
  {
    es: 'Edificaciones de importancia',
    en: 'Significant buildings',
    fra: 'Bâtiments significatifs',
  },
  {
    es: 'Efectos ópticos',
    en: 'Optical effects',
    fra: 'Effets optiques',
  },
  {
    es: 'Esculturas, pinturas',
    en: 'Sculptures, paintings',
    fra: 'Sculptures, peintures',
  },
  {
    es: 'Esoterismo',
    en: 'Esotericism',
    fra: 'Esotérisme',
  },
  {
    es: 'Estaciones experimentales',
    en: 'Experimental stations',
    fra: 'Stations expérimentales',
  },
  {
    es: 'Etnografía y folclore',
    en: 'Ethnography and folklore',
    fra: 'Ethnographie et folklore',
  },
  {
    es: 'Explotaciones a cielo abierto',
    en: 'Open-pit mining',
    fra: 'Exploitations à ciel ouvert',
  },
  {
    es: 'Exposiciones de artes',
    en: 'Art exhibitions',
    fra: "Expositions d'art",
  },
  {
    es: 'Expresiones de música y danza contemporánea',
    en: 'Expressions of contemporary music and dance',
    fra: 'Expressions de musique et de danse contemporaine',
  },
  {
    es: 'Fabricas',
    en: 'Factories',
    fra: 'Usines',
  },
  {
    es: 'Fauna',
    en: 'Fauna',
    fra: 'Faune',
  },
  {
    es: 'Ferias nacionales e internacionales',
    en: 'National and international fairs',
    fra: 'Foires nationales et internationales',
  },
  {
    es: 'Ferias y mercados',
    en: 'Fairs and markets',
    fra: 'Foires et marchés',
  },
  {
    es: 'Festivales de cine',
    en: 'Film festivals',
    fra: 'Festivals de cinéma',
  },
  {
    es: 'Festivales de teatro',
    en: 'Theater festivals',
    fra: 'Festivals de théâtre',
  },
  {
    es: 'Fiestas populares y religiosas',
    en: 'Popular and religious festivals',
    fra: 'Fêtes populaires et religieuses',
  },
  {
    es: 'Flora',
    en: 'Flora',
    fra: 'Flore',
  },
  {
    es: 'Formaciones rocosas',
    en: 'Rock formations',
    fra: 'Formations rocheuses',
  },
  {
    es: 'Fumarolas y géisers',
    en: 'Fumaroles and geysers',
    fra: 'Fumerolles et geysers',
  },
  {
    es: 'Glaciales',
    en: 'Glaciers',
    fra: 'Glaciers',
  },
  {
    es: 'Grupos étnicos',
    en: 'Ethnic groups',
    fra: 'Groupes ethniques',
  },
  {
    es: 'Grutas',
    en: 'Caves',
    fra: 'Grottes',
  },
  {
    es: 'Historia natural',
    en: 'Natural history',
    fra: 'Histoire naturelle',
  },
  {
    es: 'Histórico cultural',
    en: 'Cultural history',
    fra: 'Histoire culturelle',
  },
  {
    es: 'Imaginería',
    en: 'Imagery',
    fra: 'Imagerie',
  },
  {
    es: 'Industrias líticas',
    en: 'Lithic industries',
    fra: 'Industries lithiques',
  },
  {
    es: 'Ingenios',
    en: 'Sugarcane mills',
    fra: 'Moulins à sucre',
  },
  {
    es: 'Institutos de investigación',
    en: 'Research institutes',
    fra: 'Instituts de recherche',
  },
  {
    es: 'Instrumentos musicales',
    en: 'Musical instruments',
    fra: 'Instruments de musique',
  },
  {
    es: 'Internacional',
    en: 'International',
    fra: 'International',
  },
  {
    es: 'Islas en salinas',
    en: 'Islands in salt flats',
    fra: 'Îles dans les salines',
  },
  {
    es: 'Islas fluviales',
    en: 'River islands',
    fra: 'Îles fluviales',
  },
  {
    es: 'Islas lacustres',
    en: 'Lake islands',
    fra: 'Îles lacustres',
  },
  {
    es: 'Jardín botánico',
    en: 'Botanical garden',
    fra: 'Jardin botanique',
  },
  {
    es: 'Joyería',
    en: 'Jewelry',
    fra: 'Joallerie',
  },
  {
    es: 'Lagos',
    en: 'Lakes',
    fra: 'Lacs',
  },
  {
    es: 'Lagos amargos',
    en: 'Bitter lakes',
    fra: 'Lacs amers',
  },
  {
    es: 'Lagos salinos',
    en: 'Saltwater lakes',
    fra: 'Lacs salés',
  },
  {
    es: 'Lagunas',
    en: 'Lagoons',
    fra: 'Lagunes',
  },
  {
    es: 'Lagunas glaciares',
    en: 'Glacial lagoons',
    fra: 'Lagunes glaciaires',
  },
  {
    es: 'Lavaderos',
    en: 'Laundries',
    fra: 'Lavoirs',
  },
  {
    es: 'Llanuras selváticas',
    en: 'Rainforest plains',
    fra: 'Plaines de forêt tropicale',
  },
  {
    es: 'Local',
    en: 'Local',
    fra: 'Local',
  },
  {
    es: 'Lomas o colinas',
    en: 'Hills or hillocks',
    fra: 'Collines ou monticules',
  },
  {
    es: 'Lugares de interés histórico',
    en: 'Historical places of interest',
    fra: "Lieux d'intérêt historique",
  },
  {
    es: 'Macizo',
    en: 'Massif',
    fra: 'Massif',
  },
  {
    es: 'Magia',
    en: 'Magic',
    fra: 'Magie',
  },
  {
    es: 'Manantial o fuente',
    en: 'Spring or fountain',
    fra: 'Source ou fontaine',
  },
  {
    es: 'Manifestaciones y creencias populares',
    en: 'Popular manifestations and beliefs',
    fra: 'Manifestations populaires et croyances',
  },
  {
    es: 'Meandros o curvas',
    en: 'Meanders or curves',
    fra: 'Méandres ou courbes',
  },
  {
    es: 'Medicina popular',
    en: 'Folk medicine',
    fra: 'Médecine populaire',
  },
  {
    es: 'Mesetas',
    en: 'Plateaus',
    fra: 'Plateaux',
  },
  {
    es: 'Metalurgia',
    en: 'Metallurgy',
    fra: 'Métallurgie',
  },
  {
    es: 'Minas',
    en: 'Mines',
    fra: 'Mines',
  },
  {
    es: 'Mineralógico',
    en: 'Mineralogical',
    fra: 'Minéralogique',
  },
  {
    es: 'Miniaturas',
    en: 'Miniatures',
    fra: 'Miniatures',
  },
  {
    es: 'Montes',
    en: 'Mountains',
    fra: 'Montagnes',
  },
  {
    es: 'Monumentos naturales',
    en: 'Natural monuments',
    fra: 'Monuments naturels',
  },
  {
    es: 'Monumentos, edificios, obras de arquitectura',
    en: 'Monuments, buildings, architectural works',
    fra: 'Monuments, bâtiments, œuvres architecturales',
  },
  {
    es: 'Murales',
    en: 'Murals',
    fra: 'Fresques murales',
  },
  {
    es: 'Máscaras',
    en: 'Masks',
    fra: 'Masques',
  },
  {
    es: 'Música',
    en: 'Music',
    fra: 'Musique',
  },
  {
    es: 'Nacional',
    en: 'National',
    fra: 'National',
  },
  {
    es: 'Observatorios astronómicos',
    en: 'Astronomical observatories',
    fra: 'Observatoires astronomiques',
  },
  {
    es: 'Ojos de agua',
    en: 'Water sources',
    fra: "Sources d'eau",
  },
  {
    es: 'Parques nacionales',
    en: 'National parks',
    fra: 'Parcs nationaux',
  },
  {
    es: 'Penínsulas',
    en: 'Peninsulas',
    fra: 'Péninsules',
  },
  {
    es: 'Peregrinaciones',
    en: 'Pilgrimages',
    fra: 'Pèlerinages',
  },
  {
    es: 'Pesca deportiva',
    en: 'Sport fishing',
    fra: 'Pêche sportive',
  },
  {
    es: 'Picos/Nevados',
    en: 'Peaks/Snow-capped mountains',
    fra: 'Sommet/Montagnes enneigées',
  },
  {
    es: 'Praderas',
    en: 'Meadows',
    fra: 'Prairies',
  },
  {
    es: 'Puentes',
    en: 'Bridges',
    fra: 'Ponts',
  },
  {
    es: 'Regiones de interés',
    en: 'Regions of interest',
    fra: "Régions d'intérêt",
  },
  {
    es: 'Religioso',
    en: 'Religious',
    fra: 'Religieux',
  },
  {
    es: 'Represas',
    en: 'Dams',
    fra: 'Barrages',
  },
  {
    es: 'Reservas de vida silvestre',
    en: 'Wildlife reserves',
    fra: 'Réserves de faune',
  },
  {
    es: 'Reservas naturales de inmovilización',
    en: 'Natural immobilization reserves',
    fra: "Réserves naturelles d'immobilisation",
  },
  {
    es: 'Reservas particulares',
    en: 'Private reserves',
    fra: 'Réserves privées',
  },
  {
    es: 'Riachuelo o arroyo',
    en: 'Stream or creek',
    fra: "Ruisseau ou cours d'eau",
  },
  {
    es: 'Riberas',
    en: 'Riverbanks',
    fra: 'Rives de la rivière',
  },
  {
    es: 'Riberas de ingenios',
    en: 'Mill riverbanks',
    fra: 'Rives des moulins à sucre',
  },
  {
    es: 'Rodeo',
    en: 'Rodeo',
    fra: 'Rodeo',
  },
  {
    es: 'Rápidos y cachuelas',
    en: 'Rapids and pools',
    fra: 'Rapides et bassins',
  },
  {
    es: 'Sabanas o llanos',
    en: 'Savannas or plains',
    fra: 'Savanes ou plaines',
  },
  {
    es: 'Salas de exposición',
    en: 'Exhibition rooms',
    fra: "Salles d'exposition",
  },
  {
    es: 'Salinas',
    en: 'Salt flats',
    fra: 'Salines',
  },
  {
    es: 'Santuarios de flora y fauna silvestre',
    en: 'Wildlife sanctuaries',
    fra: 'Sanctuaires de faune et de flore',
  },
  {
    es: 'Sendas peatonales',
    en: 'Footpaths',
    fra: 'Sentiers pédestres',
  },
  {
    es: 'Sierras',
    en: 'Mountain ranges',
    fra: 'Chaînes de montagnes',
  },
  {
    es: 'Sitios o conjuntos',
    en: 'Sites or ensembles',
    fra: 'Sites ou ensembles',
  },
  {
    es: 'Sitios o yacimientos paleontológicos',
    en: 'Paleontological sites or deposits',
    fra: 'Sites ou gisements paléontologiques',
  },
  {
    es: 'Tejidos',
    en: 'Textiles',
    fra: 'Textiles',
  },
  {
    es: 'Teleféricos',
    en: 'Cable cars',
    fra: 'Téléphériques',
  },
  {
    es: 'Tocados de pluma',
    en: 'Feather headdresses',
    fra: 'Coiffes en plumes',
  },
  {
    es: 'Torrentes',
    en: 'Torrents',
    fra: 'Torrents',
  },
  {
    es: 'Trabajos en cuero',
    en: 'Leather work',
    fra: 'Travail du cuir',
  },
  {
    es: 'Túneles',
    en: 'Tunnels',
    fra: 'Tunnels',
  },
  {
    es: 'Valles',
    en: 'Valleys',
    fra: 'Vallées',
  },
  {
    es: 'Volcanes/Nevados',
    en: 'Volcanoes/Snow-capped mountains',
    fra: 'Volcans/Montagnes enneigées',
  },
  {
    es: 'Yugo de buey',
    en: 'Ox yoke',
    fra: 'Joug de bœuf',
  },
  {
    es: 'Yungas',
    en: 'Yungas',
    fra: 'Yungas',
  },
  {
    es: 'Zoológicos, acuarios, mariposarios',
    en: 'Zoos, aquariums, butterfly houses',
    fra: 'Zoos, aquariums, maisons de papillons',
  },
  {
    es: 'Área natural de manejo integrado',
    en: 'Natural integrated management area',
    fra: 'Zone de gestion intégrée naturelle',
  },
];
