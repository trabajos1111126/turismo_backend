import {
  Table,
  Column,
  Model,
  BelongsTo,
  ForeignKey,
} from 'sequelize-typescript';
import { Idioma } from '../../../module-library/idioma/entity/idioma.entity';

@Table({ tableName: 'modalidad' })
export class Modalidad extends Model {
  @Column({ primaryKey: true, autoIncrement: true }) // autoIncrement: true
  id_modalidad: number;

  @BelongsTo(() => Idioma)
  idioma: Idioma;

  @ForeignKey(() => Idioma)
  id_idioma: number;

  @Column({ defaultValue: false }) // Agregamos el campo "deleted" con valor por defecto en "false"
  deleted: boolean;
}
