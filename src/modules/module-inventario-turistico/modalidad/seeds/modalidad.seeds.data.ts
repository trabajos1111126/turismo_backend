export const modalidadInventarioSeedData = [
  {
    es: 'Ecoturismo',
    en: 'Ecotourism',
    fra: 'Écotourisme',
  },
  {
    es: 'Turismo costero, marítimo y de aguas interiores',
    en: 'Coastal, Maritime and Inland Waters Tourism',
    fra: 'Tourisme côtier, maritime et des eaux intérieures',
  },
  {
    es: 'Turismo cultural',
    en: 'Cultural Tourism',
    fra: 'Tourisme culturel',
  },
  {
    es: 'Turismo de aventura',
    en: 'Adventure Tourism',
    fra: "Tourisme d'aventure",
  },
  {
    es: 'Turismo de bienestar',
    en: 'Wellness Tourism',
    fra: 'Tourisme de bien-être',
  },
  {
    es: 'Turismo de montaña',
    en: 'Mountain Tourism',
    fra: 'Tourisme de montagne',
  },
  {
    es: 'Turismo de negocios',
    en: 'Business Tourism',
    fra: "Tourisme d'affaires",
  },
  {
    es: 'Turismo de salud',
    en: 'Health Tourism',
    fra: 'Tourisme de santé',
  },
  {
    es: 'Turismo deportivo',
    en: 'Sports Tourism',
    fra: 'Tourisme sportif',
  },
  {
    es: 'Turismo educativo',
    en: 'Educational Tourism',
    fra: 'Tourisme éducatif',
  },
  {
    es: 'Turismo gastronómico',
    en: 'Gastronomic Tourism',
    fra: 'Tourisme gastronomique',
  },
  {
    es: 'Turismo médico',
    en: 'Medical Tourism',
    fra: 'Tourisme médical',
  },
  {
    es: 'Turismo rural',
    en: 'Rural Tourism',
    fra: 'Tourisme rural',
  },
  {
    es: 'Turismo urbano',
    en: 'Urban Tourism',
    fra: 'Tourisme urbain',
  },
];
