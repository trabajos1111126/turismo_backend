import { Module } from '@nestjs/common';
import { modalidadProviders } from './modalidad.providers';
import { ModalidadService } from './modalidad.service';
import { ModalidadController } from './modalidad.controller';

@Module({
  imports: [],
  controllers: [ModalidadController],
  providers: [ModalidadService, ...modalidadProviders],
  exports: [],
})
export class ModalidadModule {}
