import { Modalidad } from 'src/modules/module-inventario-turistico/modalidad/entities/modalidad.entity';

export const modalidadProviders = [
  {
    provide: 'MODALIDAD_REPOSITORY',
    useValue: Modalidad,
  },
];
