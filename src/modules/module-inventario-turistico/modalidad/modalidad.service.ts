import { Injectable, Inject, NotFoundException } from '@nestjs/common';
import { CreateIdiomaDto } from 'src/modules/module-library/idioma/dto/create-idioma';
import { Idioma } from 'src/modules/module-library/idioma/entity/idioma.entity';
import { TipoIdioma } from 'src/modules/module-library/idioma/enum/status.enum';
import { Modalidad } from 'src/modules/module-inventario-turistico/modalidad/entities/modalidad.entity';
import { UsuarioHeaderDto } from 'src/modules/module-usuario/auth/dto/header-usuario';
import { modalidadInventarioSeedData } from './seeds/modalidad.seeds.data';

@Injectable()
export class ModalidadService {
  constructor(
    @Inject('MODALIDAD_REPOSITORY')
    private modalidadRepository: typeof Modalidad,
  ) {}

  async seeds() {
    const count = await this.modalidadRepository.count();
    if (count === 0) {
      for (const element of modalidadInventarioSeedData) {
        await this.insert(element);
      }
    }
  }

  async getAll(req: UsuarioHeaderDto) {
    try {
      const inventarios = await this.modalidadRepository.findAll({
        attributes: ['id_modalidad'],
        include: [
          {
            model: Idioma,
            attributes: [[req.idioma, 'idioma']],
          },
        ],
      });

      const data = await inventarios.map((item) => ({
        id_modalidad: item.id_modalidad,
        nombre: item.idioma.dataValues.idioma,
      }));
      return data;
    } catch (error) {
      throw new NotFoundException(error.message);
    }
  }

  async insert(body: CreateIdiomaDto) {
    try {
      const traduccion = new Idioma();
      traduccion.es = body.es;
      traduccion.en = body.en;
      traduccion.fra = body.fra;
      traduccion.tipo = TipoIdioma.MODALIDAD_INVENTARIO;
      const traduccion_guardado = await traduccion.save();

      const modalidad = new Modalidad();
      modalidad.idioma = traduccion_guardado;
      modalidad.id_idioma = traduccion_guardado.id_idioma;

      const modalidad_guardado = await modalidad.save();

      return modalidad_guardado;
    } catch (error) {
      return error;
    }
  }
}
