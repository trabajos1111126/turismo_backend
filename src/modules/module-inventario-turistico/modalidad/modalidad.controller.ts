import {
  Body,
  Controller,
  Get,
  Post,
  Request,
  SetMetadata,
  UseGuards,
} from '@nestjs/common';

import { AuthGuard } from 'src/modules/module-usuario/auth/auth.guard';
import { CreateIdiomaDto } from 'src/modules/module-library/idioma/dto/create-idioma';
import { ModalidadService } from './modalidad.service';
import { RolUsuarioEnum } from 'src/modules/module-usuario/usuario/enum/usuario.enum';

@Controller('api/modalidad')
export class ModalidadController {
  constructor(private readonly modalidadService: ModalidadService) {}

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Get('')
  getAll(@Request() req) {
    console.log('getAll modalidad_Inventario');
    return this.modalidadService.getAll(req);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Post()
  insert(@Body() body: CreateIdiomaDto) {
    console.log('Insert modalidad_inventario');
    return this.modalidadService.insert(body);
  }
}
