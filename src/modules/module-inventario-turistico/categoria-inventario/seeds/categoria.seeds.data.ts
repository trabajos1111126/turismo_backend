export const categoriaInventarioSeedData = [
  {
    es: 'Acontecimientos Programados',
    en: 'Scheduled Events',
    fra: 'Événements Programmés',
  },
  {
    es: 'Etnografía y Folclore',
    en: 'Ethnography and Folklore',
    fra: 'Ethnographie et Folklore',
  },
  {
    es: 'Patrimonio Urbano, Arquitectónico y Artístico, Museos y Manifestaciones Culturales',
    en: 'Urban, Architectural and Artistic Heritage, Museums and Cultural Manifestations',
    fra: 'Patrimoine Urbain, Architectural et Artistique, Musées et Manifestations Culturelles',
  },
  {
    es: 'Realizaciones Técnicas Científicas',
    en: 'Technical Scientific Achievements',
    fra: 'Réalisations Techniques Scientifiques',
  },
  {
    es: 'Sitios Naturales',
    en: 'Natural Sites',
    fra: 'Sites Naturels',
  },
];
