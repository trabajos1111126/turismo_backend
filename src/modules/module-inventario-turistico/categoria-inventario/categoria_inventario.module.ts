import { Module } from '@nestjs/common';
import { categoriaInventarioProviders } from './categoria_inventario.providers';
import { CategoriaInventarioService } from './categoria_inventario.service';
import { CategoriaInventarioController } from './categoria_inventario.controller';

@Module({
  imports: [],
  controllers: [CategoriaInventarioController],
  providers: [CategoriaInventarioService, ...categoriaInventarioProviders],
  exports: [],
})
export class CategoriaInventarioModule {}
