import { Injectable, Inject, NotFoundException } from '@nestjs/common';
import { CategoriaInventario } from 'src/modules/module-inventario-turistico/categoria-inventario/entities/categoria_inventario.entity';

import { Idioma } from 'src/modules/module-library/idioma/entity/idioma.entity';
import { UsuarioHeaderDto } from 'src/modules/module-usuario/auth/dto/header-usuario';
import { CreateIdiomaDto } from 'src/modules/module-library/idioma/dto/create-idioma';
import { TipoIdioma } from 'src/modules/module-library/idioma/enum/status.enum';
import { categoriaInventarioSeedData } from './seeds/categoria.seeds.data';

@Injectable()
export class CategoriaInventarioService {
  constructor(
    @Inject('CATEGORIA_INVENTARIO_REPOSITORY')
    private categoriaInventarioRepository: typeof CategoriaInventario,
  ) {}

  async seeds() {
    const count = await this.categoriaInventarioRepository.count();
    if (count === 0) {
      for (const element of categoriaInventarioSeedData) {
        await this.insert(element);
      }
      
      console.log('Todas las inserciones se han completado.');
    }
  }

  async getAll(req: UsuarioHeaderDto) {
    try {
      const inventarios = await this.categoriaInventarioRepository.findAll({
        attributes: ['id_categoria'],
        include: [
          {
            model: Idioma,
            attributes: [[req.idioma, 'idioma']],
          },
        ],
      });

      const data = await inventarios.map((item) => ({
        id_categoria: item.id_categoria,
        nombre: item.idioma.dataValues.idioma,
      }));
      return data;
    } catch (error) {
      throw new NotFoundException(error.message);
    }
  }

  async insert(body: CreateIdiomaDto) {
    try {
      const traduccion = new Idioma();
      traduccion.es = body.es;
      traduccion.en = body.en;
      traduccion.fra = body.fra;
      traduccion.tipo = TipoIdioma.CATEGORIA_INVENTARIO;
      const traduccion_guardado = await traduccion.save();

      const categoriaInventario = new CategoriaInventario();
      categoriaInventario.idioma = traduccion_guardado;
      categoriaInventario.id_idioma = traduccion_guardado.id_idioma;

      const categoria_inventario_guardado = await categoriaInventario.save();

      return categoria_inventario_guardado;
    } catch (error) {
      throw new NotFoundException(error.message);
    }
  }
}
