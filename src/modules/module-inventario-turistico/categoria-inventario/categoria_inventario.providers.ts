import { CategoriaInventario } from 'src/modules/module-inventario-turistico/categoria-inventario/entities/categoria_inventario.entity';

export const categoriaInventarioProviders = [
  {
    provide: 'CATEGORIA_INVENTARIO_REPOSITORY',
    useValue: CategoriaInventario,
  },
];
