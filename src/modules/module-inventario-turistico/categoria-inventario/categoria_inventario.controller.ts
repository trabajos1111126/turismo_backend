import {
  Body,
  Controller,
  Get,
  Post,
  Request,
  SetMetadata,
  UseGuards,
} from '@nestjs/common';

import { CategoriaInventarioService } from './categoria_inventario.service';
import { AuthGuard } from 'src/modules/module-usuario/auth/auth.guard';
import { CreateIdiomaDto } from 'src/modules/module-library/idioma/dto/create-idioma';
import { RolUsuarioEnum } from 'src/modules/module-usuario/usuario/enum/usuario.enum';
@Controller('api/categoria')
export class CategoriaInventarioController {
  constructor(
    private readonly categoriaInventarioService: CategoriaInventarioService,
  ) {}
  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Get('')
  getAll(@Request() req) {
    console.log('getAll categoria_invetarios');
    return this.categoriaInventarioService.getAll(req);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Post()
  insert(@Body() body: CreateIdiomaDto) {
    console.log('Insert categoria_invetarios');
    return this.categoriaInventarioService.insert(body);
  }
}
