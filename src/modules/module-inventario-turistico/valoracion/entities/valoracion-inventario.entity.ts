import { DataTypes } from 'sequelize';
import {
  Table,
  Column,
  Model,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';

import { InventarioTuristico } from 'src/modules/module-inventario-turistico/inventario-turistico/entities/inventario_turistico.entity';
import { Usuario } from 'src/modules/module-usuario/usuario/entities/usuario.entity';

@Table({ tableName: 'valoracion_inventario' })
export class ValoracionInventario extends Model {
  @Column({ primaryKey: true, autoIncrement: true })
  id_valoracion_inventario: number;

  @Column({ type: DataTypes.FLOAT })
  calificacion: number; //1,2,3,4,5

  @Column
  comentario: string; //max 500

  @Column
  idioma: string; //es,en,fra

  @ForeignKey(() => Usuario)
  id_usuario: number;
  @BelongsTo(() => Usuario)
  usuario: Usuario;

  @ForeignKey(() => InventarioTuristico)
  id_inventario: number;
  @BelongsTo(() => InventarioTuristico)
  invetario: InventarioTuristico;

  @Column({ defaultValue: false })
  deleted: boolean;
}
