import { Module } from '@nestjs/common';
import { ValoracionInventarioController } from './valoracion.controller';
import { ValoracionInventarioService } from './valoracion.service';
import { valoracionInventarioProviders } from './valoracion-inventario.providers';

@Module({
  imports: [],
  controllers: [ValoracionInventarioController],
  providers: [ValoracionInventarioService, ...valoracionInventarioProviders],
  exports: [],
})
export class ValoracionInventarioModule {}
