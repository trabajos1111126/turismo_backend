import { ValoracionInventario } from './entities/valoracion-inventario.entity';

export const valoracionInventarioProviders = [
  {
    provide: 'VALORACION_INVENTARIO_REPOSITORY',
    useValue: ValoracionInventario,
  },
];
