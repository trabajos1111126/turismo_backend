import { IsNotEmpty, IsNumber, Length, Max, Min } from 'class-validator';

export class CreateValoracionInventarioDto {
  @IsNotEmpty()
  @IsNumber()
  @Min(1) // Reemplaza 10 con el valor mínimo del rango
  @Max(5) // Reemplaza 100 con el valor máximo del rango
  calificacion: number;

  @IsNotEmpty()
  @Length(0, 500)
  comentario: string;

  @IsNotEmpty()
  @IsNumber()
  @Min(1)
  id_inventario: number;
}
