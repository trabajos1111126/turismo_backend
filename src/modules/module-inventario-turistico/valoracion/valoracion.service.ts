import {
  Injectable,
  Inject,
  NotFoundException,
  InternalServerErrorException,
} from '@nestjs/common';
import { ValoracionInventario } from './entities/valoracion-inventario.entity';
import { CreateValoracionInventarioDto } from './dto/create-valoracion.dto';
import { Usuario } from 'src/modules/module-usuario/usuario/entities/usuario.entity';

@Injectable()
export class ValoracionInventarioService {
  constructor(
    @Inject('VALORACION_INVENTARIO_REPOSITORY')
    private valoracionInventario: typeof ValoracionInventario,
  ) {}

  async insert(
    id_usuario: number,
    idioma: string,
    valoracion: CreateValoracionInventarioDto,
  ) {
    const usuario = await Usuario.findByPk(id_usuario);
    if (!usuario) {
      throw new NotFoundException('Usuario no registrado');
    }
    const valoracion_nuevo = new ValoracionInventario();
    valoracion_nuevo.calificacion = valoracion.calificacion;
    valoracion_nuevo.comentario = valoracion.comentario;
    valoracion_nuevo.idioma = idioma;
    valoracion_nuevo.id_usuario = id_usuario;
    valoracion_nuevo.id_inventario = valoracion.id_inventario;

    const respuesta = await valoracion_nuevo.save();
    if (!respuesta) {
      throw new NotFoundException('valoracion no registrada');
    }
    return respuesta;
  }

  async delete(id: number) {
    try {
      const valoracion = await this.valoracionInventario.findByPk(id);
      console.log(valoracion);

      if (!valoracion) {
        throw new NotFoundException('Valoración no encontrada');
      }

      // Marcamos la entidad como borrada en la base de datos
      await valoracion.update({ deleted: true });

      return {
        message: 'La valoración se ha marcado como borrada exitosamente',
      };
    } catch (error) {
      console.error('Error al borrar valoración:', error);
      throw new InternalServerErrorException(
        'No se pudo marcar como borrada la valoración',
      );
    }
  }
}
