import {
  Controller,
  Post,
  Request,
  UseGuards,
  Body,
  SetMetadata,
  Delete,
  Param,
} from '@nestjs/common';

import { AuthGuard } from 'src/modules/module-usuario/auth/auth.guard';
import { CreateValoracionInventarioDto } from './dto/create-valoracion.dto';
import { ValoracionInventarioService } from './valoracion.service';
import { RolUsuarioEnum } from 'src/modules/module-usuario/usuario/enum/usuario.enum';

@Controller('api/valoracion_inventario')
export class ValoracionInventarioController {
  constructor(
    private readonly valoracionInventarioService: ValoracionInventarioService,
  ) {}

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR, RolUsuarioEnum.TURISTA])
  @UseGuards(AuthGuard)
  @Post('')
  insert(@Request() req, @Body() body: CreateValoracionInventarioDto) {
    console.log(req.id_usuario);
    console.log('Agregamos valoracion de Inventario');
    return this.valoracionInventarioService.insert(
      req.id_usuario,
      req.idioma,
      body,
    );
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Delete(':id')
  delete(@Param('id') id: number) {
    console.log('Delete valoracion');
    return this.valoracionInventarioService.delete(id);
    //return 'comentario eleiminado' + id;
  }
}
