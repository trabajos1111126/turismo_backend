import { InteresPrincipal } from 'src/modules/module-inventario-turistico/interes-principal/entities/interes_principal';

export const interesPrincipalProviders = [
  {
    provide: 'INTERES_PRINCIPAL_REPOSITORY',
    useValue: InteresPrincipal,
  },
];
