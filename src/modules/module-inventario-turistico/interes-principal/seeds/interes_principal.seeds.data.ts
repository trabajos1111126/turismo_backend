export const interesPrincipalInventarioSeedData = [
  {
    es: 'Apacheta',
    en: 'Cairn',
    fra: 'Apacheta',
  },
  {
    es: 'Arte rupestre',
    en: 'Rock art',
    fra: 'Art rupestre',
  },
  {
    es: 'Arte urbano',
    en: 'Urban art',
    fra: 'Art urbain',
  },
  {
    es: 'Artesanía',
    en: 'Handicrafts',
    fra: 'Artisanat',
  },
  {
    es: 'Avenida',
    en: 'Avenue',
    fra: 'Avenue',
  },
  {
    es: 'Calle colonial',
    en: 'Colonial street',
    fra: 'Rue coloniale',
  },
  {
    es: 'Campo de Golf',
    en: 'Golf course',
    fra: 'Terrain de golf',
  },
  {
    es: 'Cascadas',
    en: 'Waterfalls',
    fra: "Chutes d'eau",
  },
  {
    es: 'Cementerio',
    en: 'Cemetery',
    fra: 'Cimetière',
  },
  {
    es: 'Cueva',
    en: 'Cave',
    fra: 'Grotte',
  },
  {
    es: 'Edificio patrimonial',
    en: 'Heritage building',
    fra: 'Bâtiment patrimonial',
  },
  {
    es: 'Elementos para rituales',
    en: 'Elements for rituals',
    fra: 'Éléments pour rituels',
  },
  {
    es: 'Entrada Folklórica',
    en: 'Folkloric entrance',
    fra: 'Entrée Folklorique',
  },
  {
    es: 'Escalones',
    en: 'Steps',
    fra: 'Marches',
  },
  {
    es: 'Estadio',
    en: 'Stadium',
    fra: 'Stade',
  },
  {
    es: 'Eventos culturales',
    en: 'Cultural events',
    fra: 'Événements culturels',
  },
  {
    es: 'Fauna',
    en: 'Fauna',
    fra: 'Faune',
  },
  {
    es: 'Fauna y flora',
    en: 'Fauna and flora',
    fra: 'Faune et flore',
  },
  {
    es: 'Festividad tradicional',
    en: 'Traditional festivity',
    fra: 'Fête traditionnelle',
  },
  {
    es: 'Flora',
    en: 'Flora',
    fra: 'Flore',
  },
  {
    es: 'Formaciones geológicas',
    en: 'Geological formations',
    fra: 'Formations géologiques',
  },
  {
    es: 'Gastronomía tradicional',
    en: 'Traditional gastronomy',
    fra: 'Gastronomie traditionnelle',
  },
  {
    es: 'Huerto urbano',
    en: 'Urban garden',
    fra: 'Jardin urbain',
  },
  {
    es: 'Laguna',
    en: 'Lagoon',
    fra: 'Lagune',
  },
  {
    es: 'Laguna',
    en: 'Lagoon',
    fra: 'Lagune',
  },
  {
    es: 'Mercado tradicional',
    en: 'Traditional market',
    fra: 'Marché traditionnel',
  },
  {
    es: 'Mirador',
    en: 'Viewpoint',
    fra: 'Belvédère',
  },
  {
    es: 'Monumento',
    en: 'Monument',
    fra: 'Monument',
  },
  {
    es: 'Museo',
    en: 'Museum',
    fra: 'Musée',
  },
  {
    es: 'Nevado',
    en: 'Snowy mountain',
    fra: 'Mont enneigé',
  },
  {
    es: 'Parque recreacional',
    en: 'Recreational park',
    fra: 'Parc récréatif',
  },
  {
    es: 'Plaza',
    en: 'Plaza',
    fra: 'Place',
  },
  {
    es: 'Puente',
    en: 'Bridge',
    fra: 'Pont',
  },
  {
    es: 'Ritual tradicional',
    en: 'Traditional ritual',
    fra: 'Rituel traditionnel',
  },
  {
    es: 'Ruta de escalada en roca',
    en: 'Rock climbing route',
    fra: "Itinéraire d'escalade en roche",
  },
  {
    es: 'Salas de exposición',
    en: 'Exhibition halls',
    fra: "Salles d'exposition",
  },
  {
    es: 'Sendero',
    en: 'Trail',
    fra: 'Sentier',
  },
  {
    es: 'Teatro',
    en: 'Theater',
    fra: 'Théâtre',
  },
  {
    es: 'Teleférico',
    en: 'Cable car',
    fra: 'Téléphérique',
  },
  {
    es: 'Templo',
    en: 'Temple',
    fra: 'Temple',
  },
  {
    es: 'Trajes típicos',
    en: 'Traditional costumes',
    fra: 'Costumes traditionnels',
  },
  {
    es: 'Túneles',
    en: 'Tunnels',
    fra: 'Tunnels',
  },
];
