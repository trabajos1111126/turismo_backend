import { Injectable, Inject, NotFoundException } from '@nestjs/common';
import { CreateIdiomaDto } from 'src/modules/module-library/idioma/dto/create-idioma';
import { Idioma } from 'src/modules/module-library/idioma/entity/idioma.entity';
import { TipoIdioma } from 'src/modules/module-library/idioma/enum/status.enum';
import { InteresPrincipal } from 'src/modules/module-inventario-turistico/interes-principal/entities/interes_principal';
import { UsuarioHeaderDto } from 'src/modules/module-usuario/auth/dto/header-usuario';
import { interesPrincipalInventarioSeedData } from './seeds/interes_principal.seeds.data';

@Injectable()
export class InteresPrincipalService {
  constructor(
    @Inject('INTERES_PRINCIPAL_REPOSITORY')
    private interesPrincipalRepository: typeof InteresPrincipal,
  ) {}

  async seeds() {
    const count = await this.interesPrincipalRepository.count();
    if (count === 0) {
      for (const element of interesPrincipalInventarioSeedData) {
        await this.insert(element);
      }
      console.log(
        'Todas las inserciones  de inventarios turisticos se han completado.',
      );
    }
  }
  async getAll(req: UsuarioHeaderDto) {
    try {
      const interes_principal = await this.interesPrincipalRepository.findAll({
        attributes: ['id_interes_principal'],
        include: [
          {
            model: Idioma,
            attributes: [[req.idioma, 'idioma']],
          },
        ],
      });

      const data = await interes_principal.map((item) => ({
        id_categoria: item.id_interes_principal,
        nombre: item.idioma.dataValues.idioma,
      }));
      return data;
    } catch (error) {
      throw new NotFoundException(error.message);
    }
  }

  async insert(body: CreateIdiomaDto) {
    try {
      const traduccion = new Idioma();
      traduccion.es = body.es;
      traduccion.en = body.en;
      traduccion.fra = body.fra;
      traduccion.tipo = TipoIdioma.INTERES_PRINCIPAL_INVENTARIO;
      const traduccion_guardado = await traduccion.save();
      const interesPrincipal = new InteresPrincipal();
      interesPrincipal.idioma = traduccion_guardado;
      interesPrincipal.id_idioma = traduccion_guardado.id_idioma;

      const interesPrincipal_guardado = await interesPrincipal.save();

      return interesPrincipal_guardado;
    } catch (error) {
      return error;
    }
  }
}
