import {
  Body,
  Controller,
  Get,
  Post,
  Request,
  SetMetadata,
  UseGuards,
} from '@nestjs/common';

import { AuthGuard } from 'src/modules/module-usuario/auth/auth.guard';
import { CreateIdiomaDto } from 'src/modules/module-library/idioma/dto/create-idioma';
import { InteresPrincipalService } from './interes_principal.service';
import { RolUsuarioEnum } from 'src/modules/module-usuario/usuario/enum/usuario.enum';
@Controller('api/interes_principal')
export class InteresPrincipalController {
  constructor(
    private readonly interesPrincipalService: InteresPrincipalService,
  ) {}

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Get('')
  getAll(@Request() req) {
    console.log('getAll interes_principal INVENTARIO');
    return this.interesPrincipalService.getAll(req);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Post()
  insert(@Body() body: CreateIdiomaDto) {
    console.log('insert interes_principal INVENTARIO');
    return this.interesPrincipalService.insert(body);
  }
}
