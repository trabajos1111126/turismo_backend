import {
  Table,
  Column,
  Model,
  BelongsTo,
  ForeignKey,
} from 'sequelize-typescript';
import { Idioma } from '../../../module-library/idioma/entity/idioma.entity';

@Table({ tableName: 'interes_principal' })
export class InteresPrincipal extends Model {
  @Column({ primaryKey: true, autoIncrement: true })
  id_interes_principal: number;

  @BelongsTo(() => Idioma)
  idioma: Idioma;

  @ForeignKey(() => Idioma)
  id_idioma: number;

  @Column({ defaultValue: false })
  deleted: boolean;
}
