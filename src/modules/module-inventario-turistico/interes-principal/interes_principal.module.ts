import { Module } from '@nestjs/common';
import { InteresPrincipalService } from './interes_principal.service';
import { interesPrincipalProviders } from './interes_principal.providers';
import { InteresPrincipalController } from './interes_principal.controller';

@Module({
  imports: [],
  controllers: [InteresPrincipalController],
  providers: [InteresPrincipalService, ...interesPrincipalProviders],
  exports: [],
})
export class InteresPrincipalModule {}
