import { Module } from '@nestjs/common';
import { CategoriaGuiaController } from './categoria_guia.controller';
import { CategoriaGuiaService } from './categoria_guia.service';
import { categoriaGuiaProviders } from './categoria_guia.providers';

@Module({
  imports: [],
  controllers: [CategoriaGuiaController],
  providers: [CategoriaGuiaService, ...categoriaGuiaProviders],
})
export class CategoriaGuiaModule {}
