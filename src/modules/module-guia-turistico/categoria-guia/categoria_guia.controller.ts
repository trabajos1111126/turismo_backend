import {
  Controller,
  Get,
  Post,
  Request,
  UseGuards,
  SetMetadata,
  Body,
} from '@nestjs/common';

import { CreateIdiomaDto } from 'src/modules/module-library/idioma/dto/create-idioma';

import { CategoriaGuiaService } from './categoria_guia.service';
import { AuthGuard } from 'src/modules/module-usuario/auth/auth.guard';
import { RolUsuarioEnum } from 'src/modules/module-usuario/usuario/enum/usuario.enum';
@Controller('api/categoria_guia')
export class CategoriaGuiaController {
  constructor(private readonly categoriaGuiaService: CategoriaGuiaService) {}

  @SetMetadata('roles', [
    RolUsuarioEnum.ADMINISTRADOR,
    RolUsuarioEnum.TURISTA,
    RolUsuarioEnum.ANONIMO,
  ])
  @UseGuards(AuthGuard)
  @Get('')
  getAll(@Request() req) {
    return this.categoriaGuiaService.getAll(req);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Post()
  insert(@Body() body: CreateIdiomaDto) {
    return this.categoriaGuiaService.insert(body);
  }
}
