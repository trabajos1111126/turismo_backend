export const categoriaGuiaSeedData = [
  {
    es: 'Guía Comunitario',
    en: 'Community Guide',
    fra: 'Guide communautaire',
  },
  {
    es: 'Observación de Flora y Fauna',
    en: 'Flora and Fauna Observation',
    fra: 'Flore et de la faune',
  },
  {
    es: 'Culturas',
    en: 'Cultures',
    fra: 'Cultures',
  },
  {
    es: 'Deportes extremos y Aventura',
    en: 'Extreme Sports and Adventure',
    fra: 'Sports extrêmes et aventure',
  },
  {
    es: 'Montaña',
    en: 'Mountain',
    fra: 'montagne',
  },
  {
    es: 'Guía Fijo',
    en: 'Fixed Guide',
    fra: 'Guide fixe',
  },
];

