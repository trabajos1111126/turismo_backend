import { CategoriaGuia } from 'src/modules/module-guia-turistico/categoria-guia/entities/categoria_guia.entity';

export const categoriaGuiaProviders = [
  {
    provide: 'CATEGORIA_GUIA_REPOSITORY',
    useValue: CategoriaGuia,
  },
];
