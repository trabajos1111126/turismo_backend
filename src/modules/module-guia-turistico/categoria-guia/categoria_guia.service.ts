import { Injectable, Inject } from '@nestjs/common';
import { categoriaGuiaSeedData } from 'src/modules/module-guia-turistico/categoria-guia/seeds/categoria_guia.seeds.data';
import { CategoriaGuia } from 'src/modules/module-guia-turistico/categoria-guia/entities/categoria_guia.entity';
import { CreateIdiomaDto } from 'src/modules/module-library/idioma/dto/create-idioma';
import { Idioma } from 'src/modules/module-library/idioma/entity/idioma.entity';
import { TipoIdioma } from 'src/modules/module-library/idioma/enum/status.enum';
import { UsuarioHeaderDto } from 'src/modules/module-usuario/auth/dto/header-usuario';

@Injectable()
export class CategoriaGuiaService {
  constructor(
    @Inject('CATEGORIA_GUIA_REPOSITORY')
    private categoriaGuiaRepository: typeof CategoriaGuia,
  ) {}

  async getAll(req: UsuarioHeaderDto) {
    const categorias = await this.categoriaGuiaRepository.findAll({
      include: [
        {
          model: Idioma,
          attributes: [[req.idioma, 'idioma']],
        },
      ],
    });
    return categorias.map((item) => ({
      id_categoria: item.id_categoria,
      nombre: item.idioma.dataValues.idioma,
    }));
  }

  async insert(body: CreateIdiomaDto) {
    try {
      const traduccion = new Idioma();
      traduccion.es = body.es;
      traduccion.en = body.en;
      traduccion.fra = body.fra;
      traduccion.tipo = TipoIdioma.CATEGORIA_GUIA;
      const traduccion_guardado = await traduccion.save();

      const categoria_guia = new CategoriaGuia();
      categoria_guia.idioma = traduccion_guardado;
      categoria_guia.id_idioma = traduccion_guardado.id_idioma;

      const categoria_guia_guardado = await categoria_guia.save();

      return categoria_guia_guardado;
    } catch (error) {
      return error;
    }
  }
  async seeds() {
    const count = await this.categoriaGuiaRepository.count();
    if (count === 0) {
      for (const element of categoriaGuiaSeedData) {
        await this.insert(element);
      }
      console.log(
        'Todas las inserciones  de categoria de guia se han completado.',
      );
    }
  }
}
