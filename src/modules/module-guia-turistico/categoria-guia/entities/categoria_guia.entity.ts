import {
  Table,
  Column,
  Model,
  BelongsTo,
  ForeignKey,
  HasMany,
} from 'sequelize-typescript';
import { Idioma } from '../../../module-library/idioma/entity/idioma.entity';
import { GuiaTuristico } from '../../guia-turistico/entities/guia_turistico.entity';

@Table({ tableName: 'categoria_guia' })
export class CategoriaGuia extends Model {
  @Column({ primaryKey: true, autoIncrement: true }) //, autoIncrement: true
  id_categoria: number;

  @BelongsTo(() => Idioma)
  idioma: Idioma;

  @ForeignKey(() => Idioma)
  id_idioma: number;

  @HasMany(() => GuiaTuristico)
  guias: GuiaTuristico[];

  @Column({ defaultValue: false })
  deleted: boolean;
}
