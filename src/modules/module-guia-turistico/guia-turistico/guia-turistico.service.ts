import { Injectable, Inject, NotFoundException } from '@nestjs/common';
import { guiasTuristicosSeedData } from 'src/modules/module-guia-turistico/guia-turistico/seeds/guia_turistico.seeds.data';
import { CategoriaGuia } from 'src/modules/module-guia-turistico/categoria-guia/entities/categoria_guia.entity';
import { GuiaTuristico } from 'src/modules/module-guia-turistico/guia-turistico/entities/guia_turistico.entity';
import { Idioma } from 'src/modules/module-library/idioma/entity/idioma.entity';
import { UsuarioHeaderDto } from '../../module-usuario/auth/dto/header-usuario';
import { GetPaginaDto } from './dto/getPaguinationDto';
import { SetPaginaDto } from './dto/setPaguinationDto';
import { GuiaDTO } from './dto/guiaDTO';
import { UpdateGuiaDTO } from './dto/updateGuiaDTO';
@Injectable()
export class GuiaTuristicoService {
  constructor(
    @Inject('GUIA_TURISTICO_REPOSITORY')
    private guiaTuristicoRepository: typeof GuiaTuristico,
  ) {}

  async getAllById(req: UsuarioHeaderDto,id: number) {
    const rows = await this.guiaTuristicoRepository.findAll({
      include: [
        {
          model: CategoriaGuia,
          attributes: ['id_idioma'],
          include: [
            {
              model: Idioma,
              attributes: [[req.idioma, 'idioma']],
            },
          ],
        },
      ],
      where: {
        deleted: false, // Agregar esta condición para excluir registros borrados
        id_categoria: id,
      },
      order: [['nombre', 'ASC']], // Ordenar por nombre en orden ascendente
    });
    if (!rows) {
      console.log('no se encuentra guias que pertenece a esa categoria');
      
    }

    const data = rows.map((item) => ({
      id_guia: item.id_guia,
      nombre: item.nombre,
      direccion: item.direccion,
      contacto: item.contacto,
      email: item.email,
      categoria: item.categoria?.idioma.dataValues.idioma,
    }));
    return data;
  }
  async getAll(req: UsuarioHeaderDto) {
    const rows = await this.guiaTuristicoRepository.findAll({
      include: [
        {
          model: CategoriaGuia,
          attributes: ['id_idioma'],
          include: [
            {
              model: Idioma,
              attributes: [[req.idioma, 'idioma']],
            },
          ],
        },
      ],
      where: {
        deleted: false, // Agregar esta condición para excluir registros borrados
      },
      order: [['nombre', 'ASC']], // Ordenar por nombre en orden ascendente
    });

    const data = rows.map((item) => ({
      id_guia: item.id_guia,
      nombre: item.nombre,
      direccion: item.direccion,
      contacto: item.contacto,
      email: item.email,
      categoria: item.categoria?.idioma.dataValues.idioma,
    }));
    return data;
  }
  async insert(guia: GuiaDTO) {
    try {
      const guia_guardado = new GuiaTuristico();

      guia_guardado.nombre = guia.nombre;
      guia_guardado.direccion = guia.direccion;
      guia_guardado.contacto = guia.contacto;
      guia_guardado.email = guia.email;
      guia_guardado.id_categoria = guia.id_categoria;

      const c = await guia_guardado.save();
      return c;
    } catch (error) {
      throw new NotFoundException(error.message);
    }
  }

  async delete(id: number) {
    const guia = await this.guiaTuristicoRepository.findByPk(id);

    if (!guia) {
      throw new NotFoundException('Guía no encontrada');
    }

    guia.deleted = true; // Marcamos la entidad como borrada
    await guia.save();
    return 'El guía se ha borrado exitosamente';
  }

  async paginate(
    req: UsuarioHeaderDto,
    paguinaDTO: SetPaginaDto,
  ): Promise<GetPaginaDto> {
    try {
      const page = parseInt(paguinaDTO.page, 10);
      const pageSize = parseInt(paguinaDTO.pageSize, 10);
      if (page <= 0) {
        throw new NotFoundException(
          'el numero de paquina tiene que ser mayor o igual a 1',
        );
      }
      if (pageSize <= 5 && pageSize > 100) {
        throw new NotFoundException(
          'el numero de datos por paquina tiene que ser mayor o igual a 5 y menor que 100',
        );
      }
      const { count, rows } =
        await this.guiaTuristicoRepository.findAndCountAll({
          attributes: ['nombre', 'direccion', 'contacto', 'email'],
          include: [
            {
              model: CategoriaGuia,
              attributes: ['id_idioma'],
              include: [
                {
                  model: Idioma,
                  attributes: [[req.idioma, 'idioma']],
                },
              ],
            },
          ],
          limit: pageSize,
          offset: (page - 1) * pageSize,
        });
      const totalPaguinas = Math.ceil(count / pageSize);

      const data = rows.map((item) => ({
        nombre: item.nombre,
        direccion: item.direccion,
        contacto: item.contacto,
        email: item.email,
        categoria: item.categoria.idioma.dataValues.idioma,
      }));
      return {
        data,
        paguinaActual: page,
        totalItems: count,
        totalPaguinas,
      };
    } catch (error) {
      throw new NotFoundException(error.message);
    }
  }

  async update(updateguiaDTO: UpdateGuiaDTO) {
    const guiaExistente = await this.guiaTuristicoRepository.findByPk(
      updateguiaDTO.id_guia,
    );

    if (!guiaExistente) {
      throw new NotFoundException('Guía no encontrada');
    }

    // Actualizar los campos relevantes con los datos del DTO
    guiaExistente.nombre = updateguiaDTO.nombre;
    guiaExistente.direccion = updateguiaDTO.direccion;
    guiaExistente.contacto = updateguiaDTO.contacto;
    guiaExistente.email = updateguiaDTO.email;

    const guia_guardado = await guiaExistente.save();

    return guia_guardado;
  }

  async seeds() {
    const count = await this.guiaTuristicoRepository.count();
    if (count === 0) {
      for (const element of guiasTuristicosSeedData) {
        await this.insert({
          nombre: element.nombre,
          direccion: element.direccion,
          contacto: element.contacto,
          email: element.email,
          id_categoria: element.id_categoria,
        });
      }
    }
  }
}
