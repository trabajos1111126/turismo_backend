import { GuiaTuristico } from 'src/modules/module-guia-turistico/guia-turistico/entities/guia_turistico.entity';

export const guiaTuristicoProviders = [
  {
    provide: 'GUIA_TURISTICO_REPOSITORY',
    useValue: GuiaTuristico,
  },
];
