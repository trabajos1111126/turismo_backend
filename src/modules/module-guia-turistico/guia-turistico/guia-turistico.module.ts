import { Module } from '@nestjs/common';
import { guiaTuristicoController } from './guia-turistico.controller';
import { GuiaTuristicoService } from './guia-turistico.service';
import { guiaTuristicoProviders } from './guia-turistico.providers';
import { CategoriaGuia } from '../categoria-guia/entities/categoria_guia.entity';
import { CategoriaGuiaModule } from '../categoria-guia/categoria_guia.module';

@Module({
  imports: [CategoriaGuiaModule],
  controllers: [guiaTuristicoController],
  providers: [GuiaTuristicoService, ...guiaTuristicoProviders, CategoriaGuia],
})
export class GuiaTuristicoModule {}
