import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Request,
  SetMetadata,
  UseGuards,
} from '@nestjs/common';
import { GuiaTuristicoService } from './guia-turistico.service';

import { SetPaginaDto } from './dto/setPaguinationDto';
import { AuthGuard } from 'src/modules/module-usuario/auth/auth.guard';
import { GuiaDTO } from './dto/guiaDTO';
import { UpdateGuiaDTO } from './dto/updateGuiaDTO';
import { RolUsuarioEnum } from 'src/modules/module-usuario/usuario/enum/usuario.enum';
@Controller('api/guia')
export class guiaTuristicoController {
  constructor(private readonly guiaTuristicoService: GuiaTuristicoService) {}

  @SetMetadata('roles', [
    RolUsuarioEnum.ADMINISTRADOR,
    RolUsuarioEnum.TURISTA,
    RolUsuarioEnum.ANONIMO,
  ])
  @UseGuards(AuthGuard)
  @Get('paguina')
  getPage(@Request() req, @Query() paginaDto: SetPaginaDto) {
    console.log('Paguina guia_turistico');
    return this.guiaTuristicoService.paginate(req, paginaDto);
  }

  @SetMetadata('roles', [
    RolUsuarioEnum.ADMINISTRADOR,
    RolUsuarioEnum.TURISTA,
    RolUsuarioEnum.ANONIMO,
  ])
  @UseGuards(AuthGuard)
  @Get()
  getAll(@Request() req) {
    console.log('GetAll guia_turistico');
    return this.guiaTuristicoService.getAll(req);
  }

  @SetMetadata('roles', [
    RolUsuarioEnum.ADMINISTRADOR,
    RolUsuarioEnum.TURISTA,
    RolUsuarioEnum.ANONIMO,
  ])
  @UseGuards(AuthGuard)
  @Get('categoria/:id')
  getAllById(@Request() req, @Param('id') id: number) {
    console.log('GetAllById guia_turistico');
    return this.guiaTuristicoService.getAllById(req, id);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Post()
  insert(@Body() body: GuiaDTO) {
    console.log('Insert guia_turistico');
    return this.guiaTuristicoService.insert(body);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Patch()
  async update(@Body() updateGuiaDTO: UpdateGuiaDTO) {
    console.log('Update guia_turistico');
    return this.guiaTuristicoService.update(updateGuiaDTO);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Delete(':id')
  delete(@Param('id') id: number) {
    console.log('Delete guia_turistico');
    return this.guiaTuristicoService.delete(id);
  }
}
