import { IsNotEmpty, IsNumberString } from 'class-validator';

export class SetPaginaDto {
  @IsNotEmpty()
  @IsNumberString()
  page: string;

  @IsNotEmpty()
  @IsNumberString()
  pageSize: string;
}
