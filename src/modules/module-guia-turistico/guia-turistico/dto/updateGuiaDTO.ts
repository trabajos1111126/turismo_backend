import { IsNotEmpty, IsEmail, IsInt } from 'class-validator';

export class UpdateGuiaDTO {
  @IsNotEmpty()
  @IsInt()
  id_guia: number;

  @IsNotEmpty()
  nombre: string;

  @IsNotEmpty()
  direccion: string;

  @IsNotEmpty()
  contacto: string;

  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  @IsInt()
  id_categoria: number;
}
