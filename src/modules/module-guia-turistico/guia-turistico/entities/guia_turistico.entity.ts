import {
  Table,
  Column,
  Model,
  DataType,
  BelongsTo,
  ForeignKey,
} from 'sequelize-typescript';
import { CategoriaGuia } from '../../categoria-guia/entities/categoria_guia.entity';

@Table({ tableName: 'guia_turistico' })
export class GuiaTuristico extends Model {
  @Column({ primaryKey: true, autoIncrement: true }) //, autoIncrement: true
  id_guia: number;

  @Column
  nombre: string;

  @Column(DataType.TEXT)
  direccion: string;

  @Column
  contacto: string;

  @Column
  email: string;
  @BelongsTo(() => CategoriaGuia)
  categoria: CategoriaGuia;

  @ForeignKey(() => CategoriaGuia)
  id_categoria: number;

  @Column({ defaultValue: false })
  deleted: boolean;
}
