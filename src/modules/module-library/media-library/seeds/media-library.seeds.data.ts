export const mediaLibrarySeedData = [
  {
    descripcion: 'AJAYU DE LAS MONTAÑAS',
    tipo: 'guia_adjunto',
    url_file_es:
      'https://drive.google.com/file/d/136WuCQ-uB2KgFajd-9K4JbQIWvhU_fyS/view?usp=drive_link',
    url_file_en:
      'https://drive.google.com/file/d/136WuCQ-uB2KgFajd-9K4JbQIWvhU_fyS/view?usp=drive_link',
    url_file_fra:
      'https://drive.google.com/file/d/136WuCQ-uB2KgFajd-9K4JbQIWvhU_fyS/view?usp=drive_link',
    mimetype: 'application/pdf',
    url_foto: 'libro1.png',
    enlace: '',
  },
  {
    descripcion: 'GUIA DE MUSEOS Y ENTRETENIMIENTO',
    tipo: 'guia_adjunto',
    url_file_es:
      'https://drive.google.com/file/d/18ldG6mIWKG-dUPelN1-7lahuamacszxD/view?usp=drive_link',
    url_file_en:
      'https://drive.google.com/file/d/18ldG6mIWKG-dUPelN1-7lahuamacszxD/view?usp=drive_link',
    url_file_fra:
      'https://drive.google.com/file/d/18ldG6mIWKG-dUPelN1-7lahuamacszxD/view?usp=drive_link',
    mimetype: 'application/pdf',
    url_foto: 'libro2.png',
    enlace: '',
  },
  {
    descripcion:
      'DOSSIER ESTADISTICO DE LA OFERTA TURISTICA DEL MUNICIPIO DE LA PAZ',
    tipo: 'guia_adjunto',
    url_file_es:
      'https://drive.google.com/file/d/1-iSbHnMKulP1G-xBVnG4uQWzzAPpKL1P/view?usp=drive_link',
    url_file_en:
      'https://drive.google.com/file/d/1-iSbHnMKulP1G-xBVnG4uQWzzAPpKL1P/view?usp=drive_link',
    url_file_fra:
      'https://drive.google.com/file/d/1-iSbHnMKulP1G-xBVnG4uQWzzAPpKL1P/view?usp=drive_link',
    mimetype: 'application/pdf',
    url_foto: 'libro3.png',
    enlace: '',
  },
  {
    descripcion: 'MAPA TURISTICO HAMPATURI',
    tipo: 'guia_adjunto',
    url_file_es:
      'https://drive.google.com/file/d/1YyH0TcI8LmioJimFRBtlXoemEnuJQO0_/view?usp=drive_link',
    url_file_en:
      'https://drive.google.com/file/d/1YyH0TcI8LmioJimFRBtlXoemEnuJQO0_/view?usp=drive_link',
    url_file_fra:
      'https://drive.google.com/file/d/1YyH0TcI8LmioJimFRBtlXoemEnuJQO0_/view?usp=drive_link',
    mimetype: 'application/pdf',
    url_foto: 'libro4.png',
    enlace: '',
  },
  {
    descripcion: 'GUIA DE MIRADORES',
    tipo: 'guia_adjunto',
    url_file_es:
      'https://drive.google.com/file/d/1etqTnkjlT0JJo7AAbWrr37I3fcRmystq/view?usp=drive_link',
    url_file_en:
      'https://drive.google.com/file/d/1etqTnkjlT0JJo7AAbWrr37I3fcRmystq/view?usp=drive_link',
    url_file_fra:
      'https://drive.google.com/file/d/1etqTnkjlT0JJo7AAbWrr37I3fcRmystq/view?usp=drive_link',
    mimetype: 'application/pdf',
    url_foto: 'libro5.png',
    enlace: '',
  },

  {
    descripcion: 'GUIA DE BOLSILLO PARA AVISTAMIENTO DE AVES',
    tipo: 'guia_adjunto',
    url_file_es:
      'https://drive.google.com/file/d/1Vo5gliOCLtR3xGgWcUoHGOQDOWNAocr4/view?usp=drive_link',
    url_file_en:
      'https://drive.google.com/file/d/1Vo5gliOCLtR3xGgWcUoHGOQDOWNAocr4/view?usp=drive_link',
    url_file_fra:
      'https://drive.google.com/file/d/1Vo5gliOCLtR3xGgWcUoHGOQDOWNAocr4/view?usp=drive_link',
    mimetype: 'application/pdf',
    url_foto: 'libro6.png',
    enlace: '',
  },

  {
    descripcion: '7 LAGUNAS RUTA DE AVITURISMO',
    tipo: 'guia_adjunto',
    url_file_es:
      'https://drive.google.com/file/d/1Aj4yVAUMIAXttXQ_4GPeQP6uo4Ub0DXq/view?usp=drive_link',
    url_file_en:
      'https://drive.google.com/file/d/1Aj4yVAUMIAXttXQ_4GPeQP6uo4Ub0DXq/view?usp=drive_link',
    url_file_fra:
      'https://drive.google.com/file/d/1Aj4yVAUMIAXttXQ_4GPeQP6uo4Ub0DXq/view?usp=drive_link',
    mimetype: 'application/pdf',
    url_foto: 'libro7.png',
    enlace: '',
  },

  {
    descripcion: 'VALLE DE LOS GIGANTES',
    tipo: 'guia_adjunto',
    url_file_es:
      'https://drive.google.com/file/d/1RId3Dwst3Djpw5L1jlePw4LmTjTJ1LEb/view?usp=drivesdk',
    url_file_en:
      'https://drive.google.com/file/d/1RId3Dwst3Djpw5L1jlePw4LmTjTJ1LEb/view?usp=drivesdk',
    url_file_fra:
      'https://drive.google.com/file/d/1RId3Dwst3Djpw5L1jlePw4LmTjTJ1LEb/view?usp=drivesdk',
    mimetype: 'application/pdf',
    url_foto: 'libro8.jpg',
    enlace: '',
  },

  {
    descripcion: 'banner 1 en prueba',
    tipo: 'noticia',
    url_file_es: 'baner1.png',
    url_file_en: 'baner1.png',
    url_file_fra: 'baner1.png',
    mimetype: 'image/png',
    url_foto: '',
    enlace: 'https://m.facebook.com/LaPazCiudadDelCieloOficial',
  },
  {
    descripcion: 'banner 2 en prueba',
    tipo: 'noticia',
    url_file_es: 'baner2.png',
    url_file_en: 'baner2.png',
    url_file_fra: 'baner2.png',
    mimetype: 'image/png',
    url_foto: '',
    enlace: 'https://m.facebook.com/LaPazCiudadDelCieloOficial',
  },
  {
    descripcion: 'banner 3 en prueba',
    tipo: 'noticia',
    url_file_es: 'baner3.png',
    url_file_en: 'baner3.png',
    url_file_fra: 'baner3.png',
    mimetype: 'image/png',
    url_foto: '',
    enlace: 'https://m.facebook.com/LaPazCiudadDelCieloOficial',
  },
];
