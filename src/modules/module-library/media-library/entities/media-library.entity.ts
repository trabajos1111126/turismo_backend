import {
  Table,
  Column,
  Model,
  BelongsTo,
  ForeignKey,
  DataType,
} from 'sequelize-typescript';
import { Adjunto } from '../../adjunto/entities/adjunto.entity';
import { TipoMediaLibraryEnum } from '../enum/media-library.enum';

@Table({ tableName: 'media_library' })
export class MediaLibrary extends Model {
  @Column({ primaryKey: true, autoIncrement: true }) //
  id_media_library: number;

  @BelongsTo(() => Adjunto)
  adjunto: Adjunto;

  @ForeignKey(() => Adjunto)
  id_adjunto: number;

  @Column({ defaultValue: '' })
  descripcion: string;

  @Column({ defaultValue: '' })
  enlace: string;

  @Column(DataType.ENUM(...Object.values(TipoMediaLibraryEnum)))
  tipo: string;

  @Column({ defaultValue: false })
  deleted: boolean;
}
