import { Module } from '@nestjs/common';
import { MediaLibraryController } from './media-library.controller';
import { MediaLibraryService } from './media-library.service';
import { mediaLibraryProviders } from './media-library.providers';

@Module({
  imports: [],
  controllers: [MediaLibraryController],
  providers: [MediaLibraryService, ...mediaLibraryProviders],
  exports: [],
})
export class MediaLibraryModule {}
