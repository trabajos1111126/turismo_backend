import {
  Injectable,
  Inject,
  NotFoundException,
  Body,
  UploadedFile,
} from '@nestjs/common';

import { UsuarioHeaderDto } from 'src/modules/module-usuario/auth/dto/header-usuario';
import { Adjunto } from 'src/modules/module-library/adjunto/entities/adjunto.entity';
import { MediaLibrary } from './entities/media-library.entity';
import { CreateMediaLibraryDto } from './dto/create-media-library.dto';
import { CreateMediaLibrarySeedDto } from './dto/create-media-library.seed.dto';
import { mediaLibrarySeedData } from './seeds/media-library.seeds.data';
import { UpdateNoticiaDto } from './dto/update-noticia.dto';
import { CreateGuiaDto } from './dto/create-guia-pdf';
import { UpdateGuiaDto } from './dto/update-guia.dto';
@Injectable()
export class MediaLibraryService {
  constructor(
    @Inject('MEDIA_LIBRARY_REPOSITORY')
    private mediaLibraryRepository: typeof MediaLibrary,
  ) {}
  async getAllByTipo(req: UsuarioHeaderDto, tipo: string) {
    try {
      const nombre_archivo = 'url_file_' + req.idioma;
      const list = await this.mediaLibraryRepository.findAll({
        attributes: ['id_media_library', 'descripcion', 'enlace', 'tipo'],
        where: {
          tipo: tipo,
          deleted: false,
        },
        include: [
          {
            model: Adjunto,
            attributes: [[nombre_archivo, 'url_file'], 'url_foto'],
          },
        ],
      });

      //console.log(list);

      const data = await list.map((item) => ({
        id_media_library: item.id_media_library,
        descripcion: item.descripcion,
        enlace: item.enlace,
        url_file: item.adjunto.dataValues.url_file,
        url_foto: item.adjunto.dataValues.url_foto,
      }));
      return data;
    } catch (error) {
      throw new NotFoundException(error.message);
    }
  }
  async insertNoticia(@Body() body: CreateMediaLibraryDto) {
    try {
      const mediaLibrary = new MediaLibrary();
      mediaLibrary.id_adjunto = 1;
      mediaLibrary.descripcion = body.descripcion;
      mediaLibrary.enlace = body.enlace;
      mediaLibrary.tipo = 'noticia';

      const mediaLibrary_guardado = await mediaLibrary.save();

      return mediaLibrary_guardado;
    } catch (error) {
      throw new NotFoundException(error.message);
    }
  }

  async insertGuia(@Body() body: CreateGuiaDto) {
    console.log(body.descripcion);
    try {
      const adjunto = new Adjunto();
      adjunto.url_file_es = body.url_file_es;
      adjunto.url_file_en = body.url_file_en;
      adjunto.url_file_fra = body.url_file_fra;
      adjunto.descripcion = 'Enlace de guia turistico';
      const adjunto_guardado = await adjunto.save();

      const mediaLibrary = new MediaLibrary();
      mediaLibrary.id_adjunto = adjunto_guardado.id_adjunto;
      mediaLibrary.descripcion = body.descripcion;
      mediaLibrary.tipo = 'guia_adjunto';
      const mediaLibrary_guardado = await mediaLibrary.save();

      return mediaLibrary_guardado;
    } catch (error) {
      throw new NotFoundException(error.message);
    }
  }
  async updateNoticia(updateNoticiaDto: UpdateNoticiaDto) {
    const noticiaExistente = await this.mediaLibraryRepository.findByPk(
      updateNoticiaDto.id_media_library,
    );

    if (!noticiaExistente) {
      throw new NotFoundException('Guía no encontrada');
    }

    // Actualizar los campos relevantes con los datos del DTO
    noticiaExistente.descripcion = updateNoticiaDto.descripcion;
    noticiaExistente.enlace = updateNoticiaDto.enlace;

    const noticia_guardado = await noticiaExistente.save();

    return noticia_guardado;
  }

  async updateGuia(updateGuiaDto: UpdateGuiaDto) {
    const noticiaExistente = await this.mediaLibraryRepository.findByPk(
      updateGuiaDto.id_media_library,
    );

    if (!noticiaExistente) {
      throw new NotFoundException('Guía no encontrada');
    }

    // Actualizar los campos relevantes con los datos del DTO
    noticiaExistente.descripcion = updateGuiaDto.descripcion;
    //noticiaExistente.enlace = updateNoticiaDto.enlace;

    const adjuntoExistente = await Adjunto.findByPk(
      noticiaExistente.id_adjunto,
    );

    if (!adjuntoExistente) {
      throw new NotFoundException('Adjunto no encontrado');
    }

    adjuntoExistente.url_file_es = updateGuiaDto.url_file_es;
    adjuntoExistente.url_file_en = updateGuiaDto.url_file_en;
    adjuntoExistente.url_file_fra = updateGuiaDto.url_file_fra;
    await adjuntoExistente.save();

    const noticia_guardado = await noticiaExistente.save();

    return noticia_guardado;
  }
  async insertSeeds(@Body() body: CreateMediaLibrarySeedDto) {
    try {
      const adjunto = new Adjunto();
      let ruta_file = '';
      if (body.tipo === 'noticia') {
        adjunto.descripcion = 'baner de noticia';
        ruta_file = 'images/media_library/';
      }

      adjunto.url_file_es = ruta_file + body.url_file_es;
      adjunto.url_file_en = ruta_file + body.url_file_en;
      adjunto.url_file_fra = ruta_file + body.url_file_fra;
      if (body.url_foto !== '') {
        adjunto.url_foto = 'images/media_library/' + body.url_foto;
      }

      adjunto.mimetype = body.mimetype;

      const adjunto_guardado = await adjunto.save();
      const mediaLibrary = new MediaLibrary();
      mediaLibrary.id_adjunto = adjunto_guardado.id_adjunto;
      mediaLibrary.descripcion = body.descripcion;
      mediaLibrary.tipo = body.tipo;
      mediaLibrary.enlace = body.enlace;

      const mediaLibrary_guardado = await mediaLibrary.save();

      return mediaLibrary_guardado;
    } catch (error) {
      throw new NotFoundException(error.message);
    }
  }
  async agregarFoto(id: number, @UploadedFile() file: Express.Multer.File) {
    try {
      const mediaLibrary = await this.mediaLibraryRepository.findByPk(id);
      const adjunto = new Adjunto();
      adjunto.url_foto = 'images/media_library/' + file.filename;
      adjunto.descripcion = 'imagen de noticia';
      adjunto.mimetype = file.mimetype;
      const adjunto_guardado = await adjunto.save();
      mediaLibrary.id_adjunto = adjunto_guardado.id_adjunto;
      mediaLibrary.adjunto = adjunto_guardado;
      const mediaLibrary_guardado = mediaLibrary.save();
      return mediaLibrary_guardado;
    } catch (error) {
      throw new NotFoundException(error.message);
    }
  }
  async agregarBannerES(id: number, @UploadedFile() file: Express.Multer.File) {
    try {
      const mediaLibrary = await this.mediaLibraryRepository.findByPk(id);
      if (!mediaLibrary) {
        throw new NotFoundException('Noticia o Guia no encontrada');
      }
      if (mediaLibrary.id_adjunto == 1) {
        //adjunto por defecto
        const adjunto = new Adjunto();
        adjunto.url_file_es = 'images/media_library/' + file.filename;
        adjunto.mimetype = file.mimetype;
        const adjunto_guardado = await adjunto.save();
        mediaLibrary.id_adjunto = adjunto_guardado.id_adjunto;
        await mediaLibrary.save();
      } else {
        //adjunto propio ya especificasdo
        const adjunto = await Adjunto.findByPk(mediaLibrary.id_adjunto);
        adjunto.url_file_es = 'images/media_library/' + file.filename;
        adjunto.descripcion = 'banner de noticia';
        adjunto.mimetype = file.mimetype;
        await adjunto.save();
      }
      return 'Banner noticia en español  ya guardado';
    } catch (error) {
      throw new NotFoundException(error.message);
    }
  }
  async agregarBannerEN(id: number, @UploadedFile() file: Express.Multer.File) {
    try {
      const mediaLibrary = await this.mediaLibraryRepository.findByPk(id);
      if (!mediaLibrary) {
        throw new NotFoundException('Noticia o Guia no encontrada');
      }
      if (mediaLibrary.id_adjunto == 1) {
        //adjunto por defecto
        const adjunto = new Adjunto();
        adjunto.url_file_en = 'images/media_library/' + file.filename;
        adjunto.mimetype = file.mimetype;
        const adjunto_guardado = await adjunto.save();
        mediaLibrary.id_adjunto = adjunto_guardado.id_adjunto;
        await mediaLibrary.save();
      } else {
        //adjunto propio ya especificasdo
        const adjunto = await Adjunto.findByPk(mediaLibrary.id_adjunto);
        adjunto.url_file_en = 'images/media_library/' + file.filename;
        adjunto.descripcion = 'banner de noticia';
        adjunto.mimetype = file.mimetype;
        await adjunto.save();
      }
      return 'Banner noticia en ingles  ya guardado';
    } catch (error) {
      throw new NotFoundException(error.message);
    }
  }
  async agregarBannerFRA(
    id: number,
    @UploadedFile() file: Express.Multer.File,
  ) {
    try {
      const mediaLibrary = await this.mediaLibraryRepository.findByPk(id);
      if (!mediaLibrary) {
        throw new NotFoundException('Noticia o Guia no encontrada');
      }
      if (mediaLibrary.id_adjunto == 1) {
        //adjunto por defecto
        const adjunto = new Adjunto();
        adjunto.url_file_fra = 'images/media_library/' + file.filename;
        adjunto.mimetype = file.mimetype;
        const adjunto_guardado = await adjunto.save();
        mediaLibrary.id_adjunto = adjunto_guardado.id_adjunto;
        await mediaLibrary.save();
      } else {
        //adjunto propio ya especificasdo
        const adjunto = await Adjunto.findByPk(mediaLibrary.id_adjunto);
        adjunto.url_file_fra = 'images/media_library/' + file.filename;
        adjunto.descripcion = 'banner de noticia';
        adjunto.mimetype = file.mimetype;
        await adjunto.save();
      }
      return 'Banner noticia en frances  ya guardado';
    } catch (error) {
      throw new NotFoundException(error.message);
    }
  }

  async seeds() {
    const count = await this.mediaLibraryRepository.count();
    if (count === 0) {

      for (const element of mediaLibrarySeedData) {
        await this.insertSeeds(element);
      }
      console.log(
        'Todas las inserciones  de noticias y guias en pdf se han completado.',
      );
    }
  }

  async delete(id: number) {
    const mediaLibrary = await this.mediaLibraryRepository.findByPk(id);

    if (!mediaLibrary) {
      throw new NotFoundException('Noticia o Guia no encontrada');
    }

    mediaLibrary.deleted = true; // Marcamos la entidad como borrada
    await mediaLibrary.save();
    return 'La Noticia/Guia se ha borrado exitosamente';
  }
}
