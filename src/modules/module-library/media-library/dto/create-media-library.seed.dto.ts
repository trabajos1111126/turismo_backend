export class CreateMediaLibrarySeedDto {
  descripcion: string;
  tipo: string;
  url_file_es: string;
  url_file_en: string;
  url_file_fra: string;
  mimetype: string;
  url_foto: string;
  enlace: string;
}
