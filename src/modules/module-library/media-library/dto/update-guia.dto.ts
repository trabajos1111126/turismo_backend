export class UpdateGuiaDto {
  id_media_library: number;
  descripcion: string;
  url_file_es: string;
  url_file_en: string;
  url_file_fra: string;
}
