import {
  Controller,
  Get,
  Post,
  Request,
  UseGuards,
  UploadedFile,
  UseInterceptors,
  Body,
  Param,
  Patch,
  Delete,
  SetMetadata,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';

import { AuthGuard } from 'src/modules/module-usuario/auth/auth.guard';

import { MediaLibraryService } from './media-library.service';
import { multerConfigMediaLibrary } from 'src/config/multer.config';
import { CreateGuiaDto } from './dto/create-guia-pdf';
import { CreateNoticiaDto } from './dto/create-noticia.dto';
import { UpdateNoticiaDto } from './dto/update-noticia.dto';
import { UpdateGuiaDto } from './dto/update-guia.dto';
import { RolUsuarioEnum } from 'src/modules/module-usuario/usuario/enum/usuario.enum';

@Controller('api/media_library')
export class MediaLibraryController {
  constructor(private readonly mediaLibraryService: MediaLibraryService) {}

  /******************************************* */
  //              NOTICIAS
  /******************************************* */
  @SetMetadata('roles', [
    RolUsuarioEnum.ADMINISTRADOR,
    RolUsuarioEnum.TURISTA,
    RolUsuarioEnum.ANONIMO,
  ])
  @UseGuards(AuthGuard)
  @Get('noticia')
  getAllNoticias(@Request() req) {
    console.log('getAllNoticias Media-Library');
    return this.mediaLibraryService.getAllByTipo(req, 'noticia');
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Post('noticia')
  insertNoticia(@Body() body: CreateNoticiaDto) {
    console.log('insert media-library noticia');
    return this.mediaLibraryService.insertNoticia(body);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Patch('noticia')
  async updateNoticia(@Body() updateNoticiaDto: UpdateNoticiaDto) {
    console.log('Update noticia');
    return this.mediaLibraryService.updateNoticia(updateNoticiaDto);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Post('noticia/:id/bannerES')
  @UseInterceptors(FileInterceptor('file', multerConfigMediaLibrary))
  InsertBanerES(
    @Param('id') id: number,
    @UploadedFile() file: Express.Multer.File,
  ) {
    console.log('Insert Banner en ESPAÑOL de noticia media-library');
    return this.mediaLibraryService.agregarBannerES(id, file);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Post('noticia/:id/bannerEN')
  @UseInterceptors(FileInterceptor('file', multerConfigMediaLibrary))
  InsertBanerEN(
    @Param('id') id: number,
    @UploadedFile() file: Express.Multer.File,
  ) {
    console.log('Insert Banner en INGLES de noticia media-library');
    return this.mediaLibraryService.agregarBannerEN(id, file);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Post('noticia/:id/bannerFRA')
  @UseInterceptors(FileInterceptor('file', multerConfigMediaLibrary))
  InsertBanerFRA(
    @Param('id') id: number,
    @UploadedFile() file: Express.Multer.File,
  ) {
    console.log('Insert Banner en FRANCES noticia media-library');
    return this.mediaLibraryService.agregarBannerFRA(id, file);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Delete('noticia/:id')
  deleteNoticia(@Param('id') id: number) {
    console.log('Delete media-library');
    return this.mediaLibraryService.delete(id);
  }

  /******************************************* */
  //              GUIAS
  /******************************************* */

  @SetMetadata('roles', [
    RolUsuarioEnum.ADMINISTRADOR,
    RolUsuarioEnum.TURISTA,
    RolUsuarioEnum.ANONIMO,
  ])
  @UseGuards(AuthGuard)
  @Get('guia')
  getAllGuias(@Request() req) {
    console.log('getAllGuias  Media-Library');
    return this.mediaLibraryService.getAllByTipo(req, 'guia_adjunto');
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Post('guia')
  insertGuia(@Body() body: CreateGuiaDto) {
    console.log(body);
    console.log('insertGuia Media-library');
    return this.mediaLibraryService.insertGuia(body);
    //return this.noticiaService.getAllByTipo(req, 'guia_adjunto');
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Patch('guia')
  async updateGuia(@Body() updateGuiaDto: UpdateGuiaDto) {
    console.log('Update guia media-library');
    return this.mediaLibraryService.updateGuia(updateGuiaDto);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Delete('guia/:id')
  deleteGuia(@Param('id') id: number) {
    console.log('Delete guia media-library');
    return this.mediaLibraryService.delete(id);
  }

  @SetMetadata('roles', [RolUsuarioEnum.ADMINISTRADOR])
  @UseGuards(AuthGuard)
  @Patch('guia/:id/portada')
  @UseInterceptors(FileInterceptor('file', multerConfigMediaLibrary))
  insertFile1(
    @Param('id') id: number,
    @UploadedFile() file: Express.Multer.File,
  ) {
    console.log('Agregamos file ');
    return this.mediaLibraryService.agregarFoto(id, file);
  }
  /******************************************* */
  //              AGREGAR IMAGENES
  /******************************************* */
}
