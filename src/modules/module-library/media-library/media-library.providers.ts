import { MediaLibrary } from './entities/media-library.entity';

export const mediaLibraryProviders = [
  {
    provide: 'MEDIA_LIBRARY_REPOSITORY',
    useValue: MediaLibrary,
  },
];
