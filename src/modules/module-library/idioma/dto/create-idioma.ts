export class CreateIdiomaDto {
  es: string;
  en: string;
  fra: string;
}
