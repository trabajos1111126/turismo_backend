import { Table, Column, Model, HasOne, DataType } from 'sequelize-typescript';
import { CategoriaGuia } from '../../../module-guia-turistico/categoria-guia/entities/categoria_guia.entity';
import { CategoriaInventario } from '../../../module-inventario-turistico/categoria-inventario/entities/categoria_inventario.entity';
import { SubTipoInventario } from '../../../module-inventario-turistico/subtipo-inventario/entities/subtipo_inventario.entity';
import { TipoInventario } from '../../../module-inventario-turistico/tipo-inventario/entities/tipo_inventario.entity';
import { Vocacion } from '../../../module-inventario-turistico/vocacion/entities/vocacion.entity';
import { Modalidad } from '../../../module-inventario-turistico/modalidad/entities/modalidad.entity';
import { InteresPrincipal } from '../../../module-inventario-turistico/interes-principal/entities/interes_principal';
import { ActividadPrincipal } from '../../../module-inventario-turistico/actividad-principal/entities/actividad_principal';
import { TipoIdioma } from '../enum/status.enum';
import { TipoEmpresa } from 'src/modules/module-empresa/tipo-empresa/entities/tipo_de_empresa.entity';
import { EspecialidadEmpresa } from 'src/modules/module-empresa/especialidad-empresa/entities/especialidad_de_empresa.entity';
import { SubtipoEmpresa } from 'src/modules/module-empresa/subtipo-empresa/entities/subtipo_de_empresa.entity';
import { InventarioTuristico } from 'src/modules/module-inventario-turistico/inventario-turistico/entities/inventario_turistico.entity';
import { Ruta } from 'src/modules/module-rutas-turisticas/rutas/entities/ruta.entity';
import { Empresa } from 'src/modules/module-empresa/empresa/entities/empresa.entity';

@Table({ tableName: 'idioma' })
export class Idioma extends Model {
  @Column({ primaryKey: true, autoIncrement: true })
  id_idioma: number;

  @Column({
    type: DataType.STRING(1000), // Establece la longitud máxima a 1000 caracteres
    allowNull: false,
  })
  es: string;

  @Column({
    type: DataType.STRING(1000), // Establece la longitud máxima a 1000 caracteres
    allowNull: false,
  })
  en: string;

  @Column({
    type: DataType.STRING(1000), // Establece la longitud máxima a 1000 caracteres
    allowNull: false,
  })
  fra: string;

  @Column(DataType.ENUM(...Object.values(TipoIdioma)))
  tipo: TipoIdioma;

  @HasOne(() => CategoriaGuia)
  categoriaGuia: CategoriaGuia;

  @HasOne(() => CategoriaInventario)
  categoriaInventario: CategoriaInventario;

  @HasOne(() => TipoInventario)
  tipoInventario: TipoInventario;

  @HasOne(() => SubTipoInventario)
  subtipoInventario: SubTipoInventario;

  @HasOne(() => Vocacion)
  vocacion: Vocacion;

  @HasOne(() => Ruta)
  ruta: Ruta;

  @HasOne(() => Empresa)
  empresa: Empresa;

  @HasOne(() => Modalidad)
  modalidad: Modalidad;

  @HasOne(() => InteresPrincipal)
  interesPrincipal: InteresPrincipal;

  @HasOne(() => ActividadPrincipal)
  actividadPrincipal: ActividadPrincipal;

  @HasOne(() => TipoEmpresa)
  tipoEmpresa: TipoEmpresa;

  @HasOne(() => SubtipoEmpresa)
  subtipoEmpresa: SubtipoEmpresa;

  @HasOne(() => EspecialidadEmpresa)
  especialidadEmpresa: EspecialidadEmpresa;

  //descripcion inventario
  @HasOne(() => InventarioTuristico)
  inventario: InventarioTuristico;

  @Column({ defaultValue: false })
  deleted: boolean;
}
