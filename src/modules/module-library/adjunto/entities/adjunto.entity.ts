import {
  Table,
  Column,
  Model,
  HasOne,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';
import { Vocacion } from '../../../module-inventario-turistico/vocacion/entities/vocacion.entity';
import { MediaLibrary } from '../../media-library/entities/media-library.entity';
import { InventarioTuristico } from 'src/modules/module-inventario-turistico/inventario-turistico/entities/inventario_turistico.entity';
import { Empresa } from 'src/modules/module-empresa/empresa/entities/empresa.entity';
import { TipoEmpresa } from 'src/modules/module-empresa/tipo-empresa/entities/tipo_de_empresa.entity';

@Table({ tableName: 'adjunto' })
export class Adjunto extends Model {
  @Column({ primaryKey: true, autoIncrement: true }) // autoIncrement: true
  id_adjunto: number;

  @Column({ defaultValue: '' })
  url_foto: string;

  @Column({ defaultValue: '' })
  url_file_es: string;

  @Column({ defaultValue: '' })
  url_file_en: string;

  @Column({ defaultValue: '' })
  url_file_fra: string;
  @Column
  mimetype: string;

  @Column
  descripcion: string;

  @HasOne(() => Vocacion)
  vocacion: Vocacion;

  @HasOne(() => TipoEmpresa)
  tipoEmpresa: TipoEmpresa;

  //@HasOne(() => Empresa)
  //empresa: Empresa;

  @HasOne(() => MediaLibrary)
  mediaLibrary: MediaLibrary;

  @ForeignKey(() => InventarioTuristico)
  @Column
  id_inventario: number;

  @BelongsTo(() => InventarioTuristico)
  inventario: InventarioTuristico;

  @Column({ defaultValue: false })
  deleted: boolean;
}
