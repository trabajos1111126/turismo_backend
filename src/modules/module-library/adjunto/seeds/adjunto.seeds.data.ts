export const adjuntoSeedData = [
  {
    url_foto: 'images/default.png',
    mimetype: 'image/png',
    descripcion: 'foto por defecto',
  },
  {
    url_foto: 'images/empresa_default.jpg',
    mimetype: 'image/jpg',
    descripcion: 'logo de empresa por defecto',
  },
];
