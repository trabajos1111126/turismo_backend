import { Adjunto } from './entities/adjunto.entity';

export const adjuntoProviders = [
  {
    provide: 'ADJUNTO_REPOSITORY',
    useValue: Adjunto,
  },
];
