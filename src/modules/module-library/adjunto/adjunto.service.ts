import { Injectable, Inject } from '@nestjs/common';
import { Adjunto } from './entities/adjunto.entity';
import { adjuntoSeedData } from 'src/modules/module-library/adjunto/seeds/adjunto.seeds.data';

@Injectable()
export class AdjuntoService {
  constructor(
    @Inject('ADJUNTO_REPOSITORY')
    private adjuntoRepository: typeof Adjunto,
  ) {}
  async seeds() {
    const count = await this.adjuntoRepository.count();
    if (count === 0) {
      await this.adjuntoRepository.bulkCreate(adjuntoSeedData);
    }
  }
}
