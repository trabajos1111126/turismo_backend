import { FotoEmpresa } from './entities/foto_empresa.entity';

export const adjuntoProviders = [
  {
    provide: 'FOTO__EMPRESA_REPOSITORY',
    useValue: FotoEmpresa,
  },
];
