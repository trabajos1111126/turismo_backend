import {
  Table,
  Column,
  Model,
  HasOne,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';
import { Vocacion } from '../../../module-inventario-turistico/vocacion/entities/vocacion.entity';
import { MediaLibrary } from '../../media-library/entities/media-library.entity';
import { InventarioTuristico } from 'src/modules/module-inventario-turistico/inventario-turistico/entities/inventario_turistico.entity';
import { Empresa } from 'src/modules/module-empresa/empresa/entities/empresa.entity';
import { TipoEmpresa } from 'src/modules/module-empresa/tipo-empresa/entities/tipo_de_empresa.entity';

@Table({ tableName: 'foto_empresa' })
export class FotoEmpresa extends Model {
  @Column({ primaryKey: true, autoIncrement: true }) // autoIncrement: true
  id_foto_empresa: number;

  @Column({ defaultValue: '' })
  url_foto: string;

  @Column
  mimetype: string;
  @ForeignKey(() => Empresa)
  @Column
  id_empresa: number;

  @BelongsTo(() => Empresa)
  empresa: Empresa;

  @Column({ defaultValue: false })
  deleted: boolean;
}
