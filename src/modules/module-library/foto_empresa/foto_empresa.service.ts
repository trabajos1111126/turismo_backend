import { Injectable, Inject } from '@nestjs/common';
import { adjuntoSeedData } from 'src/modules/module-library/adjunto/seeds/adjunto.seeds.data';
import { FotoEmpresa } from './entities/foto_empresa.entity';

@Injectable()
export class AdjuntoService {
  constructor(
    @Inject('ADJUNTO_REPOSITORY')
    private fotoEmpresaRepository: typeof FotoEmpresa,
  ) {}
}
