import { Sequelize } from 'sequelize-typescript';

import databaseConfig from 'src/config/database.config';

// Entity
import { Idioma } from 'src/modules/module-library/idioma/entity/idioma.entity';
import { GuiaTuristico } from 'src/modules/module-guia-turistico/guia-turistico/entities/guia_turistico.entity';
import { Vocacion } from 'src/modules/module-inventario-turistico/vocacion/entities/vocacion.entity';
import { CategoriaInventario } from 'src/modules/module-inventario-turistico/categoria-inventario/entities/categoria_inventario.entity';
import { TipoInventario } from 'src/modules/module-inventario-turistico/tipo-inventario/entities/tipo_inventario.entity';
import { SubTipoInventario } from 'src/modules/module-inventario-turistico/subtipo-inventario/entities/subtipo_inventario.entity';
import { CategoriaGuia } from 'src/modules/module-guia-turistico/categoria-guia/entities/categoria_guia.entity';
import { InventarioTuristico } from 'src/modules/module-inventario-turistico/inventario-turistico/entities/inventario_turistico.entity';
import { Modalidad } from 'src/modules/module-inventario-turistico/modalidad/entities/modalidad.entity';
import { InteresPrincipal } from 'src/modules/module-inventario-turistico/interes-principal/entities/interes_principal';
import { ActividadPrincipal } from 'src/modules/module-inventario-turistico/actividad-principal/entities/actividad_principal';

//Service

import { TipoInventarioService } from 'src/modules/module-inventario-turistico/tipo-inventario/tipo_inventario.service';
import { SubtipoInventarioService } from 'src/modules/module-inventario-turistico/subtipo-inventario/subtipo_inventario.service';
import { Usuario } from 'src/modules/module-usuario/usuario/entities/usuario.entity';
import { VocacionService } from 'src/modules/module-inventario-turistico/vocacion/vocacion.service';
import { ModalidadService } from 'src/modules/module-inventario-turistico/modalidad/modalidad.service';
import { InteresPrincipalService } from 'src/modules/module-inventario-turistico/interes-principal/interes_principal.service';
import { InventarioTuristicoService } from 'src/modules/module-inventario-turistico/inventario-turistico/inventario_turistico.service';

import { CategoriaInventarioService } from 'src/modules/module-inventario-turistico/categoria-inventario/categoria_inventario.service';
import { ActividadPrincipalService } from 'src/modules/module-inventario-turistico/actividad-principal/actividad.principal.service';
import { CategoriaGuiaService } from 'src/modules/module-guia-turistico/categoria-guia/categoria_guia.service';
import { GuiaTuristicoService } from 'src/modules/module-guia-turistico/guia-turistico/guia-turistico.service';

import { Adjunto } from 'src/modules/module-library/adjunto/entities/adjunto.entity';
import { AdjuntoService } from 'src/modules/module-library/adjunto/adjunto.service';
import { MediaLibrary } from 'src/modules/module-library/media-library/entities/media-library.entity';
import { ValoracionInventario } from 'src/modules/module-inventario-turistico/valoracion/entities/valoracion-inventario.entity';
import { TipoEmpresa } from 'src/modules/module-empresa/tipo-empresa/entities/tipo_de_empresa.entity';
import { SubtipoEmpresa } from 'src/modules/module-empresa/subtipo-empresa/entities/subtipo_de_empresa.entity';
import { EspecialidadEmpresa } from 'src/modules/module-empresa/especialidad-empresa/entities/especialidad_de_empresa.entity';
import { Empresa } from 'src/modules/module-empresa/empresa/entities/empresa.entity';
import { Sucursal } from 'src/modules/module-empresa/sucursal/entities/sucursal.entity';
import { TipoEmpresaService } from 'src/modules/module-empresa/tipo-empresa/tipo_empresa.service';
import { SubtipoEmpresaService } from 'src/modules/module-empresa/subtipo-empresa/subtipo_empresa.service';
import { EspecialidadDeEmpresaService } from 'src/modules/module-empresa/especialidad-empresa/especialidad_empresa.service';
import { EmpresaService } from 'src/modules/module-empresa/empresa/empresa.service';
import { ValoracionEmpresa } from 'src/modules/module-empresa/valoracion/entities/valoracion-empresa.entity';
import { MediaLibraryService } from 'src/modules/module-library/media-library/media-library.service';

import { UsuarioService } from 'src/modules/module-usuario/usuario/usuario.service';
import { FotoEmpresa } from 'src/modules/module-library/foto_empresa/entities/foto_empresa.entity';
import { Ruta } from 'src/modules/module-rutas-turisticas/rutas/entities/ruta.entity';
import { RutaSucursal } from 'src/modules/module-rutas-turisticas/rutas/entities/ruta-sucursal.entity';
import { Log } from 'src/modules/module-usuario/log/entities/log.entity';

export const databaseProviders = [
  {
    provide: 'SEQUELIZE',
    useFactory: async () => {
      const sequelize = new Sequelize(databaseConfig);

      sequelize.addModels([
        Idioma,
        MediaLibrary,
        GuiaTuristico,
        CategoriaGuia,
        Vocacion,
        Adjunto,
        CategoriaInventario,
        TipoInventario,
        SubTipoInventario,
        InventarioTuristico,
        Modalidad,
        InteresPrincipal,
        ActividadPrincipal,
        //usuario
        Usuario,
        ValoracionInventario,
        //Empresa
        TipoEmpresa,
        SubtipoEmpresa,
        EspecialidadEmpresa,
        Empresa,
        Sucursal,
        ValoracionEmpresa,
        FotoEmpresa,
        // Agregamos lo que sea necesario para las rutas
        Ruta,
        RutaSucursal,
        //Agregamso el lo9g
        Log,
      ]);
      //cargamos la imagen  por defecto
      await sequelize.sync();

      const adjuntoService = new AdjuntoService(Adjunto);
      await adjuntoService.seeds();

      //cargamso las noticias y guias en pdf
      const media_library_service = new MediaLibraryService(MediaLibrary);
      await media_library_service.seeds();

      //cargamos todo lo util para el inventario turistico
      const actividad_principal_service = new ActividadPrincipalService(
        ActividadPrincipal,
      );
      await actividad_principal_service.seeds();

      const categoria_service = new CategoriaInventarioService(
        CategoriaInventario,
      );
      await categoria_service.seeds();
      const interes_principal_service = new InteresPrincipalService(
        InteresPrincipal,
      );
      await interes_principal_service.seeds();

      const modalidad_service = new ModalidadService(Modalidad);
      await modalidad_service.seeds();

      const subtipo_service = new SubtipoInventarioService(SubTipoInventario);
      await subtipo_service.seeds();

      const tipo_service = new TipoInventarioService(TipoInventario);
      await tipo_service.seeds();

      const vocacion_service = new VocacionService(Vocacion);
      await vocacion_service.seeds();

      const inventario_service = new InventarioTuristicoService(
        InventarioTuristico,
      );
      await inventario_service.seeds();

      /// Agregamos GUIA TURISTICO
      const categoria_guia_service = new CategoriaGuiaService(CategoriaGuia);
      await categoria_guia_service.seeds();

      const guiasTuristicosService = new GuiaTuristicoService(GuiaTuristico);
      await guiasTuristicosService.seeds();

      //Agregamos todo lo necesario para empresa
      const tiposDeEmpresaService = new TipoEmpresaService(TipoEmpresa);
      await tiposDeEmpresaService.seeds();

      const subtiposDeEmpresaService = new SubtipoEmpresaService(
        SubtipoEmpresa,
      );
      await subtiposDeEmpresaService.seeds();

      const especialidadDeEmpresaService = new EspecialidadDeEmpresaService(
        EspecialidadEmpresa,
      );
      await especialidadDeEmpresaService.seeds();

      const empresaService = new EmpresaService(Empresa);
      await empresaService.seeds();

      await empresaService.agregarDescripciones();

      const usuarioService = new UsuarioService(Usuario);
      await usuarioService.InsertAdmin();

      return sequelize;
    },
  },
];
