import { Controller, Get, Param, Res } from '@nestjs/common';
import { Response } from 'express';

@Controller('images')
export class ImageController {
  @Get(':imageName')
  getImage(@Param('imageName') imageName: string, @Res() res: Response) {
    try {
      console.log('cargamos imagen');
      const image = res.sendFile(imageName, { root: 'uploads' });
      return image;
    } catch (error) {
      return error;
    }
  }
}
