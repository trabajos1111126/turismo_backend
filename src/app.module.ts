import { Module } from '@nestjs/common';
import { DatabaseModule } from './database/database.module';
import { VocacionModule } from './modules/module-inventario-turistico/vocacion/vocacion.module';

import { AppController } from './app.controller';
import { InventarioTuristicoModule } from './modules/module-inventario-turistico/inventario-turistico/inventario_turistico.module';
import { AuthModule } from './modules/module-usuario/auth/auth.module';
import { GuiaTuristicoModule } from './modules/module-guia-turistico/guia-turistico/guia-turistico.module';

import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { ImageController } from './image.controller';
import { CategoriaGuiaModule } from './modules/module-guia-turistico/categoria-guia/categoria_guia.module';
import { MediaLibraryModule } from './modules/module-library/media-library/media-library.module';
import { InteresPrincipalModule } from './modules/module-inventario-turistico/interes-principal/interes_principal.module';
import { SubtipoInventarioModule } from './modules/module-inventario-turistico/subtipo-inventario/subtipo_inventario.module';
import { TipoInventarioModule } from './modules/module-inventario-turistico/tipo-inventario/tipo_inventario.module';
import { ValoracionInventarioModule } from './modules/module-inventario-turistico/valoracion/valoracion.module';
import { TipoEmpresaModule } from './modules/module-empresa/tipo-empresa/tipo_empresa.module';
import { EmpresaModule } from './modules/module-empresa/empresa/empresa.module';
import { ValoracionEmpresaModule } from './modules/module-empresa/valoracion/valoracion-empresa.module';

import { ConfigModule } from '@nestjs/config';
import { FotoEmpresa } from './modules/module-library/foto_empresa/entities/foto_empresa.entity';
import { RutaModule } from './modules/module-rutas-turisticas/rutas/ruta.module';
import { LogModule } from './modules/module-usuario/log/log.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env',
      isGlobal: true,
    }),
    DatabaseModule,
    //IdiomasModule,
    CategoriaGuiaModule,
    GuiaTuristicoModule,
    VocacionModule,
    //CategoriaInventarioModule,
    SubtipoInventarioModule,
    TipoInventarioModule,
    InventarioTuristicoModule,
    InteresPrincipalModule,
    MediaLibraryModule,
    ValoracionInventarioModule,
    //utiles para empresa
    TipoEmpresaModule,
    EmpresaModule,
    ValoracionEmpresaModule,
    RutaModule,
    LogModule,
    AuthModule,
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'uploads'), // Ruta a la carpeta de imágenes
      serveRoot: '/images', // Ruta base para servir las imágenes
    }),
  ],
  controllers: [AppController, ImageController],
  providers: [],
})
export class AppModule {}
