import { SequelizeOptions } from 'sequelize-typescript';
import * as moment from 'moment-timezone';

import * as dotenv from 'dotenv';

dotenv.config();

const databaseConfig: SequelizeOptions = {
  dialect: 'postgres',
  host: process.env.DATABASE_HOST, // Cambia esto por la dirección de tu servidor MySQL
  port: parseInt(process.env.DATABASE_PORT), // Cambia esto por el puerto de tu servidor MySQL
  username: process.env.DATABASE_USER, // Cambia esto por el nombre de usuario de tu base de datos
  password: process.env.DATABASE_PASSWORD, // Cambia esto por la contraseña de tu base de datos
  database: process.env.DATABASE_NAME, // Cambia esto por el nombre de tu base de datos
  //timezone: moment().tz(moment.tz.guess()).format('YYYY-MM-DD HH:mm:ss'), //moment.tz.guess(),
  timezone: 'America/La_Paz',
  define: {
    timestamps: true, // Si deseas incluir campos createdAt y updatedAt en tus tablas
    underscored: true, // Si deseas utilizar el estilo de nombres de columna con guiones bajos en lugar de camelCase
    //createdAt: 'created_at',
    getterMethods: {
      createdAt() {
        return moment(this.dataValues.createdAt).format('YYYY-MM-DD HH:mm:ss');
      },
      updatedAt() {
        return moment(this.dataValues.updatedAt).format('YYYY-MM-DD HH:mm:ss');
      },
    },
  },
  models: [__dirname + '/../**/*.entity.ts'], // Ruta a tus archivos de modelos de Sequelize
  logging: false, // Si deseas mostrar las consultas SQL generadas por Sequelize en la consola
  dialectOptions: {
    //
    useUTC: false, // Establece useUTC en false
  },
};

export default databaseConfig;
