import { HttpException, HttpStatus } from '@nestjs/common';
import { diskStorage } from 'multer';
import { extname } from 'path';

export const multerConfigVocacion = {
  dest: './uploads/vocacion', // Ruta donde se almacenarán las imágenes subidas
  storage: diskStorage({
    destination: './uploads/vocacion',
    filename: (req, file, cb) => {
      const uniqueSuffix = `${Date.now()}-${Math.round(Math.random() * 1e9)}`;
      const extension = extname(file.originalname);
      cb(null, `${uniqueSuffix}${extension}`);
    },
  }),
  limits: {
    fileSize: 1024 * 1024 * 1, // Tamaño máximo del archivo en bytes (en este caso, 5MB)
    files: 1,
  },
  fileFilter: (req, file, cb) => {
    // Función para filtrar los tipos de archivo permitidos
    const allowedMimes = ['image/jpeg', 'image/png', 'image/jpg'];
    if (allowedMimes.includes(file.mimetype)) {
      cb(null, true);
    } else {
      cb(
        new HttpException('Tipo de Archivo Invalido.', HttpStatus.BAD_REQUEST),
      );
    }
  },
};

export const multerConfigMediaLibrary = {
  dest: './uploads/media_library', // Ruta donde se almacenarán las imágenes subidas
  storage: diskStorage({
    destination: './uploads/media_library',
    filename: (req, file, cb) => {
      const uniqueSuffix = `${Date.now()}-${Math.round(Math.random() * 1e9)}`;
      const extension = extname(file.originalname);
      cb(null, `${uniqueSuffix}${extension}`);
    },
  }),
  limits: {
    fileSize: 1024 * 1024 * 1, // Tamaño máximo del archivo en bytes (en este caso, 5MB)
    files: 1,
  },
  fileFilter: (req, file, cb) => {
    // Función para filtrar los tipos de archivo permitidos
    const allowedMimes = ['image/jpeg', 'image/png', 'image/jpg'];
    if (allowedMimes.includes(file.mimetype)) {
      cb(null, true);
    } else {
      cb(
        new HttpException('Tipo de Archivo Invalido.', HttpStatus.BAD_REQUEST),
      );
    }
  },
};

export const multerConfigInventario = {
  dest: './uploads/inventario', // Ruta donde se almacenarán las imágenes subidas
  storage: diskStorage({
    destination: './uploads/inventario',
    filename: (req, file, cb) => {
      const uniqueSuffix = `${Date.now()}-${Math.round(Math.random() * 1e9)}`;
      const extension = extname(file.originalname);
      cb(null, `${uniqueSuffix}${extension}`);
    },
  }),
  limits: {
    fileSize: 1024 * 1024 * 1, // Tamaño máximo del archivo en bytes (en este caso, 5MB)
    files: 1,
  },
  fileFilter: (req, file, cb) => {
    // Función para filtrar los tipos de archivo permitidos
    const allowedMimes = ['image/jpeg', 'image/png', 'image/jpg'];
    if (allowedMimes.includes(file.mimetype)) {
      cb(null, true);
    } else {
      cb(
        new HttpException('Tipo de Archivo Invalido.', HttpStatus.BAD_REQUEST),
      );
    }
  },
};

export const multerConfigEmpresa = {
  dest: './uploads/empresa', // Ruta donde se almacenarán las imágenes subidas
  storage: diskStorage({
    destination: './uploads/empresa',
    filename: (req, file, cb) => {
      const uniqueSuffix = `${Date.now()}-${Math.round(Math.random() * 1e9)}`;
      const extension = extname(file.originalname);
      console.log('fileee');
      cb(null, `${uniqueSuffix}${extension}`);
    },
  }),
  limits: {
    fileSize: 1024 * 1024 * 1, // Tamaño máximo del archivo en bytes (en este caso, 5MB)
    files: 1,
  },
  fileFilter: (req, file, cb) => {
    // Función para filtrar los tipos de archivo permitidos
    const allowedMimes = ['image/jpeg', 'image/png', 'image/jpg'];
    if (allowedMimes.includes(file.mimetype)) {
      cb(null, true);
    } else {
      cb(
        new HttpException('Tipo de Archivo Invalido.', HttpStatus.BAD_REQUEST),
      );
    }
  },
};

export const multerConfigEmpresaLogo = {
  dest: './uploads/empresa/logo', // Ruta donde se almacenarán las imágenes subidas
  storage: diskStorage({
    destination: './uploads/empresa/logo',
    filename: (req, file, cb) => {
      const uniqueSuffix = `${Date.now()}-${Math.round(Math.random() * 1e9)}`;
      const extension = extname(file.originalname);
      console.log('fileee');
      cb(null, `${uniqueSuffix}${extension}`);
    },
  }),
  limits: {
    fileSize: 1024 * 1024 * 1, // Tamaño máximo del archivo en bytes (en este caso, 5MB)
    files: 1,
  },
  fileFilter: (req, file, cb) => {
    // Función para filtrar los tipos de archivo permitidos
    const allowedMimes = ['image/jpeg', 'image/png', 'image/jpg'];
    if (allowedMimes.includes(file.mimetype)) {
      cb(null, true);
    } else {
      cb(
        new HttpException('Tipo de Archivo Invalido.', HttpStatus.BAD_REQUEST),
      );
    }
  },
};

export const multerConfigRutaLogo = {
  dest: './uploads/empresa/logo', // Ruta donde se almacenarán las imágenes subidas
  storage: diskStorage({
    destination: './uploads/ruta',
    filename: (req, file, cb) => {
      const uniqueSuffix = `${Date.now()}-${Math.round(Math.random() * 1e9)}`;
      const extension = extname(file.originalname);
      console.log('fileee');
      cb(null, `${uniqueSuffix}${extension}`);
    },
  }),
  limits: {
    fileSize: 1024 * 1024 * 1, // Tamaño máximo del archivo en bytes (en este caso, 5MB)
    files: 1,
  },
  fileFilter: (req, file, cb) => {
    // Función para filtrar los tipos de archivo permitidos
    const allowedMimes = ['image/jpeg', 'image/png', 'image/jpg'];
    if (allowedMimes.includes(file.mimetype)) {
      cb(null, true);
    } else {
      cb(
        new HttpException('Tipo de Archivo Invalido.', HttpStatus.BAD_REQUEST),
      );
    }
  },
};
