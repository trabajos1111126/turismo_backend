import { Controller, Get, HttpCode, HttpStatus } from '@nestjs/common';
import * as momentTimezone from 'moment-timezone';
@Controller('api')
export class AppController {
  @HttpCode(HttpStatus.OK)
  @Get()
  saludo() {
    const currentTime = momentTimezone().format('YYYY-MM-DD HH:mm:ss');
    console.log('saludo de aplicacion');
    return 'Saludos desde Backen-Turismo! \n' + currentTime;
  }
}
