import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import * as momentTimezone from 'moment-timezone';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });
  app.useGlobalPipes(new ValidationPipe());
  // Configura la zona horaria para Bolivia (GMT-4)
  momentTimezone.tz.setDefault('America/La_Paz');
  await app.listen(process.env.HOST_PORT || 3000);
  console.log('************************************************************');
  console.log(
    `*  🚀 Backend Application is running on: ${await app.getUrl()} *`,
  );
  console.log('************************************************************');
}
bootstrap();
