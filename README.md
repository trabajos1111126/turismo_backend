<div align="center">
  <img src="logo.png" alt="Logo de Mi Proyecto" width="200">
</div>

# SISTEMA DE ADMINISTRACIÓN E INFORMACIÓN TURÍSTICA  
## (BACK-END)

El objetivo principal es proporcionar una experiencia integral y accesible que promueva la cultura, aventura y gastronomía de la ciudad de La Paz, brindando a los turistas opciones de viaje personalizadas y fomentando la participación en actividades turísticas.. 

## Contenido

Back-End controlador de usuraio y notocias.
El proyecto esta realizado en:

- [Node.js](https://nodejs.org/es/)
- [NestJS](https://nestjs.com/)
- [PostgresSQL](https://www.postgresql.org/)

  **ESTE ES UN SERVIDOR NO VISUAL**

## Programas necesarios

Para poder utilizar el proyecto en localhost en necesario clonarlo y tener algunos programas necesarios:

- [Nodejs](https://nodejs.org/es/download/) v12.18.0 o Superior.
- IDE de desarrollo de tu comodidad Ej. [VS Code](https://code.visualstudio.com/download)
- [PostMan](https://www.postman.com/downloads/) para puebas de APIS. (Opcional)
- [Git](https://git-scm.com/downloads) para poder gestionar las versiones.


## Configuración del Entorno

1. **Clonar el Repositorio:**

   Comando para clonar:
   
   ```bash
    cd existing_folder
    git clone [LINK DEL REPOSITORIO]
    ```

2. **Instalar dependencias:**

   Asegúrate de tener Node.js y npm instalados en tu máquina. Luego, instala las dependencias del proyecto:
   
   ```bash
   npm install
   ```
   
3. **Configuracion de variales env:**

   Renombra el archivo **.env.example** por **.env** en la raíz del proyecto y define las variables de entorno necesarias. Por ejemplo:en **host_port** podemos cambiar por **3000**.

   ```bash
   HOST_PORT=<host_port>
   
   DATABASE_HOST= <database_host>
   DATABASE_USER= <database_user>
   DATABASE_PASSWORD= <database_password>
   DATABASE_PORT= <database_port>
   DATABASE_NAME= <database_name>
   
   # jwtoken
   JWT_SECRET=<key_encrited>
   ```
   donde describimos las variables como:
   
   - **host_port**: Numero de puesto asignado
   
   - **database_host**: Cambia esto por la dirección de tu servidor POSTGRES
   
   - **database_user**: Cambia esto por el nombre de usuario de tu base de datos
   
   - **database_password**: Cambia esto por la contraseña de tu base de datos
   
   - **database_port**: Cambia esto por el puerto de tu servidor POSTGRES
   
   - **database_name**: Cambia esto por el nombre de tu base de datos

## Puesta en marha del servidor
1. **Iniciar servidor**: 

   Una vez que las dependencias estén instaladas y las variables de entorno estén configuradas, puedes iniciar el servidor:
   ```bash
   npm run start:dev
   ```
2. **Explorar la API:**

   ¡Tu servidor Nest.js está en funcionamiento! Explora la API accediendo a http://localhost:3000/api en tu navegador o utilizando herramientas como **Postman** para hacer solicitudes API.
   
## Contribuciones

¡Las contribuciones son bienvenidas! Si encuentras algún problema o tienes ideas para mejorar este proyecto, siéntete libre de crear un issue o enviar un pull request.

## Distribuciones

Derechos reservador por **@DTIGA 2023**.

## Autor
Desarrollo realizado por [Abraham Villca](https://github.com/vicovillca "Title") 🚀